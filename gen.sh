set -e

echo "⚠️⚠️⚠️⚠️ This should be used with flutter version 2.* and not 1.* ⚠️⚠️⚠️⚠️"

export DART_POST_PROCESS_FILE="/Users/nse/dev/flutter/flutter-2/bin/cache/dart-sdk/bin/dartfmt -w"

rm -rf ./gen ./.dart_tool ./pubspec.lock ./.packages

echo "Cleanup done"

openapi-generator generate \
    -i https://storage.ssegning.com/sse.media/openapi/99travl/99travl-api.yml \
    -g dart-dio-next \
    --additional-properties=pubHomepage=https://developers.99travl.com,pubAuthor=99travl.com,pubName=nine_travl_api,pubVersion=5.1.0 \
    -o ./gen/nine_travl_api \
    --enable-post-process-file \
    --package-name=nine_travl_api

echo "Now generated"

cd gen/nine_travl_api && flutter pub get && flutter pub run build_runner build
