// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'average_rating_travel.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$AverageRatingTravel extends AverageRatingTravel {
  @override
  final double value;
  @override
  final int count;
  @override
  final String? travelId;

  factory _$AverageRatingTravel(
          [void Function(AverageRatingTravelBuilder)? updates]) =>
      (new AverageRatingTravelBuilder()..update(updates)).build();

  _$AverageRatingTravel._(
      {required this.value, required this.count, this.travelId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        value, 'AverageRatingTravel', 'value');
    BuiltValueNullFieldError.checkNotNull(
        count, 'AverageRatingTravel', 'count');
  }

  @override
  AverageRatingTravel rebuild(
          void Function(AverageRatingTravelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  AverageRatingTravelBuilder toBuilder() =>
      new AverageRatingTravelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is AverageRatingTravel &&
        value == other.value &&
        count == other.count &&
        travelId == other.travelId;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, value.hashCode), count.hashCode), travelId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('AverageRatingTravel')
          ..add('value', value)
          ..add('count', count)
          ..add('travelId', travelId))
        .toString();
  }
}

class AverageRatingTravelBuilder
    implements Builder<AverageRatingTravel, AverageRatingTravelBuilder> {
  _$AverageRatingTravel? _$v;

  double? _value;
  double? get value => _$this._value;
  set value(double? value) => _$this._value = value;

  int? _count;
  int? get count => _$this._count;
  set count(int? count) => _$this._count = count;

  String? _travelId;
  String? get travelId => _$this._travelId;
  set travelId(String? travelId) => _$this._travelId = travelId;

  AverageRatingTravelBuilder() {
    AverageRatingTravel._initializeBuilder(this);
  }

  AverageRatingTravelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _value = $v.value;
      _count = $v.count;
      _travelId = $v.travelId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(AverageRatingTravel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$AverageRatingTravel;
  }

  @override
  void update(void Function(AverageRatingTravelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$AverageRatingTravel build() {
    final _$result = _$v ??
        new _$AverageRatingTravel._(
            value: BuiltValueNullFieldError.checkNotNull(
                value, 'AverageRatingTravel', 'value'),
            count: BuiltValueNullFieldError.checkNotNull(
                count, 'AverageRatingTravel', 'count'),
            travelId: travelId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
