//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'travel_reservation_count.g.dart';

abstract class TravelReservationCount
    implements Built<TravelReservationCount, TravelReservationCountBuilder> {
  @BuiltValueField(wireName: r'count')
  int? get count;

  TravelReservationCount._();

  static void _initializeBuilder(TravelReservationCountBuilder b) => b;

  factory TravelReservationCount(
          [void updates(TravelReservationCountBuilder b)]) =
      _$TravelReservationCount;

  @BuiltValueSerializer(custom: true)
  static Serializer<TravelReservationCount> get serializer =>
      _$TravelReservationCountSerializer();
}

class _$TravelReservationCountSerializer
    implements StructuredSerializer<TravelReservationCount> {
  @override
  final Iterable<Type> types = const [
    TravelReservationCount,
    _$TravelReservationCount
  ];

  @override
  final String wireName = r'TravelReservationCount';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, TravelReservationCount object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.count != null) {
      result
        ..add(r'count')
        ..add(serializers.serialize(object.count,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  TravelReservationCount deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = TravelReservationCountBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }
    return result.build();
  }
}
