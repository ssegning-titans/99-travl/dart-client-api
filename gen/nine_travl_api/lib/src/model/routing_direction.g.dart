// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'routing_direction.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$RoutingDirection extends RoutingDirection {
  @override
  final BuiltList<RoutingMapPlace>? routes;

  factory _$RoutingDirection(
          [void Function(RoutingDirectionBuilder)? updates]) =>
      (new RoutingDirectionBuilder()..update(updates)).build();

  _$RoutingDirection._({this.routes}) : super._();

  @override
  RoutingDirection rebuild(void Function(RoutingDirectionBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RoutingDirectionBuilder toBuilder() =>
      new RoutingDirectionBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RoutingDirection && routes == other.routes;
  }

  @override
  int get hashCode {
    return $jf($jc(0, routes.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RoutingDirection')
          ..add('routes', routes))
        .toString();
  }
}

class RoutingDirectionBuilder
    implements Builder<RoutingDirection, RoutingDirectionBuilder> {
  _$RoutingDirection? _$v;

  ListBuilder<RoutingMapPlace>? _routes;
  ListBuilder<RoutingMapPlace> get routes =>
      _$this._routes ??= new ListBuilder<RoutingMapPlace>();
  set routes(ListBuilder<RoutingMapPlace>? routes) => _$this._routes = routes;

  RoutingDirectionBuilder() {
    RoutingDirection._initializeBuilder(this);
  }

  RoutingDirectionBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _routes = $v.routes?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RoutingDirection other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RoutingDirection;
  }

  @override
  void update(void Function(RoutingDirectionBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RoutingDirection build() {
    _$RoutingDirection _$result;
    try {
      _$result = _$v ?? new _$RoutingDirection._(routes: _routes?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'routes';
        _routes?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'RoutingDirection', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
