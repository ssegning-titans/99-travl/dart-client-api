//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:nine_travl_api/src/model/chat_contact.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'contact_chat_message_all_of.g.dart';

abstract class ContactChatMessageAllOf
    implements Built<ContactChatMessageAllOf, ContactChatMessageAllOfBuilder> {
  @BuiltValueField(wireName: r'contacts')
  BuiltSet<ChatContact> get contacts;

  ContactChatMessageAllOf._();

  static void _initializeBuilder(ContactChatMessageAllOfBuilder b) => b;

  factory ContactChatMessageAllOf(
          [void updates(ContactChatMessageAllOfBuilder b)]) =
      _$ContactChatMessageAllOf;

  @BuiltValueSerializer(custom: true)
  static Serializer<ContactChatMessageAllOf> get serializer =>
      _$ContactChatMessageAllOfSerializer();
}

class _$ContactChatMessageAllOfSerializer
    implements StructuredSerializer<ContactChatMessageAllOf> {
  @override
  final Iterable<Type> types = const [
    ContactChatMessageAllOf,
    _$ContactChatMessageAllOf
  ];

  @override
  final String wireName = r'ContactChatMessageAllOf';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, ContactChatMessageAllOf object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'contacts')
      ..add(serializers.serialize(object.contacts,
          specifiedType: const FullType(BuiltSet, [FullType(ChatContact)])));
    return result;
  }

  @override
  ContactChatMessageAllOf deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = ContactChatMessageAllOfBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'contacts':
          result.contacts.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltSet, [FullType(ChatContact)]))
              as BuiltSet<ChatContact>);
          break;
      }
    }
    return result.build();
  }
}
