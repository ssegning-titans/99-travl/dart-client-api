// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_rating.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$BaseRating extends BaseRating {
  @override
  final String? description;
  @override
  final String authorId;
  @override
  final RatingValue value;
  @override
  final int? createdAt;
  @override
  final RatingType type;

  factory _$BaseRating([void Function(BaseRatingBuilder)? updates]) =>
      (new BaseRatingBuilder()..update(updates)).build();

  _$BaseRating._(
      {this.description,
      required this.authorId,
      required this.value,
      this.createdAt,
      required this.type})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(authorId, 'BaseRating', 'authorId');
    BuiltValueNullFieldError.checkNotNull(value, 'BaseRating', 'value');
    BuiltValueNullFieldError.checkNotNull(type, 'BaseRating', 'type');
  }

  @override
  BaseRating rebuild(void Function(BaseRatingBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BaseRatingBuilder toBuilder() => new BaseRatingBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BaseRating &&
        description == other.description &&
        authorId == other.authorId &&
        value == other.value &&
        createdAt == other.createdAt &&
        type == other.type;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, description.hashCode), authorId.hashCode),
                value.hashCode),
            createdAt.hashCode),
        type.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BaseRating')
          ..add('description', description)
          ..add('authorId', authorId)
          ..add('value', value)
          ..add('createdAt', createdAt)
          ..add('type', type))
        .toString();
  }
}

class BaseRatingBuilder implements Builder<BaseRating, BaseRatingBuilder> {
  _$BaseRating? _$v;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  RatingValue? _value;
  RatingValue? get value => _$this._value;
  set value(RatingValue? value) => _$this._value = value;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  RatingType? _type;
  RatingType? get type => _$this._type;
  set type(RatingType? type) => _$this._type = type;

  BaseRatingBuilder() {
    BaseRating._initializeBuilder(this);
  }

  BaseRatingBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _description = $v.description;
      _authorId = $v.authorId;
      _value = $v.value;
      _createdAt = $v.createdAt;
      _type = $v.type;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BaseRating other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BaseRating;
  }

  @override
  void update(void Function(BaseRatingBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BaseRating build() {
    final _$result = _$v ??
        new _$BaseRating._(
            description: description,
            authorId: BuiltValueNullFieldError.checkNotNull(
                authorId, 'BaseRating', 'authorId'),
            value: BuiltValueNullFieldError.checkNotNull(
                value, 'BaseRating', 'value'),
            createdAt: createdAt,
            type: BuiltValueNullFieldError.checkNotNull(
                type, 'BaseRating', 'type'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
