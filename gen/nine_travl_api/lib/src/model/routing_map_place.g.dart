// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'routing_map_place.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$RoutingMapPlace extends RoutingMapPlace {
  @override
  final String? placeId;
  @override
  final double? latitude;
  @override
  final double? longitude;
  @override
  final String? formattedAddress;
  @override
  final BuiltList<AddressComponent>? addressComponent;
  @override
  final double? order;

  factory _$RoutingMapPlace([void Function(RoutingMapPlaceBuilder)? updates]) =>
      (new RoutingMapPlaceBuilder()..update(updates)).build();

  _$RoutingMapPlace._(
      {this.placeId,
      this.latitude,
      this.longitude,
      this.formattedAddress,
      this.addressComponent,
      this.order})
      : super._();

  @override
  RoutingMapPlace rebuild(void Function(RoutingMapPlaceBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RoutingMapPlaceBuilder toBuilder() =>
      new RoutingMapPlaceBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RoutingMapPlace &&
        placeId == other.placeId &&
        latitude == other.latitude &&
        longitude == other.longitude &&
        formattedAddress == other.formattedAddress &&
        addressComponent == other.addressComponent &&
        order == other.order;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, placeId.hashCode), latitude.hashCode),
                    longitude.hashCode),
                formattedAddress.hashCode),
            addressComponent.hashCode),
        order.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RoutingMapPlace')
          ..add('placeId', placeId)
          ..add('latitude', latitude)
          ..add('longitude', longitude)
          ..add('formattedAddress', formattedAddress)
          ..add('addressComponent', addressComponent)
          ..add('order', order))
        .toString();
  }
}

class RoutingMapPlaceBuilder
    implements Builder<RoutingMapPlace, RoutingMapPlaceBuilder> {
  _$RoutingMapPlace? _$v;

  String? _placeId;
  String? get placeId => _$this._placeId;
  set placeId(String? placeId) => _$this._placeId = placeId;

  double? _latitude;
  double? get latitude => _$this._latitude;
  set latitude(double? latitude) => _$this._latitude = latitude;

  double? _longitude;
  double? get longitude => _$this._longitude;
  set longitude(double? longitude) => _$this._longitude = longitude;

  String? _formattedAddress;
  String? get formattedAddress => _$this._formattedAddress;
  set formattedAddress(String? formattedAddress) =>
      _$this._formattedAddress = formattedAddress;

  ListBuilder<AddressComponent>? _addressComponent;
  ListBuilder<AddressComponent> get addressComponent =>
      _$this._addressComponent ??= new ListBuilder<AddressComponent>();
  set addressComponent(ListBuilder<AddressComponent>? addressComponent) =>
      _$this._addressComponent = addressComponent;

  double? _order;
  double? get order => _$this._order;
  set order(double? order) => _$this._order = order;

  RoutingMapPlaceBuilder() {
    RoutingMapPlace._initializeBuilder(this);
  }

  RoutingMapPlaceBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _placeId = $v.placeId;
      _latitude = $v.latitude;
      _longitude = $v.longitude;
      _formattedAddress = $v.formattedAddress;
      _addressComponent = $v.addressComponent?.toBuilder();
      _order = $v.order;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RoutingMapPlace other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RoutingMapPlace;
  }

  @override
  void update(void Function(RoutingMapPlaceBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RoutingMapPlace build() {
    _$RoutingMapPlace _$result;
    try {
      _$result = _$v ??
          new _$RoutingMapPlace._(
              placeId: placeId,
              latitude: latitude,
              longitude: longitude,
              formattedAddress: formattedAddress,
              addressComponent: _addressComponent?.build(),
              order: order);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'addressComponent';
        _addressComponent?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'RoutingMapPlace', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
