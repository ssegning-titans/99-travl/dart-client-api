//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_credential_response.g.dart';

abstract class CreateCredentialResponse
    implements
        Built<CreateCredentialResponse, CreateCredentialResponseBuilder> {
  @BuiltValueField(wireName: r'state')
  bool get state;

  CreateCredentialResponse._();

  static void _initializeBuilder(CreateCredentialResponseBuilder b) => b;

  factory CreateCredentialResponse(
          [void updates(CreateCredentialResponseBuilder b)]) =
      _$CreateCredentialResponse;

  @BuiltValueSerializer(custom: true)
  static Serializer<CreateCredentialResponse> get serializer =>
      _$CreateCredentialResponseSerializer();
}

class _$CreateCredentialResponseSerializer
    implements StructuredSerializer<CreateCredentialResponse> {
  @override
  final Iterable<Type> types = const [
    CreateCredentialResponse,
    _$CreateCredentialResponse
  ];

  @override
  final String wireName = r'CreateCredentialResponse';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, CreateCredentialResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'state')
      ..add(serializers.serialize(object.state,
          specifiedType: const FullType(bool)));
    return result;
  }

  @override
  CreateCredentialResponse deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = CreateCredentialResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'state':
          result.state = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }
    return result.build();
  }
}
