//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'text_chat_message_all_of.g.dart';

abstract class TextChatMessageAllOf
    implements Built<TextChatMessageAllOf, TextChatMessageAllOfBuilder> {
  @BuiltValueField(wireName: r'message')
  String get message;

  TextChatMessageAllOf._();

  static void _initializeBuilder(TextChatMessageAllOfBuilder b) => b;

  factory TextChatMessageAllOf([void updates(TextChatMessageAllOfBuilder b)]) =
      _$TextChatMessageAllOf;

  @BuiltValueSerializer(custom: true)
  static Serializer<TextChatMessageAllOf> get serializer =>
      _$TextChatMessageAllOfSerializer();
}

class _$TextChatMessageAllOfSerializer
    implements StructuredSerializer<TextChatMessageAllOf> {
  @override
  final Iterable<Type> types = const [
    TextChatMessageAllOf,
    _$TextChatMessageAllOf
  ];

  @override
  final String wireName = r'TextChatMessageAllOf';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, TextChatMessageAllOf object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'message')
      ..add(serializers.serialize(object.message,
          specifiedType: const FullType(String)));
    return result;
  }

  @override
  TextChatMessageAllOf deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = TextChatMessageAllOfBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
