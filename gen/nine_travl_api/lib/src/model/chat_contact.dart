//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'chat_contact.g.dart';

abstract class ChatContact implements Built<ChatContact, ChatContactBuilder> {
  @BuiltValueField(wireName: r'id')
  String? get id;

  @BuiltValueField(wireName: r'creationDate')
  int? get creationDate;

  @BuiltValueField(wireName: r'isAppUser')
  bool get isAppUser;

  @BuiltValueField(wireName: r'accountId')
  String? get accountId;

  @BuiltValueField(wireName: r'phoneNumber')
  String get phoneNumber;

  @BuiltValueField(wireName: r'description')
  String? get description;

  @BuiltValueField(wireName: r'firstName')
  String get firstName;

  @BuiltValueField(wireName: r'lastName')
  String get lastName;

  @BuiltValueField(wireName: r'avatar')
  String? get avatar;

  ChatContact._();

  static void _initializeBuilder(ChatContactBuilder b) => b;

  factory ChatContact([void updates(ChatContactBuilder b)]) = _$ChatContact;

  @BuiltValueSerializer(custom: true)
  static Serializer<ChatContact> get serializer => _$ChatContactSerializer();
}

class _$ChatContactSerializer implements StructuredSerializer<ChatContact> {
  @override
  final Iterable<Type> types = const [ChatContact, _$ChatContact];

  @override
  final String wireName = r'ChatContact';

  @override
  Iterable<Object?> serialize(Serializers serializers, ChatContact object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.id != null) {
      result
        ..add(r'id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.creationDate != null) {
      result
        ..add(r'creationDate')
        ..add(serializers.serialize(object.creationDate,
            specifiedType: const FullType(int)));
    }
    result
      ..add(r'isAppUser')
      ..add(serializers.serialize(object.isAppUser,
          specifiedType: const FullType(bool)));
    if (object.accountId != null) {
      result
        ..add(r'accountId')
        ..add(serializers.serialize(object.accountId,
            specifiedType: const FullType(String)));
    }
    result
      ..add(r'phoneNumber')
      ..add(serializers.serialize(object.phoneNumber,
          specifiedType: const FullType(String)));
    if (object.description != null) {
      result
        ..add(r'description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    result
      ..add(r'firstName')
      ..add(serializers.serialize(object.firstName,
          specifiedType: const FullType(String)));
    result
      ..add(r'lastName')
      ..add(serializers.serialize(object.lastName,
          specifiedType: const FullType(String)));
    if (object.avatar != null) {
      result
        ..add(r'avatar')
        ..add(serializers.serialize(object.avatar,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ChatContact deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = ChatContactBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'creationDate':
          result.creationDate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'isAppUser':
          result.isAppUser = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case r'accountId':
          result.accountId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'phoneNumber':
          result.phoneNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'firstName':
          result.firstName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'lastName':
          result.lastName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'avatar':
          result.avatar = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
