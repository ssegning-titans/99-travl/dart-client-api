//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'place_type.g.dart';

class PlaceType extends EnumClass {
  @BuiltValueEnumConst(wireName: r'STREET_ADDRESS')
  static const PlaceType STREET_ADDRESS = _$STREET_ADDRESS;
  @BuiltValueEnumConst(wireName: r'ROUTE')
  static const PlaceType ROUTE = _$ROUTE;
  @BuiltValueEnumConst(wireName: r'INTERSECTION')
  static const PlaceType INTERSECTION = _$INTERSECTION;
  @BuiltValueEnumConst(wireName: r'CONTINENT')
  static const PlaceType CONTINENT = _$CONTINENT;
  @BuiltValueEnumConst(wireName: r'POLITICAL')
  static const PlaceType POLITICAL = _$POLITICAL;
  @BuiltValueEnumConst(wireName: r'COUNTRY')
  static const PlaceType COUNTRY = _$COUNTRY;
  @BuiltValueEnumConst(wireName: r'ADMINISTRATIVE_AREA_LEVEL_1')
  static const PlaceType aDMINISTRATIVEAREALEVEL1 = _$aDMINISTRATIVEAREALEVEL1;
  @BuiltValueEnumConst(wireName: r'ADMINISTRATIVE_AREA_LEVEL_2')
  static const PlaceType aDMINISTRATIVEAREALEVEL2 = _$aDMINISTRATIVEAREALEVEL2;
  @BuiltValueEnumConst(wireName: r'ADMINISTRATIVE_AREA_LEVEL_3')
  static const PlaceType aDMINISTRATIVEAREALEVEL3 = _$aDMINISTRATIVEAREALEVEL3;
  @BuiltValueEnumConst(wireName: r'ADMINISTRATIVE_AREA_LEVEL_4')
  static const PlaceType aDMINISTRATIVEAREALEVEL4 = _$aDMINISTRATIVEAREALEVEL4;
  @BuiltValueEnumConst(wireName: r'ADMINISTRATIVE_AREA_LEVEL_5')
  static const PlaceType aDMINISTRATIVEAREALEVEL5 = _$aDMINISTRATIVEAREALEVEL5;
  @BuiltValueEnumConst(wireName: r'COLLOQUIAL_AREA')
  static const PlaceType COLLOQUIAL_AREA = _$COLLOQUIAL_AREA;
  @BuiltValueEnumConst(wireName: r'LOCALITY')
  static const PlaceType LOCALITY = _$LOCALITY;
  @BuiltValueEnumConst(wireName: r'WARD')
  static const PlaceType WARD = _$WARD;
  @BuiltValueEnumConst(wireName: r'SUBLOCALITY')
  static const PlaceType SUBLOCALITY = _$SUBLOCALITY;
  @BuiltValueEnumConst(wireName: r'SUBLOCALITY_LEVEL_1')
  static const PlaceType sUBLOCALITYLEVEL1 = _$sUBLOCALITYLEVEL1;
  @BuiltValueEnumConst(wireName: r'SUBLOCALITY_LEVEL_2')
  static const PlaceType sUBLOCALITYLEVEL2 = _$sUBLOCALITYLEVEL2;
  @BuiltValueEnumConst(wireName: r'SUBLOCALITY_LEVEL_3')
  static const PlaceType sUBLOCALITYLEVEL3 = _$sUBLOCALITYLEVEL3;
  @BuiltValueEnumConst(wireName: r'SUBLOCALITY_LEVEL_4')
  static const PlaceType sUBLOCALITYLEVEL4 = _$sUBLOCALITYLEVEL4;
  @BuiltValueEnumConst(wireName: r'SUBLOCALITY_LEVEL_5')
  static const PlaceType sUBLOCALITYLEVEL5 = _$sUBLOCALITYLEVEL5;
  @BuiltValueEnumConst(wireName: r'NEIGHBORHOOD')
  static const PlaceType NEIGHBORHOOD = _$NEIGHBORHOOD;
  @BuiltValueEnumConst(wireName: r'PREMISE')
  static const PlaceType PREMISE = _$PREMISE;
  @BuiltValueEnumConst(wireName: r'SUBPREMISE')
  static const PlaceType SUBPREMISE = _$SUBPREMISE;
  @BuiltValueEnumConst(wireName: r'POSTAL_CODE')
  static const PlaceType POSTAL_CODE = _$POSTAL_CODE;
  @BuiltValueEnumConst(wireName: r'POSTAL_CODE_PREFIX')
  static const PlaceType POSTAL_CODE_PREFIX = _$POSTAL_CODE_PREFIX;
  @BuiltValueEnumConst(wireName: r'POSTAL_CODE_SUFFIX')
  static const PlaceType POSTAL_CODE_SUFFIX = _$POSTAL_CODE_SUFFIX;
  @BuiltValueEnumConst(wireName: r'NATURAL_FEATURE')
  static const PlaceType NATURAL_FEATURE = _$NATURAL_FEATURE;
  @BuiltValueEnumConst(wireName: r'AIRPORT')
  static const PlaceType AIRPORT = _$AIRPORT;
  @BuiltValueEnumConst(wireName: r'PARK')
  static const PlaceType PARK = _$PARK;
  @BuiltValueEnumConst(wireName: r'POINT_OF_INTEREST')
  static const PlaceType POINT_OF_INTEREST = _$POINT_OF_INTEREST;
  @BuiltValueEnumConst(wireName: r'FLOOR')
  static const PlaceType FLOOR = _$FLOOR;
  @BuiltValueEnumConst(wireName: r'ESTABLISHMENT')
  static const PlaceType ESTABLISHMENT = _$ESTABLISHMENT;
  @BuiltValueEnumConst(wireName: r'PARKING')
  static const PlaceType PARKING = _$PARKING;
  @BuiltValueEnumConst(wireName: r'POST_BOX')
  static const PlaceType POST_BOX = _$POST_BOX;
  @BuiltValueEnumConst(wireName: r'POSTAL_TOWN')
  static const PlaceType POSTAL_TOWN = _$POSTAL_TOWN;
  @BuiltValueEnumConst(wireName: r'ROOM')
  static const PlaceType ROOM = _$ROOM;
  @BuiltValueEnumConst(wireName: r'STREET_NUMBER')
  static const PlaceType STREET_NUMBER = _$STREET_NUMBER;
  @BuiltValueEnumConst(wireName: r'BUS_STATION')
  static const PlaceType BUS_STATION = _$BUS_STATION;
  @BuiltValueEnumConst(wireName: r'TRAIN_STATION')
  static const PlaceType TRAIN_STATION = _$TRAIN_STATION;
  @BuiltValueEnumConst(wireName: r'SUBWAY_STATION')
  static const PlaceType SUBWAY_STATION = _$SUBWAY_STATION;
  @BuiltValueEnumConst(wireName: r'TRANSIT_STATION')
  static const PlaceType TRANSIT_STATION = _$TRANSIT_STATION;
  @BuiltValueEnumConst(wireName: r'LIGHT_RAIL_STATION')
  static const PlaceType LIGHT_RAIL_STATION = _$LIGHT_RAIL_STATION;
  @BuiltValueEnumConst(wireName: r'GENERAL_CONTRACTOR')
  static const PlaceType GENERAL_CONTRACTOR = _$GENERAL_CONTRACTOR;
  @BuiltValueEnumConst(wireName: r'FOOD')
  static const PlaceType FOOD = _$FOOD;
  @BuiltValueEnumConst(wireName: r'REAL_ESTATE_AGENCY')
  static const PlaceType REAL_ESTATE_AGENCY = _$REAL_ESTATE_AGENCY;
  @BuiltValueEnumConst(wireName: r'CAR_RENTAL')
  static const PlaceType CAR_RENTAL = _$CAR_RENTAL;
  @BuiltValueEnumConst(wireName: r'TRAVEL_AGENCY')
  static const PlaceType TRAVEL_AGENCY = _$TRAVEL_AGENCY;
  @BuiltValueEnumConst(wireName: r'ELECTRONICS_STORE')
  static const PlaceType ELECTRONICS_STORE = _$ELECTRONICS_STORE;
  @BuiltValueEnumConst(wireName: r'HOME_GOODS_STORE')
  static const PlaceType HOME_GOODS_STORE = _$HOME_GOODS_STORE;
  @BuiltValueEnumConst(wireName: r'SCHOOL')
  static const PlaceType SCHOOL = _$SCHOOL;
  @BuiltValueEnumConst(wireName: r'STORE')
  static const PlaceType STORE = _$STORE;
  @BuiltValueEnumConst(wireName: r'SHOPPING_MALL')
  static const PlaceType SHOPPING_MALL = _$SHOPPING_MALL;
  @BuiltValueEnumConst(wireName: r'LODGING')
  static const PlaceType LODGING = _$LODGING;
  @BuiltValueEnumConst(wireName: r'ART_GALLERY')
  static const PlaceType ART_GALLERY = _$ART_GALLERY;
  @BuiltValueEnumConst(wireName: r'LAWYER')
  static const PlaceType LAWYER = _$LAWYER;
  @BuiltValueEnumConst(wireName: r'RESTAURANT')
  static const PlaceType RESTAURANT = _$RESTAURANT;
  @BuiltValueEnumConst(wireName: r'BAR')
  static const PlaceType BAR = _$BAR;
  @BuiltValueEnumConst(wireName: r'MEAL_TAKEAWAY')
  static const PlaceType MEAL_TAKEAWAY = _$MEAL_TAKEAWAY;
  @BuiltValueEnumConst(wireName: r'CLOTHING_STORE')
  static const PlaceType CLOTHING_STORE = _$CLOTHING_STORE;
  @BuiltValueEnumConst(wireName: r'LOCAL_GOVERNMENT_OFFICE')
  static const PlaceType LOCAL_GOVERNMENT_OFFICE = _$LOCAL_GOVERNMENT_OFFICE;
  @BuiltValueEnumConst(wireName: r'FINANCE')
  static const PlaceType FINANCE = _$FINANCE;
  @BuiltValueEnumConst(wireName: r'MOVING_COMPANY')
  static const PlaceType MOVING_COMPANY = _$MOVING_COMPANY;
  @BuiltValueEnumConst(wireName: r'STORAGE')
  static const PlaceType STORAGE = _$STORAGE;
  @BuiltValueEnumConst(wireName: r'CAFE')
  static const PlaceType CAFE = _$CAFE;
  @BuiltValueEnumConst(wireName: r'CAR_REPAIR')
  static const PlaceType CAR_REPAIR = _$CAR_REPAIR;
  @BuiltValueEnumConst(wireName: r'HEALTH')
  static const PlaceType HEALTH = _$HEALTH;
  @BuiltValueEnumConst(wireName: r'INSURANCE_AGENCY')
  static const PlaceType INSURANCE_AGENCY = _$INSURANCE_AGENCY;
  @BuiltValueEnumConst(wireName: r'PAINTER')
  static const PlaceType PAINTER = _$PAINTER;
  @BuiltValueEnumConst(wireName: r'ARCHIPELAGO')
  static const PlaceType ARCHIPELAGO = _$ARCHIPELAGO;
  @BuiltValueEnumConst(wireName: r'MUSEUM')
  static const PlaceType MUSEUM = _$MUSEUM;
  @BuiltValueEnumConst(wireName: r'CAMPGROUND')
  static const PlaceType CAMPGROUND = _$CAMPGROUND;
  @BuiltValueEnumConst(wireName: r'RV_PARK')
  static const PlaceType RV_PARK = _$RV_PARK;
  @BuiltValueEnumConst(wireName: r'MEAL_DELIVERY')
  static const PlaceType MEAL_DELIVERY = _$MEAL_DELIVERY;
  @BuiltValueEnumConst(wireName: r'PRIMARY_SCHOOL')
  static const PlaceType PRIMARY_SCHOOL = _$PRIMARY_SCHOOL;
  @BuiltValueEnumConst(wireName: r'SECONDARY_SCHOOL')
  static const PlaceType SECONDARY_SCHOOL = _$SECONDARY_SCHOOL;
  @BuiltValueEnumConst(wireName: r'TOWN_SQUARE')
  static const PlaceType TOWN_SQUARE = _$TOWN_SQUARE;
  @BuiltValueEnumConst(wireName: r'TOURIST_ATTRACTION')
  static const PlaceType TOURIST_ATTRACTION = _$TOURIST_ATTRACTION;
  @BuiltValueEnumConst(wireName: r'PLUS_CODE')
  static const PlaceType PLUS_CODE = _$PLUS_CODE;
  @BuiltValueEnumConst(wireName: r'DRUGSTORE')
  static const PlaceType DRUGSTORE = _$DRUGSTORE;
  @BuiltValueEnumConst(wireName: r'UNKNOWN')
  static const PlaceType UNKNOWN = _$UNKNOWN;

  static Serializer<PlaceType> get serializer => _$placeTypeSerializer;

  const PlaceType._(String name) : super(name);

  static BuiltSet<PlaceType> get values => _$values;
  static PlaceType valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class PlaceTypeMixin = Object with _$PlaceTypeMixin;
