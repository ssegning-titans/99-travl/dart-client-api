// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'travel.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Travel extends Travel {
  @override
  final int? createdAt;
  @override
  final int? updatedAt;
  @override
  final String? id;
  @override
  final BuiltList<TravelPoint> passagePoints;
  @override
  final String providerAccountId;
  @override
  final TravelQuantity maxQuantity;
  @override
  final TravelModeUnit? transportMode;
  @override
  final TravelLuggageDimension luggageDimension;
  @override
  final String? description;

  factory _$Travel([void Function(TravelBuilder)? updates]) =>
      (new TravelBuilder()..update(updates)).build();

  _$Travel._(
      {this.createdAt,
      this.updatedAt,
      this.id,
      required this.passagePoints,
      required this.providerAccountId,
      required this.maxQuantity,
      this.transportMode,
      required this.luggageDimension,
      this.description})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        passagePoints, 'Travel', 'passagePoints');
    BuiltValueNullFieldError.checkNotNull(
        providerAccountId, 'Travel', 'providerAccountId');
    BuiltValueNullFieldError.checkNotNull(maxQuantity, 'Travel', 'maxQuantity');
    BuiltValueNullFieldError.checkNotNull(
        luggageDimension, 'Travel', 'luggageDimension');
  }

  @override
  Travel rebuild(void Function(TravelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TravelBuilder toBuilder() => new TravelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Travel &&
        createdAt == other.createdAt &&
        updatedAt == other.updatedAt &&
        id == other.id &&
        passagePoints == other.passagePoints &&
        providerAccountId == other.providerAccountId &&
        maxQuantity == other.maxQuantity &&
        transportMode == other.transportMode &&
        luggageDimension == other.luggageDimension &&
        description == other.description;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc($jc(0, createdAt.hashCode),
                                    updatedAt.hashCode),
                                id.hashCode),
                            passagePoints.hashCode),
                        providerAccountId.hashCode),
                    maxQuantity.hashCode),
                transportMode.hashCode),
            luggageDimension.hashCode),
        description.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Travel')
          ..add('createdAt', createdAt)
          ..add('updatedAt', updatedAt)
          ..add('id', id)
          ..add('passagePoints', passagePoints)
          ..add('providerAccountId', providerAccountId)
          ..add('maxQuantity', maxQuantity)
          ..add('transportMode', transportMode)
          ..add('luggageDimension', luggageDimension)
          ..add('description', description))
        .toString();
  }
}

class TravelBuilder implements Builder<Travel, TravelBuilder> {
  _$Travel? _$v;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  int? _updatedAt;
  int? get updatedAt => _$this._updatedAt;
  set updatedAt(int? updatedAt) => _$this._updatedAt = updatedAt;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  ListBuilder<TravelPoint>? _passagePoints;
  ListBuilder<TravelPoint> get passagePoints =>
      _$this._passagePoints ??= new ListBuilder<TravelPoint>();
  set passagePoints(ListBuilder<TravelPoint>? passagePoints) =>
      _$this._passagePoints = passagePoints;

  String? _providerAccountId;
  String? get providerAccountId => _$this._providerAccountId;
  set providerAccountId(String? providerAccountId) =>
      _$this._providerAccountId = providerAccountId;

  TravelQuantityBuilder? _maxQuantity;
  TravelQuantityBuilder get maxQuantity =>
      _$this._maxQuantity ??= new TravelQuantityBuilder();
  set maxQuantity(TravelQuantityBuilder? maxQuantity) =>
      _$this._maxQuantity = maxQuantity;

  TravelModeUnit? _transportMode;
  TravelModeUnit? get transportMode => _$this._transportMode;
  set transportMode(TravelModeUnit? transportMode) =>
      _$this._transportMode = transportMode;

  TravelLuggageDimension? _luggageDimension;
  TravelLuggageDimension? get luggageDimension => _$this._luggageDimension;
  set luggageDimension(TravelLuggageDimension? luggageDimension) =>
      _$this._luggageDimension = luggageDimension;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  TravelBuilder() {
    Travel._initializeBuilder(this);
  }

  TravelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _createdAt = $v.createdAt;
      _updatedAt = $v.updatedAt;
      _id = $v.id;
      _passagePoints = $v.passagePoints.toBuilder();
      _providerAccountId = $v.providerAccountId;
      _maxQuantity = $v.maxQuantity.toBuilder();
      _transportMode = $v.transportMode;
      _luggageDimension = $v.luggageDimension;
      _description = $v.description;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Travel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Travel;
  }

  @override
  void update(void Function(TravelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Travel build() {
    _$Travel _$result;
    try {
      _$result = _$v ??
          new _$Travel._(
              createdAt: createdAt,
              updatedAt: updatedAt,
              id: id,
              passagePoints: passagePoints.build(),
              providerAccountId: BuiltValueNullFieldError.checkNotNull(
                  providerAccountId, 'Travel', 'providerAccountId'),
              maxQuantity: maxQuantity.build(),
              transportMode: transportMode,
              luggageDimension: BuiltValueNullFieldError.checkNotNull(
                  luggageDimension, 'Travel', 'luggageDimension'),
              description: description);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'passagePoints';
        passagePoints.build();

        _$failedField = 'maxQuantity';
        maxQuantity.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'Travel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
