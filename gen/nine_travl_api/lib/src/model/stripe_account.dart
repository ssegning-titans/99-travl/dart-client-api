//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'stripe_account.g.dart';

abstract class StripeAccount
    implements Built<StripeAccount, StripeAccountBuilder> {
  @BuiltValueField(wireName: r'stripeAccountId')
  String? get stripeAccountId;

  StripeAccount._();

  static void _initializeBuilder(StripeAccountBuilder b) => b;

  factory StripeAccount([void updates(StripeAccountBuilder b)]) =
      _$StripeAccount;

  @BuiltValueSerializer(custom: true)
  static Serializer<StripeAccount> get serializer =>
      _$StripeAccountSerializer();
}

class _$StripeAccountSerializer implements StructuredSerializer<StripeAccount> {
  @override
  final Iterable<Type> types = const [StripeAccount, _$StripeAccount];

  @override
  final String wireName = r'StripeAccount';

  @override
  Iterable<Object?> serialize(Serializers serializers, StripeAccount object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.stripeAccountId != null) {
      result
        ..add(r'stripeAccountId')
        ..add(serializers.serialize(object.stripeAccountId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  StripeAccount deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = StripeAccountBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'stripeAccountId':
          result.stripeAccountId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
