//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'disable_credential_response.g.dart';

abstract class DisableCredentialResponse
    implements
        Built<DisableCredentialResponse, DisableCredentialResponseBuilder> {
  @BuiltValueField(wireName: r'state')
  bool get state;

  DisableCredentialResponse._();

  static void _initializeBuilder(DisableCredentialResponseBuilder b) => b;

  factory DisableCredentialResponse(
          [void updates(DisableCredentialResponseBuilder b)]) =
      _$DisableCredentialResponse;

  @BuiltValueSerializer(custom: true)
  static Serializer<DisableCredentialResponse> get serializer =>
      _$DisableCredentialResponseSerializer();
}

class _$DisableCredentialResponseSerializer
    implements StructuredSerializer<DisableCredentialResponse> {
  @override
  final Iterable<Type> types = const [
    DisableCredentialResponse,
    _$DisableCredentialResponse
  ];

  @override
  final String wireName = r'DisableCredentialResponse';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, DisableCredentialResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'state')
      ..add(serializers.serialize(object.state,
          specifiedType: const FullType(bool)));
    return result;
  }

  @override
  DisableCredentialResponse deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = DisableCredentialResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'state':
          result.state = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }
    return result.build();
  }
}
