// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_report_bug.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CreateReportBug extends CreateReportBug {
  @override
  final String title;
  @override
  final String? description;
  @override
  final BuiltMap<String, String> deviceInfo;
  @override
  final BuiltList<BugMedia> screenshot;

  factory _$CreateReportBug([void Function(CreateReportBugBuilder)? updates]) =>
      (new CreateReportBugBuilder()..update(updates)).build();

  _$CreateReportBug._(
      {required this.title,
      this.description,
      required this.deviceInfo,
      required this.screenshot})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(title, 'CreateReportBug', 'title');
    BuiltValueNullFieldError.checkNotNull(
        deviceInfo, 'CreateReportBug', 'deviceInfo');
    BuiltValueNullFieldError.checkNotNull(
        screenshot, 'CreateReportBug', 'screenshot');
  }

  @override
  CreateReportBug rebuild(void Function(CreateReportBugBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreateReportBugBuilder toBuilder() =>
      new CreateReportBugBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreateReportBug &&
        title == other.title &&
        description == other.description &&
        deviceInfo == other.deviceInfo &&
        screenshot == other.screenshot;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, title.hashCode), description.hashCode),
            deviceInfo.hashCode),
        screenshot.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CreateReportBug')
          ..add('title', title)
          ..add('description', description)
          ..add('deviceInfo', deviceInfo)
          ..add('screenshot', screenshot))
        .toString();
  }
}

class CreateReportBugBuilder
    implements Builder<CreateReportBug, CreateReportBugBuilder> {
  _$CreateReportBug? _$v;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  MapBuilder<String, String>? _deviceInfo;
  MapBuilder<String, String> get deviceInfo =>
      _$this._deviceInfo ??= new MapBuilder<String, String>();
  set deviceInfo(MapBuilder<String, String>? deviceInfo) =>
      _$this._deviceInfo = deviceInfo;

  ListBuilder<BugMedia>? _screenshot;
  ListBuilder<BugMedia> get screenshot =>
      _$this._screenshot ??= new ListBuilder<BugMedia>();
  set screenshot(ListBuilder<BugMedia>? screenshot) =>
      _$this._screenshot = screenshot;

  CreateReportBugBuilder() {
    CreateReportBug._initializeBuilder(this);
  }

  CreateReportBugBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _title = $v.title;
      _description = $v.description;
      _deviceInfo = $v.deviceInfo.toBuilder();
      _screenshot = $v.screenshot.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreateReportBug other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CreateReportBug;
  }

  @override
  void update(void Function(CreateReportBugBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CreateReportBug build() {
    _$CreateReportBug _$result;
    try {
      _$result = _$v ??
          new _$CreateReportBug._(
              title: BuiltValueNullFieldError.checkNotNull(
                  title, 'CreateReportBug', 'title'),
              description: description,
              deviceInfo: deviceInfo.build(),
              screenshot: screenshot.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'deviceInfo';
        deviceInfo.build();
        _$failedField = 'screenshot';
        screenshot.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CreateReportBug', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
