// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'bug_media.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$BugMedia extends BugMedia {
  @override
  final String? id;
  @override
  final int? creationDate;
  @override
  final String documentId;
  @override
  final String? bugId;

  factory _$BugMedia([void Function(BugMediaBuilder)? updates]) =>
      (new BugMediaBuilder()..update(updates)).build();

  _$BugMedia._(
      {this.id, this.creationDate, required this.documentId, this.bugId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(documentId, 'BugMedia', 'documentId');
  }

  @override
  BugMedia rebuild(void Function(BugMediaBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BugMediaBuilder toBuilder() => new BugMediaBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BugMedia &&
        id == other.id &&
        creationDate == other.creationDate &&
        documentId == other.documentId &&
        bugId == other.bugId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), creationDate.hashCode),
            documentId.hashCode),
        bugId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BugMedia')
          ..add('id', id)
          ..add('creationDate', creationDate)
          ..add('documentId', documentId)
          ..add('bugId', bugId))
        .toString();
  }
}

class BugMediaBuilder implements Builder<BugMedia, BugMediaBuilder> {
  _$BugMedia? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _creationDate;
  int? get creationDate => _$this._creationDate;
  set creationDate(int? creationDate) => _$this._creationDate = creationDate;

  String? _documentId;
  String? get documentId => _$this._documentId;
  set documentId(String? documentId) => _$this._documentId = documentId;

  String? _bugId;
  String? get bugId => _$this._bugId;
  set bugId(String? bugId) => _$this._bugId = bugId;

  BugMediaBuilder() {
    BugMedia._initializeBuilder(this);
  }

  BugMediaBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _creationDate = $v.creationDate;
      _documentId = $v.documentId;
      _bugId = $v.bugId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BugMedia other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BugMedia;
  }

  @override
  void update(void Function(BugMediaBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BugMedia build() {
    final _$result = _$v ??
        new _$BugMedia._(
            id: id,
            creationDate: creationDate,
            documentId: BuiltValueNullFieldError.checkNotNull(
                documentId, 'BugMedia', 'documentId'),
            bugId: bugId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
