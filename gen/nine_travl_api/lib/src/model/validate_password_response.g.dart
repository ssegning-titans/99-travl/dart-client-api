// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'validate_password_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ValidatePasswordResponse extends ValidatePasswordResponse {
  @override
  final bool valid;

  factory _$ValidatePasswordResponse(
          [void Function(ValidatePasswordResponseBuilder)? updates]) =>
      (new ValidatePasswordResponseBuilder()..update(updates)).build();

  _$ValidatePasswordResponse._({required this.valid}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        valid, 'ValidatePasswordResponse', 'valid');
  }

  @override
  ValidatePasswordResponse rebuild(
          void Function(ValidatePasswordResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ValidatePasswordResponseBuilder toBuilder() =>
      new ValidatePasswordResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ValidatePasswordResponse && valid == other.valid;
  }

  @override
  int get hashCode {
    return $jf($jc(0, valid.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ValidatePasswordResponse')
          ..add('valid', valid))
        .toString();
  }
}

class ValidatePasswordResponseBuilder
    implements
        Builder<ValidatePasswordResponse, ValidatePasswordResponseBuilder> {
  _$ValidatePasswordResponse? _$v;

  bool? _valid;
  bool? get valid => _$this._valid;
  set valid(bool? valid) => _$this._valid = valid;

  ValidatePasswordResponseBuilder() {
    ValidatePasswordResponse._initializeBuilder(this);
  }

  ValidatePasswordResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _valid = $v.valid;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ValidatePasswordResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ValidatePasswordResponse;
  }

  @override
  void update(void Function(ValidatePasswordResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ValidatePasswordResponse build() {
    final _$result = _$v ??
        new _$ValidatePasswordResponse._(
            valid: BuiltValueNullFieldError.checkNotNull(
                valid, 'ValidatePasswordResponse', 'valid'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
