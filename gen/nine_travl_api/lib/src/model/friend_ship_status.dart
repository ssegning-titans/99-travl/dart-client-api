//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'friend_ship_status.g.dart';

class FriendShipStatus extends EnumClass {
  @BuiltValueEnumConst(wireName: r'ACCEPTED')
  static const FriendShipStatus ACCEPTED = _$ACCEPTED;
  @BuiltValueEnumConst(wireName: r'REFUSED')
  static const FriendShipStatus REFUSED = _$REFUSED;
  @BuiltValueEnumConst(wireName: r'PENDING')
  static const FriendShipStatus PENDING = _$PENDING;

  static Serializer<FriendShipStatus> get serializer =>
      _$friendShipStatusSerializer;

  const FriendShipStatus._(String name) : super(name);

  static BuiltSet<FriendShipStatus> get values => _$values;
  static FriendShipStatus valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class FriendShipStatusMixin = Object with _$FriendShipStatusMixin;
