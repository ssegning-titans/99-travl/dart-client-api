// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorite_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const FavoriteType _$TRAVEL = const FavoriteType._('TRAVEL');
const FavoriteType _$ACCOUNT = const FavoriteType._('ACCOUNT');

FavoriteType _$valueOf(String name) {
  switch (name) {
    case 'TRAVEL':
      return _$TRAVEL;
    case 'ACCOUNT':
      return _$ACCOUNT;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<FavoriteType> _$values =
    new BuiltSet<FavoriteType>(const <FavoriteType>[
  _$TRAVEL,
  _$ACCOUNT,
]);

class _$FavoriteTypeMeta {
  const _$FavoriteTypeMeta();
  FavoriteType get TRAVEL => _$TRAVEL;
  FavoriteType get ACCOUNT => _$ACCOUNT;
  FavoriteType valueOf(String name) => _$valueOf(name);
  BuiltSet<FavoriteType> get values => _$values;
}

abstract class _$FavoriteTypeMixin {
  // ignore: non_constant_identifier_names
  _$FavoriteTypeMeta get FavoriteType => const _$FavoriteTypeMeta();
}

Serializer<FavoriteType> _$favoriteTypeSerializer =
    new _$FavoriteTypeSerializer();

class _$FavoriteTypeSerializer implements PrimitiveSerializer<FavoriteType> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'TRAVEL': 'TRAVEL',
    'ACCOUNT': 'ACCOUNT',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'TRAVEL': 'TRAVEL',
    'ACCOUNT': 'ACCOUNT',
  };

  @override
  final Iterable<Type> types = const <Type>[FavoriteType];
  @override
  final String wireName = 'FavoriteType';

  @override
  Object serialize(Serializers serializers, FavoriteType object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  FavoriteType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      FavoriteType.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
