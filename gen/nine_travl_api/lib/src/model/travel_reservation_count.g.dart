// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'travel_reservation_count.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$TravelReservationCount extends TravelReservationCount {
  @override
  final int? count;

  factory _$TravelReservationCount(
          [void Function(TravelReservationCountBuilder)? updates]) =>
      (new TravelReservationCountBuilder()..update(updates)).build();

  _$TravelReservationCount._({this.count}) : super._();

  @override
  TravelReservationCount rebuild(
          void Function(TravelReservationCountBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TravelReservationCountBuilder toBuilder() =>
      new TravelReservationCountBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TravelReservationCount && count == other.count;
  }

  @override
  int get hashCode {
    return $jf($jc(0, count.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TravelReservationCount')
          ..add('count', count))
        .toString();
  }
}

class TravelReservationCountBuilder
    implements Builder<TravelReservationCount, TravelReservationCountBuilder> {
  _$TravelReservationCount? _$v;

  int? _count;
  int? get count => _$this._count;
  set count(int? count) => _$this._count = count;

  TravelReservationCountBuilder() {
    TravelReservationCount._initializeBuilder(this);
  }

  TravelReservationCountBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _count = $v.count;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TravelReservationCount other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TravelReservationCount;
  }

  @override
  void update(void Function(TravelReservationCountBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TravelReservationCount build() {
    final _$result = _$v ?? new _$TravelReservationCount._(count: count);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
