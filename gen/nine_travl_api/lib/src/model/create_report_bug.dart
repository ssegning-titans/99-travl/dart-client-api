//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/bug_media.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_report_bug.g.dart';

abstract class CreateReportBug
    implements Built<CreateReportBug, CreateReportBugBuilder> {
  @BuiltValueField(wireName: r'title')
  String get title;

  @BuiltValueField(wireName: r'description')
  String? get description;

  @BuiltValueField(wireName: r'deviceInfo')
  BuiltMap<String, String> get deviceInfo;

  @BuiltValueField(wireName: r'screenshot')
  BuiltList<BugMedia> get screenshot;

  CreateReportBug._();

  static void _initializeBuilder(CreateReportBugBuilder b) => b;

  factory CreateReportBug([void updates(CreateReportBugBuilder b)]) =
      _$CreateReportBug;

  @BuiltValueSerializer(custom: true)
  static Serializer<CreateReportBug> get serializer =>
      _$CreateReportBugSerializer();
}

class _$CreateReportBugSerializer
    implements StructuredSerializer<CreateReportBug> {
  @override
  final Iterable<Type> types = const [CreateReportBug, _$CreateReportBug];

  @override
  final String wireName = r'CreateReportBug';

  @override
  Iterable<Object?> serialize(Serializers serializers, CreateReportBug object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'title')
      ..add(serializers.serialize(object.title,
          specifiedType: const FullType(String)));
    if (object.description != null) {
      result
        ..add(r'description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    result
      ..add(r'deviceInfo')
      ..add(serializers.serialize(object.deviceInfo,
          specifiedType:
              const FullType(BuiltMap, [FullType(String), FullType(String)])));
    result
      ..add(r'screenshot')
      ..add(serializers.serialize(object.screenshot,
          specifiedType: const FullType(BuiltList, [FullType(BugMedia)])));
    return result;
  }

  @override
  CreateReportBug deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = CreateReportBugBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'title':
          result.title = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'deviceInfo':
          result.deviceInfo.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltMap, [FullType(String), FullType(String)]))
              as BuiltMap<String, String>);
          break;
        case r'screenshot':
          result.screenshot.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, [FullType(BugMedia)]))
              as BuiltList<BugMedia>);
          break;
      }
    }
    return result.build();
  }
}
