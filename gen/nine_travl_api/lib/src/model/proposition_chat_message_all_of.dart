//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'proposition_chat_message_all_of.g.dart';

abstract class PropositionChatMessageAllOf
    implements
        Built<PropositionChatMessageAllOf, PropositionChatMessageAllOfBuilder> {
  @BuiltValueField(wireName: r'propositionId')
  String get propositionId;

  @BuiltValueField(wireName: r'proposerAccountId')
  String get proposerAccountId;

  PropositionChatMessageAllOf._();

  static void _initializeBuilder(PropositionChatMessageAllOfBuilder b) => b;

  factory PropositionChatMessageAllOf(
          [void updates(PropositionChatMessageAllOfBuilder b)]) =
      _$PropositionChatMessageAllOf;

  @BuiltValueSerializer(custom: true)
  static Serializer<PropositionChatMessageAllOf> get serializer =>
      _$PropositionChatMessageAllOfSerializer();
}

class _$PropositionChatMessageAllOfSerializer
    implements StructuredSerializer<PropositionChatMessageAllOf> {
  @override
  final Iterable<Type> types = const [
    PropositionChatMessageAllOf,
    _$PropositionChatMessageAllOf
  ];

  @override
  final String wireName = r'PropositionChatMessageAllOf';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, PropositionChatMessageAllOf object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'propositionId')
      ..add(serializers.serialize(object.propositionId,
          specifiedType: const FullType(String)));
    result
      ..add(r'proposerAccountId')
      ..add(serializers.serialize(object.proposerAccountId,
          specifiedType: const FullType(String)));
    return result;
  }

  @override
  PropositionChatMessageAllOf deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = PropositionChatMessageAllOfBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'propositionId':
          result.propositionId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'proposerAccountId':
          result.proposerAccountId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
