//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'phone_tan_validate_request.g.dart';

abstract class PhoneTanValidateRequest
    implements Built<PhoneTanValidateRequest, PhoneTanValidateRequestBuilder> {
  @BuiltValueField(wireName: r'secret')
  String? get secret;

  @BuiltValueField(wireName: r'phoneNumber')
  String? get phoneNumber;

  @BuiltValueField(wireName: r'code')
  int? get code;

  PhoneTanValidateRequest._();

  static void _initializeBuilder(PhoneTanValidateRequestBuilder b) =>
      b..code = 1;

  factory PhoneTanValidateRequest(
          [void updates(PhoneTanValidateRequestBuilder b)]) =
      _$PhoneTanValidateRequest;

  @BuiltValueSerializer(custom: true)
  static Serializer<PhoneTanValidateRequest> get serializer =>
      _$PhoneTanValidateRequestSerializer();
}

class _$PhoneTanValidateRequestSerializer
    implements StructuredSerializer<PhoneTanValidateRequest> {
  @override
  final Iterable<Type> types = const [
    PhoneTanValidateRequest,
    _$PhoneTanValidateRequest
  ];

  @override
  final String wireName = r'PhoneTanValidateRequest';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, PhoneTanValidateRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.secret != null) {
      result
        ..add(r'secret')
        ..add(serializers.serialize(object.secret,
            specifiedType: const FullType(String)));
    }
    if (object.phoneNumber != null) {
      result
        ..add(r'phoneNumber')
        ..add(serializers.serialize(object.phoneNumber,
            specifiedType: const FullType(String)));
    }
    if (object.code != null) {
      result
        ..add(r'code')
        ..add(serializers.serialize(object.code,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  PhoneTanValidateRequest deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = PhoneTanValidateRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'secret':
          result.secret = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'phoneNumber':
          result.phoneNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }
    return result.build();
  }
}
