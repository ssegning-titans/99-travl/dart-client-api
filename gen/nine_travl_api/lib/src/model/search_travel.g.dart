// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'search_travel.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$SearchTravel extends SearchTravel {
  @override
  final BuiltList<TravelPoint> points;
  @override
  final Travel travel;
  @override
  final Account provider;

  factory _$SearchTravel([void Function(SearchTravelBuilder)? updates]) =>
      (new SearchTravelBuilder()..update(updates)).build();

  _$SearchTravel._(
      {required this.points, required this.travel, required this.provider})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(points, 'SearchTravel', 'points');
    BuiltValueNullFieldError.checkNotNull(travel, 'SearchTravel', 'travel');
    BuiltValueNullFieldError.checkNotNull(provider, 'SearchTravel', 'provider');
  }

  @override
  SearchTravel rebuild(void Function(SearchTravelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  SearchTravelBuilder toBuilder() => new SearchTravelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is SearchTravel &&
        points == other.points &&
        travel == other.travel &&
        provider == other.provider;
  }

  @override
  int get hashCode {
    return $jf(
        $jc($jc($jc(0, points.hashCode), travel.hashCode), provider.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('SearchTravel')
          ..add('points', points)
          ..add('travel', travel)
          ..add('provider', provider))
        .toString();
  }
}

class SearchTravelBuilder
    implements Builder<SearchTravel, SearchTravelBuilder> {
  _$SearchTravel? _$v;

  ListBuilder<TravelPoint>? _points;
  ListBuilder<TravelPoint> get points =>
      _$this._points ??= new ListBuilder<TravelPoint>();
  set points(ListBuilder<TravelPoint>? points) => _$this._points = points;

  TravelBuilder? _travel;
  TravelBuilder get travel => _$this._travel ??= new TravelBuilder();
  set travel(TravelBuilder? travel) => _$this._travel = travel;

  AccountBuilder? _provider;
  AccountBuilder get provider => _$this._provider ??= new AccountBuilder();
  set provider(AccountBuilder? provider) => _$this._provider = provider;

  SearchTravelBuilder() {
    SearchTravel._initializeBuilder(this);
  }

  SearchTravelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _points = $v.points.toBuilder();
      _travel = $v.travel.toBuilder();
      _provider = $v.provider.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(SearchTravel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$SearchTravel;
  }

  @override
  void update(void Function(SearchTravelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$SearchTravel build() {
    _$SearchTravel _$result;
    try {
      _$result = _$v ??
          new _$SearchTravel._(
              points: points.build(),
              travel: travel.build(),
              provider: provider.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'points';
        points.build();
        _$failedField = 'travel';
        travel.build();
        _$failedField = 'provider';
        provider.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'SearchTravel', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
