//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'document.g.dart';

abstract class Document implements Built<Document, DocumentBuilder> {
  @BuiltValueField(wireName: r'bytes')
  String? get bytes;

  @BuiltValueField(wireName: r'contentType')
  String? get contentType;

  @BuiltValueField(wireName: r'filePath')
  String? get filePath;

  @BuiltValueField(wireName: r'metaData')
  BuiltMap<String, String>? get metaData;

  @BuiltValueField(wireName: r'headers')
  BuiltMap<String, String>? get headers;

  Document._();

  static void _initializeBuilder(DocumentBuilder b) => b;

  factory Document([void updates(DocumentBuilder b)]) = _$Document;

  @BuiltValueSerializer(custom: true)
  static Serializer<Document> get serializer => _$DocumentSerializer();
}

class _$DocumentSerializer implements StructuredSerializer<Document> {
  @override
  final Iterable<Type> types = const [Document, _$Document];

  @override
  final String wireName = r'Document';

  @override
  Iterable<Object?> serialize(Serializers serializers, Document object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.bytes != null) {
      result
        ..add(r'bytes')
        ..add(serializers.serialize(object.bytes,
            specifiedType: const FullType(String)));
    }
    if (object.contentType != null) {
      result
        ..add(r'contentType')
        ..add(serializers.serialize(object.contentType,
            specifiedType: const FullType(String)));
    }
    if (object.filePath != null) {
      result
        ..add(r'filePath')
        ..add(serializers.serialize(object.filePath,
            specifiedType: const FullType(String)));
    }
    if (object.metaData != null) {
      result
        ..add(r'metaData')
        ..add(serializers.serialize(object.metaData,
            specifiedType: const FullType(
                BuiltMap, [FullType(String), FullType(String)])));
    }
    if (object.headers != null) {
      result
        ..add(r'headers')
        ..add(serializers.serialize(object.headers,
            specifiedType: const FullType(
                BuiltMap, [FullType(String), FullType(String)])));
    }
    return result;
  }

  @override
  Document deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = DocumentBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'bytes':
          result.bytes = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'contentType':
          result.contentType = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'filePath':
          result.filePath = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'metaData':
          result.metaData.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltMap, [FullType(String), FullType(String)]))
              as BuiltMap<String, String>);
          break;
        case r'headers':
          result.headers.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltMap, [FullType(String), FullType(String)]))
              as BuiltMap<String, String>);
          break;
      }
    }
    return result.build();
  }
}
