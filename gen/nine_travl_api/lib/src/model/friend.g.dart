// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friend.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$Friend extends Friend {
  @override
  final String id;
  @override
  final int creationDate;
  @override
  final FriendShipStatus? status;
  @override
  final FriendType? type;
  @override
  final FriendProcessType processType;
  @override
  final String? accountId;
  @override
  final String? authorId;
  @override
  final bool? isUserActive;
  @override
  final String? phoneNumber;
  @override
  final String? description;
  @override
  final String? firstName;
  @override
  final String? lastName;
  @override
  final String? avatar;

  factory _$Friend([void Function(FriendBuilder)? updates]) =>
      (new FriendBuilder()..update(updates)).build();

  _$Friend._(
      {required this.id,
      required this.creationDate,
      this.status,
      this.type,
      required this.processType,
      this.accountId,
      this.authorId,
      this.isUserActive,
      this.phoneNumber,
      this.description,
      this.firstName,
      this.lastName,
      this.avatar})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, 'Friend', 'id');
    BuiltValueNullFieldError.checkNotNull(
        creationDate, 'Friend', 'creationDate');
    BuiltValueNullFieldError.checkNotNull(processType, 'Friend', 'processType');
  }

  @override
  Friend rebuild(void Function(FriendBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FriendBuilder toBuilder() => new FriendBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Friend &&
        id == other.id &&
        creationDate == other.creationDate &&
        status == other.status &&
        type == other.type &&
        processType == other.processType &&
        accountId == other.accountId &&
        authorId == other.authorId &&
        isUserActive == other.isUserActive &&
        phoneNumber == other.phoneNumber &&
        description == other.description &&
        firstName == other.firstName &&
        lastName == other.lastName &&
        avatar == other.avatar;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc($jc(0, id.hashCode),
                                                    creationDate.hashCode),
                                                status.hashCode),
                                            type.hashCode),
                                        processType.hashCode),
                                    accountId.hashCode),
                                authorId.hashCode),
                            isUserActive.hashCode),
                        phoneNumber.hashCode),
                    description.hashCode),
                firstName.hashCode),
            lastName.hashCode),
        avatar.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Friend')
          ..add('id', id)
          ..add('creationDate', creationDate)
          ..add('status', status)
          ..add('type', type)
          ..add('processType', processType)
          ..add('accountId', accountId)
          ..add('authorId', authorId)
          ..add('isUserActive', isUserActive)
          ..add('phoneNumber', phoneNumber)
          ..add('description', description)
          ..add('firstName', firstName)
          ..add('lastName', lastName)
          ..add('avatar', avatar))
        .toString();
  }
}

class FriendBuilder implements Builder<Friend, FriendBuilder> {
  _$Friend? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _creationDate;
  int? get creationDate => _$this._creationDate;
  set creationDate(int? creationDate) => _$this._creationDate = creationDate;

  FriendShipStatus? _status;
  FriendShipStatus? get status => _$this._status;
  set status(FriendShipStatus? status) => _$this._status = status;

  FriendType? _type;
  FriendType? get type => _$this._type;
  set type(FriendType? type) => _$this._type = type;

  FriendProcessType? _processType;
  FriendProcessType? get processType => _$this._processType;
  set processType(FriendProcessType? processType) =>
      _$this._processType = processType;

  String? _accountId;
  String? get accountId => _$this._accountId;
  set accountId(String? accountId) => _$this._accountId = accountId;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  bool? _isUserActive;
  bool? get isUserActive => _$this._isUserActive;
  set isUserActive(bool? isUserActive) => _$this._isUserActive = isUserActive;

  String? _phoneNumber;
  String? get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String? phoneNumber) => _$this._phoneNumber = phoneNumber;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _firstName;
  String? get firstName => _$this._firstName;
  set firstName(String? firstName) => _$this._firstName = firstName;

  String? _lastName;
  String? get lastName => _$this._lastName;
  set lastName(String? lastName) => _$this._lastName = lastName;

  String? _avatar;
  String? get avatar => _$this._avatar;
  set avatar(String? avatar) => _$this._avatar = avatar;

  FriendBuilder() {
    Friend._initializeBuilder(this);
  }

  FriendBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _creationDate = $v.creationDate;
      _status = $v.status;
      _type = $v.type;
      _processType = $v.processType;
      _accountId = $v.accountId;
      _authorId = $v.authorId;
      _isUserActive = $v.isUserActive;
      _phoneNumber = $v.phoneNumber;
      _description = $v.description;
      _firstName = $v.firstName;
      _lastName = $v.lastName;
      _avatar = $v.avatar;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Friend other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$Friend;
  }

  @override
  void update(void Function(FriendBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Friend build() {
    final _$result = _$v ??
        new _$Friend._(
            id: BuiltValueNullFieldError.checkNotNull(id, 'Friend', 'id'),
            creationDate: BuiltValueNullFieldError.checkNotNull(
                creationDate, 'Friend', 'creationDate'),
            status: status,
            type: type,
            processType: BuiltValueNullFieldError.checkNotNull(
                processType, 'Friend', 'processType'),
            accountId: accountId,
            authorId: authorId,
            isUserActive: isUserActive,
            phoneNumber: phoneNumber,
            description: description,
            firstName: firstName,
            lastName: lastName,
            avatar: avatar);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
