// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'travel_quantity.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$TravelQuantity extends TravelQuantity {
  @override
  final int weight;
  @override
  final TravelWeightUnit unit;
  @override
  final int pricePerUnit;

  factory _$TravelQuantity([void Function(TravelQuantityBuilder)? updates]) =>
      (new TravelQuantityBuilder()..update(updates)).build();

  _$TravelQuantity._(
      {required this.weight, required this.unit, required this.pricePerUnit})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(weight, 'TravelQuantity', 'weight');
    BuiltValueNullFieldError.checkNotNull(unit, 'TravelQuantity', 'unit');
    BuiltValueNullFieldError.checkNotNull(
        pricePerUnit, 'TravelQuantity', 'pricePerUnit');
  }

  @override
  TravelQuantity rebuild(void Function(TravelQuantityBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TravelQuantityBuilder toBuilder() =>
      new TravelQuantityBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TravelQuantity &&
        weight == other.weight &&
        unit == other.unit &&
        pricePerUnit == other.pricePerUnit;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc(0, weight.hashCode), unit.hashCode), pricePerUnit.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TravelQuantity')
          ..add('weight', weight)
          ..add('unit', unit)
          ..add('pricePerUnit', pricePerUnit))
        .toString();
  }
}

class TravelQuantityBuilder
    implements Builder<TravelQuantity, TravelQuantityBuilder> {
  _$TravelQuantity? _$v;

  int? _weight;
  int? get weight => _$this._weight;
  set weight(int? weight) => _$this._weight = weight;

  TravelWeightUnit? _unit;
  TravelWeightUnit? get unit => _$this._unit;
  set unit(TravelWeightUnit? unit) => _$this._unit = unit;

  int? _pricePerUnit;
  int? get pricePerUnit => _$this._pricePerUnit;
  set pricePerUnit(int? pricePerUnit) => _$this._pricePerUnit = pricePerUnit;

  TravelQuantityBuilder() {
    TravelQuantity._initializeBuilder(this);
  }

  TravelQuantityBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _weight = $v.weight;
      _unit = $v.unit;
      _pricePerUnit = $v.pricePerUnit;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TravelQuantity other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TravelQuantity;
  }

  @override
  void update(void Function(TravelQuantityBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TravelQuantity build() {
    final _$result = _$v ??
        new _$TravelQuantity._(
            weight: BuiltValueNullFieldError.checkNotNull(
                weight, 'TravelQuantity', 'weight'),
            unit: BuiltValueNullFieldError.checkNotNull(
                unit, 'TravelQuantity', 'unit'),
            pricePerUnit: BuiltValueNullFieldError.checkNotNull(
                pricePerUnit, 'TravelQuantity', 'pricePerUnit'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
