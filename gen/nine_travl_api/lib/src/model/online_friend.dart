//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/friend_type.dart';
import 'package:nine_travl_api/src/model/friend_process_type.dart';
import 'package:nine_travl_api/src/model/base_friend.dart';
import 'package:nine_travl_api/src/model/online_friend_all_of.dart';
import 'package:nine_travl_api/src/model/friend_ship_status.dart';
import 'package:nine_travl_api/src/model/request_online_friend_ship.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'online_friend.g.dart';

// ignore_for_file: unused_import

abstract class OnlineFriend
    implements Built<OnlineFriend, OnlineFriendBuilder> {
  @BuiltValueField(wireName: r'id')
  String get id;

  @BuiltValueField(wireName: r'creationDate')
  int get creationDate;

  @BuiltValueField(wireName: r'status')
  FriendShipStatus? get status;
  // enum statusEnum {  ACCEPTED,  REFUSED,  PENDING,  };

  @BuiltValueField(wireName: r'type')
  FriendType? get type;
  // enum typeEnum {  RECEIVER_ONLY,  SENDER_ONLY,  FULL,  };

  @BuiltValueField(wireName: r'processType')
  FriendProcessType get processType;
  // enum processTypeEnum {  OFFLINE,  ONLINE,  };

  @BuiltValueField(wireName: r'accountId')
  String? get accountId;

  @BuiltValueField(wireName: r'authorId')
  String? get authorId;

  @BuiltValueField(wireName: r'isUserActive')
  bool? get isUserActive;

  OnlineFriend._();

  static void _initializeBuilder(OnlineFriendBuilder b) => b;

  factory OnlineFriend([void updates(OnlineFriendBuilder b)]) = _$OnlineFriend;

  @BuiltValueSerializer(custom: true)
  static Serializer<OnlineFriend> get serializer => _$OnlineFriendSerializer();
}

class _$OnlineFriendSerializer implements StructuredSerializer<OnlineFriend> {
  @override
  final Iterable<Type> types = const [OnlineFriend, _$OnlineFriend];

  @override
  final String wireName = r'OnlineFriend';

  @override
  Iterable<Object?> serialize(Serializers serializers, OnlineFriend object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'id')
      ..add(serializers.serialize(object.id,
          specifiedType: const FullType(String)));
    result
      ..add(r'creationDate')
      ..add(serializers.serialize(object.creationDate,
          specifiedType: const FullType(int)));
    if (object.status != null) {
      result
        ..add(r'status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(FriendShipStatus)));
    }
    if (object.type != null) {
      result
        ..add(r'type')
        ..add(serializers.serialize(object.type,
            specifiedType: const FullType(FriendType)));
    }
    result
      ..add(r'processType')
      ..add(serializers.serialize(object.processType,
          specifiedType: const FullType(FriendProcessType)));
    if (object.accountId != null) {
      result
        ..add(r'accountId')
        ..add(serializers.serialize(object.accountId,
            specifiedType: const FullType(String)));
    }
    if (object.authorId != null) {
      result
        ..add(r'authorId')
        ..add(serializers.serialize(object.authorId,
            specifiedType: const FullType(String)));
    }
    if (object.isUserActive != null) {
      result
        ..add(r'isUserActive')
        ..add(serializers.serialize(object.isUserActive,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  OnlineFriend deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = OnlineFriendBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'creationDate':
          result.creationDate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'status':
          result.status = serializers.deserialize(value,
                  specifiedType: const FullType(FriendShipStatus))
              as FriendShipStatus;
          break;
        case r'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(FriendType)) as FriendType;
          break;
        case r'processType':
          result.processType = serializers.deserialize(value,
                  specifiedType: const FullType(FriendProcessType))
              as FriendProcessType;
          break;
        case r'accountId':
          result.accountId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'authorId':
          result.authorId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'isUserActive':
          result.isUserActive = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }
    return result.build();
  }
}
