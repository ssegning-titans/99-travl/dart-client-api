// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_chat_message.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$BaseChatMessage extends BaseChatMessage {
  @override
  final String? id;
  @override
  final int? createdAt;
  @override
  final String authorId;
  @override
  final String chatId;
  @override
  final ChatMessageType messageType;
  @override
  final bool? seen;

  factory _$BaseChatMessage([void Function(BaseChatMessageBuilder)? updates]) =>
      (new BaseChatMessageBuilder()..update(updates)).build();

  _$BaseChatMessage._(
      {this.id,
      this.createdAt,
      required this.authorId,
      required this.chatId,
      required this.messageType,
      this.seen})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        authorId, 'BaseChatMessage', 'authorId');
    BuiltValueNullFieldError.checkNotNull(chatId, 'BaseChatMessage', 'chatId');
    BuiltValueNullFieldError.checkNotNull(
        messageType, 'BaseChatMessage', 'messageType');
  }

  @override
  BaseChatMessage rebuild(void Function(BaseChatMessageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BaseChatMessageBuilder toBuilder() =>
      new BaseChatMessageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BaseChatMessage &&
        id == other.id &&
        createdAt == other.createdAt &&
        authorId == other.authorId &&
        chatId == other.chatId &&
        messageType == other.messageType &&
        seen == other.seen;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, id.hashCode), createdAt.hashCode),
                    authorId.hashCode),
                chatId.hashCode),
            messageType.hashCode),
        seen.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BaseChatMessage')
          ..add('id', id)
          ..add('createdAt', createdAt)
          ..add('authorId', authorId)
          ..add('chatId', chatId)
          ..add('messageType', messageType)
          ..add('seen', seen))
        .toString();
  }
}

class BaseChatMessageBuilder
    implements Builder<BaseChatMessage, BaseChatMessageBuilder> {
  _$BaseChatMessage? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  String? _chatId;
  String? get chatId => _$this._chatId;
  set chatId(String? chatId) => _$this._chatId = chatId;

  ChatMessageType? _messageType;
  ChatMessageType? get messageType => _$this._messageType;
  set messageType(ChatMessageType? messageType) =>
      _$this._messageType = messageType;

  bool? _seen;
  bool? get seen => _$this._seen;
  set seen(bool? seen) => _$this._seen = seen;

  BaseChatMessageBuilder() {
    BaseChatMessage._initializeBuilder(this);
  }

  BaseChatMessageBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _createdAt = $v.createdAt;
      _authorId = $v.authorId;
      _chatId = $v.chatId;
      _messageType = $v.messageType;
      _seen = $v.seen;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BaseChatMessage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BaseChatMessage;
  }

  @override
  void update(void Function(BaseChatMessageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BaseChatMessage build() {
    final _$result = _$v ??
        new _$BaseChatMessage._(
            id: id,
            createdAt: createdAt,
            authorId: BuiltValueNullFieldError.checkNotNull(
                authorId, 'BaseChatMessage', 'authorId'),
            chatId: BuiltValueNullFieldError.checkNotNull(
                chatId, 'BaseChatMessage', 'chatId'),
            messageType: BuiltValueNullFieldError.checkNotNull(
                messageType, 'BaseChatMessage', 'messageType'),
            seen: seen);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
