//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'online_friend_all_of.g.dart';

abstract class OnlineFriendAllOf
    implements Built<OnlineFriendAllOf, OnlineFriendAllOfBuilder> {
  @BuiltValueField(wireName: r'authorId')
  String? get authorId;

  @BuiltValueField(wireName: r'isUserActive')
  bool? get isUserActive;

  OnlineFriendAllOf._();

  static void _initializeBuilder(OnlineFriendAllOfBuilder b) => b;

  factory OnlineFriendAllOf([void updates(OnlineFriendAllOfBuilder b)]) =
      _$OnlineFriendAllOf;

  @BuiltValueSerializer(custom: true)
  static Serializer<OnlineFriendAllOf> get serializer =>
      _$OnlineFriendAllOfSerializer();
}

class _$OnlineFriendAllOfSerializer
    implements StructuredSerializer<OnlineFriendAllOf> {
  @override
  final Iterable<Type> types = const [OnlineFriendAllOf, _$OnlineFriendAllOf];

  @override
  final String wireName = r'OnlineFriendAllOf';

  @override
  Iterable<Object?> serialize(Serializers serializers, OnlineFriendAllOf object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.authorId != null) {
      result
        ..add(r'authorId')
        ..add(serializers.serialize(object.authorId,
            specifiedType: const FullType(String)));
    }
    if (object.isUserActive != null) {
      result
        ..add(r'isUserActive')
        ..add(serializers.serialize(object.isUserActive,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  OnlineFriendAllOf deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = OnlineFriendAllOfBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'authorId':
          result.authorId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'isUserActive':
          result.isUserActive = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }
    return result.build();
  }
}
