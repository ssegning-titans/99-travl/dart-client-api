// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_account.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ReportAccount extends ReportAccount {
  @override
  final ReportType reportType;
  @override
  final String authorId;
  @override
  final ReportStatus status;
  @override
  final int? createdAt;
  @override
  final String? id;
  @override
  final String? description;
  @override
  final String accountId;

  factory _$ReportAccount([void Function(ReportAccountBuilder)? updates]) =>
      (new ReportAccountBuilder()..update(updates)).build();

  _$ReportAccount._(
      {required this.reportType,
      required this.authorId,
      required this.status,
      this.createdAt,
      this.id,
      this.description,
      required this.accountId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        reportType, 'ReportAccount', 'reportType');
    BuiltValueNullFieldError.checkNotNull(
        authorId, 'ReportAccount', 'authorId');
    BuiltValueNullFieldError.checkNotNull(status, 'ReportAccount', 'status');
    BuiltValueNullFieldError.checkNotNull(
        accountId, 'ReportAccount', 'accountId');
  }

  @override
  ReportAccount rebuild(void Function(ReportAccountBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ReportAccountBuilder toBuilder() => new ReportAccountBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ReportAccount &&
        reportType == other.reportType &&
        authorId == other.authorId &&
        status == other.status &&
        createdAt == other.createdAt &&
        id == other.id &&
        description == other.description &&
        accountId == other.accountId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, reportType.hashCode), authorId.hashCode),
                        status.hashCode),
                    createdAt.hashCode),
                id.hashCode),
            description.hashCode),
        accountId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ReportAccount')
          ..add('reportType', reportType)
          ..add('authorId', authorId)
          ..add('status', status)
          ..add('createdAt', createdAt)
          ..add('id', id)
          ..add('description', description)
          ..add('accountId', accountId))
        .toString();
  }
}

class ReportAccountBuilder
    implements Builder<ReportAccount, ReportAccountBuilder> {
  _$ReportAccount? _$v;

  ReportType? _reportType;
  ReportType? get reportType => _$this._reportType;
  set reportType(ReportType? reportType) => _$this._reportType = reportType;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  ReportStatus? _status;
  ReportStatus? get status => _$this._status;
  set status(ReportStatus? status) => _$this._status = status;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _accountId;
  String? get accountId => _$this._accountId;
  set accountId(String? accountId) => _$this._accountId = accountId;

  ReportAccountBuilder() {
    ReportAccount._initializeBuilder(this);
  }

  ReportAccountBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _reportType = $v.reportType;
      _authorId = $v.authorId;
      _status = $v.status;
      _createdAt = $v.createdAt;
      _id = $v.id;
      _description = $v.description;
      _accountId = $v.accountId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ReportAccount other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ReportAccount;
  }

  @override
  void update(void Function(ReportAccountBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ReportAccount build() {
    final _$result = _$v ??
        new _$ReportAccount._(
            reportType: BuiltValueNullFieldError.checkNotNull(
                reportType, 'ReportAccount', 'reportType'),
            authorId: BuiltValueNullFieldError.checkNotNull(
                authorId, 'ReportAccount', 'authorId'),
            status: BuiltValueNullFieldError.checkNotNull(
                status, 'ReportAccount', 'status'),
            createdAt: createdAt,
            id: id,
            description: description,
            accountId: BuiltValueNullFieldError.checkNotNull(
                accountId, 'ReportAccount', 'accountId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
