//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/rating_value.dart';
import 'package:nine_travl_api/src/model/rating_type.dart';
import 'package:nine_travl_api/src/model/base_rating.dart';
import 'package:nine_travl_api/src/model/rating_account_all_of.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'rating_account.g.dart';

// ignore_for_file: unused_import

abstract class RatingAccount
    implements Built<RatingAccount, RatingAccountBuilder> {
  @BuiltValueField(wireName: r'description')
  String? get description;

  @BuiltValueField(wireName: r'authorId')
  String get authorId;

  @BuiltValueField(wireName: r'value')
  RatingValue get value;
  // enum valueEnum {  ONE,  TWO,  THREE,  FOUR,  FIVE,  };

  @BuiltValueField(wireName: r'createdAt')
  int? get createdAt;

  @BuiltValueField(wireName: r'type')
  RatingType get type;
  // enum typeEnum {  TRAVEL,  ACCOUNT,  };

  @BuiltValueField(wireName: r'personId')
  String get personId;

  RatingAccount._();

  static void _initializeBuilder(RatingAccountBuilder b) => b;

  factory RatingAccount([void updates(RatingAccountBuilder b)]) =
      _$RatingAccount;

  @BuiltValueSerializer(custom: true)
  static Serializer<RatingAccount> get serializer =>
      _$RatingAccountSerializer();
}

class _$RatingAccountSerializer implements StructuredSerializer<RatingAccount> {
  @override
  final Iterable<Type> types = const [RatingAccount, _$RatingAccount];

  @override
  final String wireName = r'RatingAccount';

  @override
  Iterable<Object?> serialize(Serializers serializers, RatingAccount object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.description != null) {
      result
        ..add(r'description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    result
      ..add(r'authorId')
      ..add(serializers.serialize(object.authorId,
          specifiedType: const FullType(String)));
    result
      ..add(r'value')
      ..add(serializers.serialize(object.value,
          specifiedType: const FullType(RatingValue)));
    if (object.createdAt != null) {
      result
        ..add(r'createdAt')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(int)));
    }
    result
      ..add(r'type')
      ..add(serializers.serialize(object.type,
          specifiedType: const FullType(RatingType)));
    result
      ..add(r'personId')
      ..add(serializers.serialize(object.personId,
          specifiedType: const FullType(String)));
    return result;
  }

  @override
  RatingAccount deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = RatingAccountBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'authorId':
          result.authorId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(RatingValue)) as RatingValue;
          break;
        case r'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(RatingType)) as RatingType;
          break;
        case r'personId':
          result.personId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
