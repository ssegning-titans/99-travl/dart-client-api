// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'contact_chat_message_all_of.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ContactChatMessageAllOf extends ContactChatMessageAllOf {
  @override
  final BuiltSet<ChatContact> contacts;

  factory _$ContactChatMessageAllOf(
          [void Function(ContactChatMessageAllOfBuilder)? updates]) =>
      (new ContactChatMessageAllOfBuilder()..update(updates)).build();

  _$ContactChatMessageAllOf._({required this.contacts}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        contacts, 'ContactChatMessageAllOf', 'contacts');
  }

  @override
  ContactChatMessageAllOf rebuild(
          void Function(ContactChatMessageAllOfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ContactChatMessageAllOfBuilder toBuilder() =>
      new ContactChatMessageAllOfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ContactChatMessageAllOf && contacts == other.contacts;
  }

  @override
  int get hashCode {
    return $jf($jc(0, contacts.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ContactChatMessageAllOf')
          ..add('contacts', contacts))
        .toString();
  }
}

class ContactChatMessageAllOfBuilder
    implements
        Builder<ContactChatMessageAllOf, ContactChatMessageAllOfBuilder> {
  _$ContactChatMessageAllOf? _$v;

  SetBuilder<ChatContact>? _contacts;
  SetBuilder<ChatContact> get contacts =>
      _$this._contacts ??= new SetBuilder<ChatContact>();
  set contacts(SetBuilder<ChatContact>? contacts) =>
      _$this._contacts = contacts;

  ContactChatMessageAllOfBuilder() {
    ContactChatMessageAllOf._initializeBuilder(this);
  }

  ContactChatMessageAllOfBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _contacts = $v.contacts.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ContactChatMessageAllOf other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ContactChatMessageAllOf;
  }

  @override
  void update(void Function(ContactChatMessageAllOfBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ContactChatMessageAllOf build() {
    _$ContactChatMessageAllOf _$result;
    try {
      _$result =
          _$v ?? new _$ContactChatMessageAllOf._(contacts: contacts.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'contacts';
        contacts.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ContactChatMessageAllOf', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
