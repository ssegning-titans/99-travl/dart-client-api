// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stripe_payment_intent_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$StripePaymentIntentResponse extends StripePaymentIntentResponse {
  @override
  final String? secret;

  factory _$StripePaymentIntentResponse(
          [void Function(StripePaymentIntentResponseBuilder)? updates]) =>
      (new StripePaymentIntentResponseBuilder()..update(updates)).build();

  _$StripePaymentIntentResponse._({this.secret}) : super._();

  @override
  StripePaymentIntentResponse rebuild(
          void Function(StripePaymentIntentResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  StripePaymentIntentResponseBuilder toBuilder() =>
      new StripePaymentIntentResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is StripePaymentIntentResponse && secret == other.secret;
  }

  @override
  int get hashCode {
    return $jf($jc(0, secret.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('StripePaymentIntentResponse')
          ..add('secret', secret))
        .toString();
  }
}

class StripePaymentIntentResponseBuilder
    implements
        Builder<StripePaymentIntentResponse,
            StripePaymentIntentResponseBuilder> {
  _$StripePaymentIntentResponse? _$v;

  String? _secret;
  String? get secret => _$this._secret;
  set secret(String? secret) => _$this._secret = secret;

  StripePaymentIntentResponseBuilder() {
    StripePaymentIntentResponse._initializeBuilder(this);
  }

  StripePaymentIntentResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _secret = $v.secret;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(StripePaymentIntentResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$StripePaymentIntentResponse;
  }

  @override
  void update(void Function(StripePaymentIntentResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$StripePaymentIntentResponse build() {
    final _$result = _$v ?? new _$StripePaymentIntentResponse._(secret: secret);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
