//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'credential_type.g.dart';

class CredentialType extends EnumClass {
  @BuiltValueEnumConst(wireName: r'PASSWORD')
  static const CredentialType PASSWORD = _$PASSWORD;

  static Serializer<CredentialType> get serializer =>
      _$credentialTypeSerializer;

  const CredentialType._(String name) : super(name);

  static BuiltSet<CredentialType> get values => _$values;
  static CredentialType valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class CredentialTypeMixin = Object with _$CredentialTypeMixin;
