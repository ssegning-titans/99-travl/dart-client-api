// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'online_friend_all_of.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$OnlineFriendAllOf extends OnlineFriendAllOf {
  @override
  final String? authorId;
  @override
  final bool? isUserActive;

  factory _$OnlineFriendAllOf(
          [void Function(OnlineFriendAllOfBuilder)? updates]) =>
      (new OnlineFriendAllOfBuilder()..update(updates)).build();

  _$OnlineFriendAllOf._({this.authorId, this.isUserActive}) : super._();

  @override
  OnlineFriendAllOf rebuild(void Function(OnlineFriendAllOfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  OnlineFriendAllOfBuilder toBuilder() =>
      new OnlineFriendAllOfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is OnlineFriendAllOf &&
        authorId == other.authorId &&
        isUserActive == other.isUserActive;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, authorId.hashCode), isUserActive.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('OnlineFriendAllOf')
          ..add('authorId', authorId)
          ..add('isUserActive', isUserActive))
        .toString();
  }
}

class OnlineFriendAllOfBuilder
    implements Builder<OnlineFriendAllOf, OnlineFriendAllOfBuilder> {
  _$OnlineFriendAllOf? _$v;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  bool? _isUserActive;
  bool? get isUserActive => _$this._isUserActive;
  set isUserActive(bool? isUserActive) => _$this._isUserActive = isUserActive;

  OnlineFriendAllOfBuilder() {
    OnlineFriendAllOf._initializeBuilder(this);
  }

  OnlineFriendAllOfBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _authorId = $v.authorId;
      _isUserActive = $v.isUserActive;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(OnlineFriendAllOf other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$OnlineFriendAllOf;
  }

  @override
  void update(void Function(OnlineFriendAllOfBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$OnlineFriendAllOf build() {
    final _$result = _$v ??
        new _$OnlineFriendAllOf._(
            authorId: authorId, isUserActive: isUserActive);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
