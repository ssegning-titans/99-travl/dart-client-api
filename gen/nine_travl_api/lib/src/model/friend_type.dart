//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'friend_type.g.dart';

class FriendType extends EnumClass {
  @BuiltValueEnumConst(wireName: r'RECEIVER_ONLY')
  static const FriendType RECEIVER_ONLY = _$RECEIVER_ONLY;
  @BuiltValueEnumConst(wireName: r'SENDER_ONLY')
  static const FriendType SENDER_ONLY = _$SENDER_ONLY;
  @BuiltValueEnumConst(wireName: r'FULL')
  static const FriendType FULL = _$FULL;

  static Serializer<FriendType> get serializer => _$friendTypeSerializer;

  const FriendType._(String name) : super(name);

  static BuiltSet<FriendType> get values => _$values;
  static FriendType valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class FriendTypeMixin = Object with _$FriendTypeMixin;
