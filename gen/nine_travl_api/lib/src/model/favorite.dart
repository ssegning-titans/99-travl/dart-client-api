//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/favorite_type.dart';
import 'package:nine_travl_api/src/model/favorite_account.dart';
import 'package:nine_travl_api/src/model/favorite_travel.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'favorite.g.dart';

abstract class Favorite implements Built<Favorite, FavoriteBuilder> {
  @BuiltValueField(wireName: r'ownerId')
  String get ownerId;

  @BuiltValueField(wireName: r'createdAt')
  int? get createdAt;

  @BuiltValueField(wireName: r'favoriteType')
  FavoriteType get favoriteType;
  // enum favoriteTypeEnum {  TRAVEL,  ACCOUNT,  };

  @BuiltValueField(wireName: r'accountId')
  String get accountId;

  @BuiltValueField(wireName: r'travelId')
  String get travelId;

  Favorite._();

  static void _initializeBuilder(FavoriteBuilder b) => b;

  factory Favorite([void updates(FavoriteBuilder b)]) = _$Favorite;

  @BuiltValueSerializer(custom: true)
  static Serializer<Favorite> get serializer => _$FavoriteSerializer();
}

class _$FavoriteSerializer implements StructuredSerializer<Favorite> {
  @override
  final Iterable<Type> types = const [Favorite, _$Favorite];

  @override
  final String wireName = r'Favorite';

  @override
  Iterable<Object?> serialize(Serializers serializers, Favorite object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'ownerId')
      ..add(serializers.serialize(object.ownerId,
          specifiedType: const FullType(String)));
    if (object.createdAt != null) {
      result
        ..add(r'createdAt')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(int)));
    }
    result
      ..add(r'favoriteType')
      ..add(serializers.serialize(object.favoriteType,
          specifiedType: const FullType(FavoriteType)));
    result
      ..add(r'accountId')
      ..add(serializers.serialize(object.accountId,
          specifiedType: const FullType(String)));
    result
      ..add(r'travelId')
      ..add(serializers.serialize(object.travelId,
          specifiedType: const FullType(String)));
    return result;
  }

  @override
  Favorite deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = FavoriteBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'ownerId':
          result.ownerId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'favoriteType':
          result.favoriteType = serializers.deserialize(value,
              specifiedType: const FullType(FavoriteType)) as FavoriteType;
          break;
        case r'accountId':
          result.accountId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'travelId':
          result.travelId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
