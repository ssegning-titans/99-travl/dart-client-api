//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'account_gender.g.dart';

class AccountGender extends EnumClass {
  @BuiltValueEnumConst(wireName: r'MALE')
  static const AccountGender MALE = _$MALE;
  @BuiltValueEnumConst(wireName: r'FEMALE')
  static const AccountGender FEMALE = _$FEMALE;
  @BuiltValueEnumConst(wireName: r'OTHER')
  static const AccountGender OTHER = _$OTHER;

  static Serializer<AccountGender> get serializer => _$accountGenderSerializer;

  const AccountGender._(String name) : super(name);

  static BuiltSet<AccountGender> get values => _$values;
  static AccountGender valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class AccountGenderMixin = Object with _$AccountGenderMixin;
