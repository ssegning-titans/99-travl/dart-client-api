// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'proposition_chat_message_all_of.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PropositionChatMessageAllOf extends PropositionChatMessageAllOf {
  @override
  final String propositionId;
  @override
  final String proposerAccountId;

  factory _$PropositionChatMessageAllOf(
          [void Function(PropositionChatMessageAllOfBuilder)? updates]) =>
      (new PropositionChatMessageAllOfBuilder()..update(updates)).build();

  _$PropositionChatMessageAllOf._(
      {required this.propositionId, required this.proposerAccountId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        propositionId, 'PropositionChatMessageAllOf', 'propositionId');
    BuiltValueNullFieldError.checkNotNull(
        proposerAccountId, 'PropositionChatMessageAllOf', 'proposerAccountId');
  }

  @override
  PropositionChatMessageAllOf rebuild(
          void Function(PropositionChatMessageAllOfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PropositionChatMessageAllOfBuilder toBuilder() =>
      new PropositionChatMessageAllOfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PropositionChatMessageAllOf &&
        propositionId == other.propositionId &&
        proposerAccountId == other.proposerAccountId;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, propositionId.hashCode), proposerAccountId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PropositionChatMessageAllOf')
          ..add('propositionId', propositionId)
          ..add('proposerAccountId', proposerAccountId))
        .toString();
  }
}

class PropositionChatMessageAllOfBuilder
    implements
        Builder<PropositionChatMessageAllOf,
            PropositionChatMessageAllOfBuilder> {
  _$PropositionChatMessageAllOf? _$v;

  String? _propositionId;
  String? get propositionId => _$this._propositionId;
  set propositionId(String? propositionId) =>
      _$this._propositionId = propositionId;

  String? _proposerAccountId;
  String? get proposerAccountId => _$this._proposerAccountId;
  set proposerAccountId(String? proposerAccountId) =>
      _$this._proposerAccountId = proposerAccountId;

  PropositionChatMessageAllOfBuilder() {
    PropositionChatMessageAllOf._initializeBuilder(this);
  }

  PropositionChatMessageAllOfBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _propositionId = $v.propositionId;
      _proposerAccountId = $v.proposerAccountId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PropositionChatMessageAllOf other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PropositionChatMessageAllOf;
  }

  @override
  void update(void Function(PropositionChatMessageAllOfBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PropositionChatMessageAllOf build() {
    final _$result = _$v ??
        new _$PropositionChatMessageAllOf._(
            propositionId: BuiltValueNullFieldError.checkNotNull(
                propositionId, 'PropositionChatMessageAllOf', 'propositionId'),
            proposerAccountId: BuiltValueNullFieldError.checkNotNull(
                proposerAccountId,
                'PropositionChatMessageAllOf',
                'proposerAccountId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
