//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/friend_type.dart';
import 'package:nine_travl_api/src/model/friend_process_type.dart';
import 'package:nine_travl_api/src/model/offline_friend.dart';
import 'package:nine_travl_api/src/model/friend_ship_status.dart';
import 'package:nine_travl_api/src/model/online_friend.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'friend.g.dart';

abstract class Friend implements Built<Friend, FriendBuilder> {
  @BuiltValueField(wireName: r'id')
  String get id;

  @BuiltValueField(wireName: r'creationDate')
  int get creationDate;

  @BuiltValueField(wireName: r'status')
  FriendShipStatus? get status;
  // enum statusEnum {  ACCEPTED,  REFUSED,  PENDING,  };

  @BuiltValueField(wireName: r'type')
  FriendType? get type;
  // enum typeEnum {  RECEIVER_ONLY,  SENDER_ONLY,  FULL,  };

  @BuiltValueField(wireName: r'processType')
  FriendProcessType get processType;
  // enum processTypeEnum {  OFFLINE,  ONLINE,  };

  @BuiltValueField(wireName: r'accountId')
  String? get accountId;

  @BuiltValueField(wireName: r'authorId')
  String? get authorId;

  @BuiltValueField(wireName: r'isUserActive')
  bool? get isUserActive;

  @BuiltValueField(wireName: r'phoneNumber')
  String? get phoneNumber;

  @BuiltValueField(wireName: r'description')
  String? get description;

  @BuiltValueField(wireName: r'firstName')
  String? get firstName;

  @BuiltValueField(wireName: r'lastName')
  String? get lastName;

  @BuiltValueField(wireName: r'avatar')
  String? get avatar;

  Friend._();

  static void _initializeBuilder(FriendBuilder b) => b;

  factory Friend([void updates(FriendBuilder b)]) = _$Friend;

  @BuiltValueSerializer(custom: true)
  static Serializer<Friend> get serializer => _$FriendSerializer();
}

class _$FriendSerializer implements StructuredSerializer<Friend> {
  @override
  final Iterable<Type> types = const [Friend, _$Friend];

  @override
  final String wireName = r'Friend';

  @override
  Iterable<Object?> serialize(Serializers serializers, Friend object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'id')
      ..add(serializers.serialize(object.id,
          specifiedType: const FullType(String)));
    result
      ..add(r'creationDate')
      ..add(serializers.serialize(object.creationDate,
          specifiedType: const FullType(int)));
    if (object.status != null) {
      result
        ..add(r'status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(FriendShipStatus)));
    }
    if (object.type != null) {
      result
        ..add(r'type')
        ..add(serializers.serialize(object.type,
            specifiedType: const FullType(FriendType)));
    }
    result
      ..add(r'processType')
      ..add(serializers.serialize(object.processType,
          specifiedType: const FullType(FriendProcessType)));
    if (object.accountId != null) {
      result
        ..add(r'accountId')
        ..add(serializers.serialize(object.accountId,
            specifiedType: const FullType(String)));
    }
    if (object.authorId != null) {
      result
        ..add(r'authorId')
        ..add(serializers.serialize(object.authorId,
            specifiedType: const FullType(String)));
    }
    if (object.isUserActive != null) {
      result
        ..add(r'isUserActive')
        ..add(serializers.serialize(object.isUserActive,
            specifiedType: const FullType(bool)));
    }
    if (object.phoneNumber != null) {
      result
        ..add(r'phoneNumber')
        ..add(serializers.serialize(object.phoneNumber,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add(r'description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.firstName != null) {
      result
        ..add(r'firstName')
        ..add(serializers.serialize(object.firstName,
            specifiedType: const FullType(String)));
    }
    if (object.lastName != null) {
      result
        ..add(r'lastName')
        ..add(serializers.serialize(object.lastName,
            specifiedType: const FullType(String)));
    }
    if (object.avatar != null) {
      result
        ..add(r'avatar')
        ..add(serializers.serialize(object.avatar,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  Friend deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = FriendBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'creationDate':
          result.creationDate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'status':
          result.status = serializers.deserialize(value,
                  specifiedType: const FullType(FriendShipStatus))
              as FriendShipStatus;
          break;
        case r'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(FriendType)) as FriendType;
          break;
        case r'processType':
          result.processType = serializers.deserialize(value,
                  specifiedType: const FullType(FriendProcessType))
              as FriendProcessType;
          break;
        case r'accountId':
          result.accountId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'authorId':
          result.authorId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'isUserActive':
          result.isUserActive = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case r'phoneNumber':
          result.phoneNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'firstName':
          result.firstName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'lastName':
          result.lastName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'avatar':
          result.avatar = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
