// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ReportType _$TRAVEL = const ReportType._('TRAVEL');
const ReportType _$ACCOUNT = const ReportType._('ACCOUNT');
const ReportType _$BUG = const ReportType._('BUG');

ReportType _$valueOf(String name) {
  switch (name) {
    case 'TRAVEL':
      return _$TRAVEL;
    case 'ACCOUNT':
      return _$ACCOUNT;
    case 'BUG':
      return _$BUG;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ReportType> _$values =
    new BuiltSet<ReportType>(const <ReportType>[
  _$TRAVEL,
  _$ACCOUNT,
  _$BUG,
]);

class _$ReportTypeMeta {
  const _$ReportTypeMeta();
  ReportType get TRAVEL => _$TRAVEL;
  ReportType get ACCOUNT => _$ACCOUNT;
  ReportType get BUG => _$BUG;
  ReportType valueOf(String name) => _$valueOf(name);
  BuiltSet<ReportType> get values => _$values;
}

abstract class _$ReportTypeMixin {
  // ignore: non_constant_identifier_names
  _$ReportTypeMeta get ReportType => const _$ReportTypeMeta();
}

Serializer<ReportType> _$reportTypeSerializer = new _$ReportTypeSerializer();

class _$ReportTypeSerializer implements PrimitiveSerializer<ReportType> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'TRAVEL': 'TRAVEL',
    'ACCOUNT': 'ACCOUNT',
    'BUG': 'BUG',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'TRAVEL': 'TRAVEL',
    'ACCOUNT': 'ACCOUNT',
    'BUG': 'BUG',
  };

  @override
  final Iterable<Type> types = const <Type>[ReportType];
  @override
  final String wireName = 'ReportType';

  @override
  Object serialize(Serializers serializers, ReportType object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ReportType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ReportType.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
