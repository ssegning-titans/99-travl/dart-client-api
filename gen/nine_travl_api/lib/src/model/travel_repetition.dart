//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/travel_days.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'travel_repetition.g.dart';

abstract class TravelRepetition
    implements Built<TravelRepetition, TravelRepetitionBuilder> {
  @BuiltValueField(wireName: r'startingRepetitionDate')
  int get startingRepetitionDate;

  @BuiltValueField(wireName: r'endingRepetitionDate')
  int get endingRepetitionDate;

  @BuiltValueField(wireName: r'days')
  BuiltList<TravelDays> get days;

  TravelRepetition._();

  static void _initializeBuilder(TravelRepetitionBuilder b) => b;

  factory TravelRepetition([void updates(TravelRepetitionBuilder b)]) =
      _$TravelRepetition;

  @BuiltValueSerializer(custom: true)
  static Serializer<TravelRepetition> get serializer =>
      _$TravelRepetitionSerializer();
}

class _$TravelRepetitionSerializer
    implements StructuredSerializer<TravelRepetition> {
  @override
  final Iterable<Type> types = const [TravelRepetition, _$TravelRepetition];

  @override
  final String wireName = r'TravelRepetition';

  @override
  Iterable<Object?> serialize(Serializers serializers, TravelRepetition object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'startingRepetitionDate')
      ..add(serializers.serialize(object.startingRepetitionDate,
          specifiedType: const FullType(int)));
    result
      ..add(r'endingRepetitionDate')
      ..add(serializers.serialize(object.endingRepetitionDate,
          specifiedType: const FullType(int)));
    result
      ..add(r'days')
      ..add(serializers.serialize(object.days,
          specifiedType: const FullType(BuiltList, [FullType(TravelDays)])));
    return result;
  }

  @override
  TravelRepetition deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = TravelRepetitionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'startingRepetitionDate':
          result.startingRepetitionDate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'endingRepetitionDate':
          result.endingRepetitionDate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'days':
          result.days.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, [FullType(TravelDays)]))
              as BuiltList<TravelDays>);
          break;
      }
    }
    return result.build();
  }
}
