// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_media.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ChatMedia extends ChatMedia {
  @override
  final String? id;
  @override
  final int? creationDate;
  @override
  final String? documentId;
  @override
  final String? path;
  @override
  final String? name;
  @override
  final String? description;
  @override
  final BuiltMap<String, String>? metaData;
  @override
  final int width;
  @override
  final int height;
  @override
  final int? priority;

  factory _$ChatMedia([void Function(ChatMediaBuilder)? updates]) =>
      (new ChatMediaBuilder()..update(updates)).build();

  _$ChatMedia._(
      {this.id,
      this.creationDate,
      this.documentId,
      this.path,
      this.name,
      this.description,
      this.metaData,
      required this.width,
      required this.height,
      this.priority})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(width, 'ChatMedia', 'width');
    BuiltValueNullFieldError.checkNotNull(height, 'ChatMedia', 'height');
  }

  @override
  ChatMedia rebuild(void Function(ChatMediaBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChatMediaBuilder toBuilder() => new ChatMediaBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ChatMedia &&
        id == other.id &&
        creationDate == other.creationDate &&
        documentId == other.documentId &&
        path == other.path &&
        name == other.name &&
        description == other.description &&
        metaData == other.metaData &&
        width == other.width &&
        height == other.height &&
        priority == other.priority;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc($jc(0, id.hashCode),
                                        creationDate.hashCode),
                                    documentId.hashCode),
                                path.hashCode),
                            name.hashCode),
                        description.hashCode),
                    metaData.hashCode),
                width.hashCode),
            height.hashCode),
        priority.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ChatMedia')
          ..add('id', id)
          ..add('creationDate', creationDate)
          ..add('documentId', documentId)
          ..add('path', path)
          ..add('name', name)
          ..add('description', description)
          ..add('metaData', metaData)
          ..add('width', width)
          ..add('height', height)
          ..add('priority', priority))
        .toString();
  }
}

class ChatMediaBuilder implements Builder<ChatMedia, ChatMediaBuilder> {
  _$ChatMedia? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _creationDate;
  int? get creationDate => _$this._creationDate;
  set creationDate(int? creationDate) => _$this._creationDate = creationDate;

  String? _documentId;
  String? get documentId => _$this._documentId;
  set documentId(String? documentId) => _$this._documentId = documentId;

  String? _path;
  String? get path => _$this._path;
  set path(String? path) => _$this._path = path;

  String? _name;
  String? get name => _$this._name;
  set name(String? name) => _$this._name = name;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  MapBuilder<String, String>? _metaData;
  MapBuilder<String, String> get metaData =>
      _$this._metaData ??= new MapBuilder<String, String>();
  set metaData(MapBuilder<String, String>? metaData) =>
      _$this._metaData = metaData;

  int? _width;
  int? get width => _$this._width;
  set width(int? width) => _$this._width = width;

  int? _height;
  int? get height => _$this._height;
  set height(int? height) => _$this._height = height;

  int? _priority;
  int? get priority => _$this._priority;
  set priority(int? priority) => _$this._priority = priority;

  ChatMediaBuilder() {
    ChatMedia._initializeBuilder(this);
  }

  ChatMediaBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _creationDate = $v.creationDate;
      _documentId = $v.documentId;
      _path = $v.path;
      _name = $v.name;
      _description = $v.description;
      _metaData = $v.metaData?.toBuilder();
      _width = $v.width;
      _height = $v.height;
      _priority = $v.priority;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ChatMedia other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ChatMedia;
  }

  @override
  void update(void Function(ChatMediaBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ChatMedia build() {
    _$ChatMedia _$result;
    try {
      _$result = _$v ??
          new _$ChatMedia._(
              id: id,
              creationDate: creationDate,
              documentId: documentId,
              path: path,
              name: name,
              description: description,
              metaData: _metaData?.build(),
              width: BuiltValueNullFieldError.checkNotNull(
                  width, 'ChatMedia', 'width'),
              height: BuiltValueNullFieldError.checkNotNull(
                  height, 'ChatMedia', 'height'),
              priority: priority);
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'metaData';
        _metaData?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'ChatMedia', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
