//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'friend_process_type.g.dart';

class FriendProcessType extends EnumClass {
  @BuiltValueEnumConst(wireName: r'OFFLINE')
  static const FriendProcessType OFFLINE = _$OFFLINE;
  @BuiltValueEnumConst(wireName: r'ONLINE')
  static const FriendProcessType ONLINE = _$ONLINE;

  static Serializer<FriendProcessType> get serializer =>
      _$friendProcessTypeSerializer;

  const FriendProcessType._(String name) : super(name);

  static BuiltSet<FriendProcessType> get values => _$values;
  static FriendProcessType valueOf(String name) => _$valueOf(name);
}

/// Optionally, enum_class can generate a mixin to go with your enum for use
/// with Angular. It exposes your enum constants as getters. So, if you mix it
/// in to your Dart component class, the values become available to the
/// corresponding Angular template.
///
/// Trigger mixin generation by writing a line like this one next to your enum.
abstract class FriendProcessTypeMixin = Object with _$FriendProcessTypeMixin;
