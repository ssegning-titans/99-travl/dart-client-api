// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_account_all_of.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ReportAccountAllOf extends ReportAccountAllOf {
  @override
  final String accountId;

  factory _$ReportAccountAllOf(
          [void Function(ReportAccountAllOfBuilder)? updates]) =>
      (new ReportAccountAllOfBuilder()..update(updates)).build();

  _$ReportAccountAllOf._({required this.accountId}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        accountId, 'ReportAccountAllOf', 'accountId');
  }

  @override
  ReportAccountAllOf rebuild(
          void Function(ReportAccountAllOfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ReportAccountAllOfBuilder toBuilder() =>
      new ReportAccountAllOfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ReportAccountAllOf && accountId == other.accountId;
  }

  @override
  int get hashCode {
    return $jf($jc(0, accountId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ReportAccountAllOf')
          ..add('accountId', accountId))
        .toString();
  }
}

class ReportAccountAllOfBuilder
    implements Builder<ReportAccountAllOf, ReportAccountAllOfBuilder> {
  _$ReportAccountAllOf? _$v;

  String? _accountId;
  String? get accountId => _$this._accountId;
  set accountId(String? accountId) => _$this._accountId = accountId;

  ReportAccountAllOfBuilder() {
    ReportAccountAllOf._initializeBuilder(this);
  }

  ReportAccountAllOfBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _accountId = $v.accountId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ReportAccountAllOf other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ReportAccountAllOf;
  }

  @override
  void update(void Function(ReportAccountAllOfBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ReportAccountAllOf build() {
    final _$result = _$v ??
        new _$ReportAccountAllOf._(
            accountId: BuiltValueNullFieldError.checkNotNull(
                accountId, 'ReportAccountAllOf', 'accountId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
