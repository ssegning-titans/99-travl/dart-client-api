// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stripe_payment_intent_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$StripePaymentIntentRequest extends StripePaymentIntentRequest {
  @override
  final String? travelId;
  @override
  final int? quantity;

  factory _$StripePaymentIntentRequest(
          [void Function(StripePaymentIntentRequestBuilder)? updates]) =>
      (new StripePaymentIntentRequestBuilder()..update(updates)).build();

  _$StripePaymentIntentRequest._({this.travelId, this.quantity}) : super._();

  @override
  StripePaymentIntentRequest rebuild(
          void Function(StripePaymentIntentRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  StripePaymentIntentRequestBuilder toBuilder() =>
      new StripePaymentIntentRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is StripePaymentIntentRequest &&
        travelId == other.travelId &&
        quantity == other.quantity;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, travelId.hashCode), quantity.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('StripePaymentIntentRequest')
          ..add('travelId', travelId)
          ..add('quantity', quantity))
        .toString();
  }
}

class StripePaymentIntentRequestBuilder
    implements
        Builder<StripePaymentIntentRequest, StripePaymentIntentRequestBuilder> {
  _$StripePaymentIntentRequest? _$v;

  String? _travelId;
  String? get travelId => _$this._travelId;
  set travelId(String? travelId) => _$this._travelId = travelId;

  int? _quantity;
  int? get quantity => _$this._quantity;
  set quantity(int? quantity) => _$this._quantity = quantity;

  StripePaymentIntentRequestBuilder() {
    StripePaymentIntentRequest._initializeBuilder(this);
  }

  StripePaymentIntentRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _travelId = $v.travelId;
      _quantity = $v.quantity;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(StripePaymentIntentRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$StripePaymentIntentRequest;
  }

  @override
  void update(void Function(StripePaymentIntentRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$StripePaymentIntentRequest build() {
    final _$result = _$v ??
        new _$StripePaymentIntentRequest._(
            travelId: travelId, quantity: quantity);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
