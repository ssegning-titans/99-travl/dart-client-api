// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'travel_mode_unit.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const TravelModeUnit _$PLANE = const TravelModeUnit._('PLANE');
const TravelModeUnit _$CAR = const TravelModeUnit._('CAR');
const TravelModeUnit _$BOAT = const TravelModeUnit._('BOAT');

TravelModeUnit _$valueOf(String name) {
  switch (name) {
    case 'PLANE':
      return _$PLANE;
    case 'CAR':
      return _$CAR;
    case 'BOAT':
      return _$BOAT;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<TravelModeUnit> _$values =
    new BuiltSet<TravelModeUnit>(const <TravelModeUnit>[
  _$PLANE,
  _$CAR,
  _$BOAT,
]);

class _$TravelModeUnitMeta {
  const _$TravelModeUnitMeta();
  TravelModeUnit get PLANE => _$PLANE;
  TravelModeUnit get CAR => _$CAR;
  TravelModeUnit get BOAT => _$BOAT;
  TravelModeUnit valueOf(String name) => _$valueOf(name);
  BuiltSet<TravelModeUnit> get values => _$values;
}

abstract class _$TravelModeUnitMixin {
  // ignore: non_constant_identifier_names
  _$TravelModeUnitMeta get TravelModeUnit => const _$TravelModeUnitMeta();
}

Serializer<TravelModeUnit> _$travelModeUnitSerializer =
    new _$TravelModeUnitSerializer();

class _$TravelModeUnitSerializer
    implements PrimitiveSerializer<TravelModeUnit> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'PLANE': 'PLANE',
    'CAR': 'CAR',
    'BOAT': 'BOAT',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'PLANE': 'PLANE',
    'CAR': 'CAR',
    'BOAT': 'BOAT',
  };

  @override
  final Iterable<Type> types = const <Type>[TravelModeUnit];
  @override
  final String wireName = 'TravelModeUnit';

  @override
  Object serialize(Serializers serializers, TravelModeUnit object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  TravelModeUnit deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      TravelModeUnit.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
