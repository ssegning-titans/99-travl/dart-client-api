//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'routing_map_place_all_of.g.dart';

abstract class RoutingMapPlaceAllOf
    implements Built<RoutingMapPlaceAllOf, RoutingMapPlaceAllOfBuilder> {
  @BuiltValueField(wireName: r'order')
  double? get order;

  RoutingMapPlaceAllOf._();

  static void _initializeBuilder(RoutingMapPlaceAllOfBuilder b) => b;

  factory RoutingMapPlaceAllOf([void updates(RoutingMapPlaceAllOfBuilder b)]) =
      _$RoutingMapPlaceAllOf;

  @BuiltValueSerializer(custom: true)
  static Serializer<RoutingMapPlaceAllOf> get serializer =>
      _$RoutingMapPlaceAllOfSerializer();
}

class _$RoutingMapPlaceAllOfSerializer
    implements StructuredSerializer<RoutingMapPlaceAllOf> {
  @override
  final Iterable<Type> types = const [
    RoutingMapPlaceAllOf,
    _$RoutingMapPlaceAllOf
  ];

  @override
  final String wireName = r'RoutingMapPlaceAllOf';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, RoutingMapPlaceAllOf object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.order != null) {
      result
        ..add(r'order')
        ..add(serializers.serialize(object.order,
            specifiedType: const FullType(double)));
    }
    return result;
  }

  @override
  RoutingMapPlaceAllOf deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = RoutingMapPlaceAllOfBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'order':
          result.order = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }
    return result.build();
  }
}
