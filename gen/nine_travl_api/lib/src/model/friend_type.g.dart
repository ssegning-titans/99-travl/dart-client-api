// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friend_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const FriendType _$RECEIVER_ONLY = const FriendType._('RECEIVER_ONLY');
const FriendType _$SENDER_ONLY = const FriendType._('SENDER_ONLY');
const FriendType _$FULL = const FriendType._('FULL');

FriendType _$valueOf(String name) {
  switch (name) {
    case 'RECEIVER_ONLY':
      return _$RECEIVER_ONLY;
    case 'SENDER_ONLY':
      return _$SENDER_ONLY;
    case 'FULL':
      return _$FULL;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<FriendType> _$values =
    new BuiltSet<FriendType>(const <FriendType>[
  _$RECEIVER_ONLY,
  _$SENDER_ONLY,
  _$FULL,
]);

class _$FriendTypeMeta {
  const _$FriendTypeMeta();
  FriendType get RECEIVER_ONLY => _$RECEIVER_ONLY;
  FriendType get SENDER_ONLY => _$SENDER_ONLY;
  FriendType get FULL => _$FULL;
  FriendType valueOf(String name) => _$valueOf(name);
  BuiltSet<FriendType> get values => _$values;
}

abstract class _$FriendTypeMixin {
  // ignore: non_constant_identifier_names
  _$FriendTypeMeta get FriendType => const _$FriendTypeMeta();
}

Serializer<FriendType> _$friendTypeSerializer = new _$FriendTypeSerializer();

class _$FriendTypeSerializer implements PrimitiveSerializer<FriendType> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'RECEIVER_ONLY': 'RECEIVER_ONLY',
    'SENDER_ONLY': 'SENDER_ONLY',
    'FULL': 'FULL',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'RECEIVER_ONLY': 'RECEIVER_ONLY',
    'SENDER_ONLY': 'SENDER_ONLY',
    'FULL': 'FULL',
  };

  @override
  final Iterable<Type> types = const <Type>[FriendType];
  @override
  final String wireName = 'FriendType';

  @override
  Object serialize(Serializers serializers, FriendType object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  FriendType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      FriendType.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
