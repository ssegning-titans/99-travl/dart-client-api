//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'stripe_payment_intent_request.g.dart';

abstract class StripePaymentIntentRequest
    implements
        Built<StripePaymentIntentRequest, StripePaymentIntentRequestBuilder> {
  @BuiltValueField(wireName: r'travelId')
  String? get travelId;

  @BuiltValueField(wireName: r'quantity')
  int? get quantity;

  StripePaymentIntentRequest._();

  static void _initializeBuilder(StripePaymentIntentRequestBuilder b) => b;

  factory StripePaymentIntentRequest(
          [void updates(StripePaymentIntentRequestBuilder b)]) =
      _$StripePaymentIntentRequest;

  @BuiltValueSerializer(custom: true)
  static Serializer<StripePaymentIntentRequest> get serializer =>
      _$StripePaymentIntentRequestSerializer();
}

class _$StripePaymentIntentRequestSerializer
    implements StructuredSerializer<StripePaymentIntentRequest> {
  @override
  final Iterable<Type> types = const [
    StripePaymentIntentRequest,
    _$StripePaymentIntentRequest
  ];

  @override
  final String wireName = r'StripePaymentIntentRequest';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, StripePaymentIntentRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.travelId != null) {
      result
        ..add(r'travelId')
        ..add(serializers.serialize(object.travelId,
            specifiedType: const FullType(String)));
    }
    if (object.quantity != null) {
      result
        ..add(r'quantity')
        ..add(serializers.serialize(object.quantity,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  StripePaymentIntentRequest deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = StripePaymentIntentRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'travelId':
          result.travelId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'quantity':
          result.quantity = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }
    return result.build();
  }
}
