//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/account_status.dart';
import 'package:nine_travl_api/src/model/account_type.dart';
import 'package:nine_travl_api/src/model/account_gender.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'change_personal_data_request.g.dart';

abstract class ChangePersonalDataRequest
    implements
        Built<ChangePersonalDataRequest, ChangePersonalDataRequestBuilder> {
  @BuiltValueField(wireName: r'email')
  String? get email;

  @BuiltValueField(wireName: r'emailVerified')
  bool? get emailVerified;

  @BuiltValueField(wireName: r'firstName')
  String? get firstName;

  @BuiltValueField(wireName: r'lastName')
  String? get lastName;

  @BuiltValueField(wireName: r'bio')
  String? get bio;

  @BuiltValueField(wireName: r'phoneNumber')
  String? get phoneNumber;

  @BuiltValueField(wireName: r'avatarUrl')
  String? get avatarUrl;

  @BuiltValueField(wireName: r'phoneNumberValid')
  bool? get phoneNumberValid;

  @BuiltValueField(wireName: r'locale')
  String? get locale;

  @BuiltValueField(wireName: r'gender')
  AccountGender? get gender;
  // enum genderEnum {  MALE,  FEMALE,  OTHER,  };

  @BuiltValueField(wireName: r'accountType')
  AccountType? get accountType;
  // enum accountTypeEnum {  C2C,  B2C,  B2B,  };

  @BuiltValueField(wireName: r'status')
  AccountStatus? get status;
  // enum statusEnum {  ALLOWED,  BLOCKED,  };

  ChangePersonalDataRequest._();

  static void _initializeBuilder(ChangePersonalDataRequestBuilder b) => b;

  factory ChangePersonalDataRequest(
          [void updates(ChangePersonalDataRequestBuilder b)]) =
      _$ChangePersonalDataRequest;

  @BuiltValueSerializer(custom: true)
  static Serializer<ChangePersonalDataRequest> get serializer =>
      _$ChangePersonalDataRequestSerializer();
}

class _$ChangePersonalDataRequestSerializer
    implements StructuredSerializer<ChangePersonalDataRequest> {
  @override
  final Iterable<Type> types = const [
    ChangePersonalDataRequest,
    _$ChangePersonalDataRequest
  ];

  @override
  final String wireName = r'ChangePersonalDataRequest';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, ChangePersonalDataRequest object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.email != null) {
      result
        ..add(r'email')
        ..add(serializers.serialize(object.email,
            specifiedType: const FullType(String)));
    }
    if (object.emailVerified != null) {
      result
        ..add(r'emailVerified')
        ..add(serializers.serialize(object.emailVerified,
            specifiedType: const FullType(bool)));
    }
    if (object.firstName != null) {
      result
        ..add(r'firstName')
        ..add(serializers.serialize(object.firstName,
            specifiedType: const FullType(String)));
    }
    if (object.lastName != null) {
      result
        ..add(r'lastName')
        ..add(serializers.serialize(object.lastName,
            specifiedType: const FullType(String)));
    }
    if (object.bio != null) {
      result
        ..add(r'bio')
        ..add(serializers.serialize(object.bio,
            specifiedType: const FullType(String)));
    }
    if (object.phoneNumber != null) {
      result
        ..add(r'phoneNumber')
        ..add(serializers.serialize(object.phoneNumber,
            specifiedType: const FullType(String)));
    }
    if (object.avatarUrl != null) {
      result
        ..add(r'avatarUrl')
        ..add(serializers.serialize(object.avatarUrl,
            specifiedType: const FullType(String)));
    }
    if (object.phoneNumberValid != null) {
      result
        ..add(r'phoneNumberValid')
        ..add(serializers.serialize(object.phoneNumberValid,
            specifiedType: const FullType(bool)));
    }
    if (object.locale != null) {
      result
        ..add(r'locale')
        ..add(serializers.serialize(object.locale,
            specifiedType: const FullType(String)));
    }
    if (object.gender != null) {
      result
        ..add(r'gender')
        ..add(serializers.serialize(object.gender,
            specifiedType: const FullType(AccountGender)));
    }
    if (object.accountType != null) {
      result
        ..add(r'accountType')
        ..add(serializers.serialize(object.accountType,
            specifiedType: const FullType(AccountType)));
    }
    if (object.status != null) {
      result
        ..add(r'status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(AccountStatus)));
    }
    return result;
  }

  @override
  ChangePersonalDataRequest deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = ChangePersonalDataRequestBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'email':
          result.email = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'emailVerified':
          result.emailVerified = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case r'firstName':
          result.firstName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'lastName':
          result.lastName = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'bio':
          result.bio = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'phoneNumber':
          result.phoneNumber = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'avatarUrl':
          result.avatarUrl = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'phoneNumberValid':
          result.phoneNumberValid = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case r'locale':
          result.locale = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'gender':
          result.gender = serializers.deserialize(value,
              specifiedType: const FullType(AccountGender)) as AccountGender;
          break;
        case r'accountType':
          result.accountType = serializers.deserialize(value,
              specifiedType: const FullType(AccountType)) as AccountType;
          break;
        case r'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(AccountStatus)) as AccountStatus;
          break;
      }
    }
    return result.build();
  }
}
