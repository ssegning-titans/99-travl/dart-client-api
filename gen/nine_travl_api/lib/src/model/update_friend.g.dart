// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_friend.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$UpdateFriend extends UpdateFriend {
  @override
  final FriendType? type;

  factory _$UpdateFriend([void Function(UpdateFriendBuilder)? updates]) =>
      (new UpdateFriendBuilder()..update(updates)).build();

  _$UpdateFriend._({this.type}) : super._();

  @override
  UpdateFriend rebuild(void Function(UpdateFriendBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  UpdateFriendBuilder toBuilder() => new UpdateFriendBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is UpdateFriend && type == other.type;
  }

  @override
  int get hashCode {
    return $jf($jc(0, type.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('UpdateFriend')..add('type', type))
        .toString();
  }
}

class UpdateFriendBuilder
    implements Builder<UpdateFriend, UpdateFriendBuilder> {
  _$UpdateFriend? _$v;

  FriendType? _type;
  FriendType? get type => _$this._type;
  set type(FriendType? type) => _$this._type = type;

  UpdateFriendBuilder() {
    UpdateFriend._initializeBuilder(this);
  }

  UpdateFriendBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _type = $v.type;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(UpdateFriend other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$UpdateFriend;
  }

  @override
  void update(void Function(UpdateFriendBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$UpdateFriend build() {
    final _$result = _$v ?? new _$UpdateFriend._(type: type);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
