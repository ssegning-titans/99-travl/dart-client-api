//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'average_rating_person.g.dart';

abstract class AverageRatingPerson
    implements Built<AverageRatingPerson, AverageRatingPersonBuilder> {
  @BuiltValueField(wireName: r'value')
  double get value;

  @BuiltValueField(wireName: r'count')
  int get count;

  @BuiltValueField(wireName: r'personId')
  String get personId;

  AverageRatingPerson._();

  static void _initializeBuilder(AverageRatingPersonBuilder b) => b;

  factory AverageRatingPerson([void updates(AverageRatingPersonBuilder b)]) =
      _$AverageRatingPerson;

  @BuiltValueSerializer(custom: true)
  static Serializer<AverageRatingPerson> get serializer =>
      _$AverageRatingPersonSerializer();
}

class _$AverageRatingPersonSerializer
    implements StructuredSerializer<AverageRatingPerson> {
  @override
  final Iterable<Type> types = const [
    AverageRatingPerson,
    _$AverageRatingPerson
  ];

  @override
  final String wireName = r'AverageRatingPerson';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, AverageRatingPerson object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'value')
      ..add(serializers.serialize(object.value,
          specifiedType: const FullType(double)));
    result
      ..add(r'count')
      ..add(serializers.serialize(object.count,
          specifiedType: const FullType(int)));
    result
      ..add(r'personId')
      ..add(serializers.serialize(object.personId,
          specifiedType: const FullType(String)));
    return result;
  }

  @override
  AverageRatingPerson deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = AverageRatingPersonBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case r'count':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'personId':
          result.personId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
