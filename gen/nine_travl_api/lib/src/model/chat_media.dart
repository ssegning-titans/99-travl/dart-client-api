//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'chat_media.g.dart';

abstract class ChatMedia implements Built<ChatMedia, ChatMediaBuilder> {
  @BuiltValueField(wireName: r'id')
  String? get id;

  @BuiltValueField(wireName: r'creationDate')
  int? get creationDate;

  @BuiltValueField(wireName: r'documentId')
  String? get documentId;

  @BuiltValueField(wireName: r'path')
  String? get path;

  @BuiltValueField(wireName: r'name')
  String? get name;

  @BuiltValueField(wireName: r'description')
  String? get description;

  @BuiltValueField(wireName: r'metaData')
  BuiltMap<String, String>? get metaData;

  @BuiltValueField(wireName: r'width')
  int get width;

  @BuiltValueField(wireName: r'height')
  int get height;

  @BuiltValueField(wireName: r'priority')
  int? get priority;

  ChatMedia._();

  static void _initializeBuilder(ChatMediaBuilder b) => b
    ..width = 1
    ..height = 1
    ..priority = 1;

  factory ChatMedia([void updates(ChatMediaBuilder b)]) = _$ChatMedia;

  @BuiltValueSerializer(custom: true)
  static Serializer<ChatMedia> get serializer => _$ChatMediaSerializer();
}

class _$ChatMediaSerializer implements StructuredSerializer<ChatMedia> {
  @override
  final Iterable<Type> types = const [ChatMedia, _$ChatMedia];

  @override
  final String wireName = r'ChatMedia';

  @override
  Iterable<Object?> serialize(Serializers serializers, ChatMedia object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.id != null) {
      result
        ..add(r'id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.creationDate != null) {
      result
        ..add(r'creationDate')
        ..add(serializers.serialize(object.creationDate,
            specifiedType: const FullType(int)));
    }
    if (object.documentId != null) {
      result
        ..add(r'documentId')
        ..add(serializers.serialize(object.documentId,
            specifiedType: const FullType(String)));
    }
    if (object.path != null) {
      result
        ..add(r'path')
        ..add(serializers.serialize(object.path,
            specifiedType: const FullType(String)));
    }
    if (object.name != null) {
      result
        ..add(r'name')
        ..add(serializers.serialize(object.name,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add(r'description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    if (object.metaData != null) {
      result
        ..add(r'metaData')
        ..add(serializers.serialize(object.metaData,
            specifiedType: const FullType(
                BuiltMap, [FullType(String), FullType(String)])));
    }
    result
      ..add(r'width')
      ..add(serializers.serialize(object.width,
          specifiedType: const FullType(int)));
    result
      ..add(r'height')
      ..add(serializers.serialize(object.height,
          specifiedType: const FullType(int)));
    if (object.priority != null) {
      result
        ..add(r'priority')
        ..add(serializers.serialize(object.priority,
            specifiedType: const FullType(int)));
    }
    return result;
  }

  @override
  ChatMedia deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = ChatMediaBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'creationDate':
          result.creationDate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'documentId':
          result.documentId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'path':
          result.path = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'name':
          result.name = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'metaData':
          result.metaData.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltMap, [FullType(String), FullType(String)]))
              as BuiltMap<String, String>);
          break;
        case r'width':
          result.width = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'height':
          result.height = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'priority':
          result.priority = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }
    return result.build();
  }
}
