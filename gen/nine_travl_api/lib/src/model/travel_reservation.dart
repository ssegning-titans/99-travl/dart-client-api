//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'travel_reservation.g.dart';

abstract class TravelReservation
    implements Built<TravelReservation, TravelReservationBuilder> {
  @BuiltValueField(wireName: r'id')
  String? get id;

  @BuiltValueField(wireName: r'creationDate')
  int? get creationDate;

  @BuiltValueField(wireName: r'travelId')
  String? get travelId;

  @BuiltValueField(wireName: r'startingReservationPoint')
  String? get startingReservationPoint;

  @BuiltValueField(wireName: r'endingReservationPoint')
  String? get endingReservationPoint;

  @BuiltValueField(wireName: r'reserverAccountId')
  String? get reserverAccountId;

  TravelReservation._();

  static void _initializeBuilder(TravelReservationBuilder b) => b;

  factory TravelReservation([void updates(TravelReservationBuilder b)]) =
      _$TravelReservation;

  @BuiltValueSerializer(custom: true)
  static Serializer<TravelReservation> get serializer =>
      _$TravelReservationSerializer();
}

class _$TravelReservationSerializer
    implements StructuredSerializer<TravelReservation> {
  @override
  final Iterable<Type> types = const [TravelReservation, _$TravelReservation];

  @override
  final String wireName = r'TravelReservation';

  @override
  Iterable<Object?> serialize(Serializers serializers, TravelReservation object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.id != null) {
      result
        ..add(r'id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.creationDate != null) {
      result
        ..add(r'creationDate')
        ..add(serializers.serialize(object.creationDate,
            specifiedType: const FullType(int)));
    }
    if (object.travelId != null) {
      result
        ..add(r'travelId')
        ..add(serializers.serialize(object.travelId,
            specifiedType: const FullType(String)));
    }
    if (object.startingReservationPoint != null) {
      result
        ..add(r'startingReservationPoint')
        ..add(serializers.serialize(object.startingReservationPoint,
            specifiedType: const FullType(String)));
    }
    if (object.endingReservationPoint != null) {
      result
        ..add(r'endingReservationPoint')
        ..add(serializers.serialize(object.endingReservationPoint,
            specifiedType: const FullType(String)));
    }
    if (object.reserverAccountId != null) {
      result
        ..add(r'reserverAccountId')
        ..add(serializers.serialize(object.reserverAccountId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  TravelReservation deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = TravelReservationBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'creationDate':
          result.creationDate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'travelId':
          result.travelId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'startingReservationPoint':
          result.startingReservationPoint = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'endingReservationPoint':
          result.endingReservationPoint = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'reserverAccountId':
          result.reserverAccountId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
