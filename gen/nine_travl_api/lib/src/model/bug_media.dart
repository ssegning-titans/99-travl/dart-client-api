//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'bug_media.g.dart';

abstract class BugMedia implements Built<BugMedia, BugMediaBuilder> {
  @BuiltValueField(wireName: r'id')
  String? get id;

  @BuiltValueField(wireName: r'creationDate')
  int? get creationDate;

  @BuiltValueField(wireName: r'documentId')
  String get documentId;

  @BuiltValueField(wireName: r'bugId')
  String? get bugId;

  BugMedia._();

  static void _initializeBuilder(BugMediaBuilder b) => b;

  factory BugMedia([void updates(BugMediaBuilder b)]) = _$BugMedia;

  @BuiltValueSerializer(custom: true)
  static Serializer<BugMedia> get serializer => _$BugMediaSerializer();
}

class _$BugMediaSerializer implements StructuredSerializer<BugMedia> {
  @override
  final Iterable<Type> types = const [BugMedia, _$BugMedia];

  @override
  final String wireName = r'BugMedia';

  @override
  Iterable<Object?> serialize(Serializers serializers, BugMedia object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.id != null) {
      result
        ..add(r'id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.creationDate != null) {
      result
        ..add(r'creationDate')
        ..add(serializers.serialize(object.creationDate,
            specifiedType: const FullType(int)));
    }
    result
      ..add(r'documentId')
      ..add(serializers.serialize(object.documentId,
          specifiedType: const FullType(String)));
    if (object.bugId != null) {
      result
        ..add(r'bugId')
        ..add(serializers.serialize(object.bugId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BugMedia deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = BugMediaBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'creationDate':
          result.creationDate = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'documentId':
          result.documentId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'bugId':
          result.bugId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
