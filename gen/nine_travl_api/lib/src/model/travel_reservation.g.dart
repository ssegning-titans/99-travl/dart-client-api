// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'travel_reservation.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$TravelReservation extends TravelReservation {
  @override
  final String? id;
  @override
  final int? creationDate;
  @override
  final String? travelId;
  @override
  final String? startingReservationPoint;
  @override
  final String? endingReservationPoint;
  @override
  final String? reserverAccountId;

  factory _$TravelReservation(
          [void Function(TravelReservationBuilder)? updates]) =>
      (new TravelReservationBuilder()..update(updates)).build();

  _$TravelReservation._(
      {this.id,
      this.creationDate,
      this.travelId,
      this.startingReservationPoint,
      this.endingReservationPoint,
      this.reserverAccountId})
      : super._();

  @override
  TravelReservation rebuild(void Function(TravelReservationBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TravelReservationBuilder toBuilder() =>
      new TravelReservationBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TravelReservation &&
        id == other.id &&
        creationDate == other.creationDate &&
        travelId == other.travelId &&
        startingReservationPoint == other.startingReservationPoint &&
        endingReservationPoint == other.endingReservationPoint &&
        reserverAccountId == other.reserverAccountId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, id.hashCode), creationDate.hashCode),
                    travelId.hashCode),
                startingReservationPoint.hashCode),
            endingReservationPoint.hashCode),
        reserverAccountId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TravelReservation')
          ..add('id', id)
          ..add('creationDate', creationDate)
          ..add('travelId', travelId)
          ..add('startingReservationPoint', startingReservationPoint)
          ..add('endingReservationPoint', endingReservationPoint)
          ..add('reserverAccountId', reserverAccountId))
        .toString();
  }
}

class TravelReservationBuilder
    implements Builder<TravelReservation, TravelReservationBuilder> {
  _$TravelReservation? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _creationDate;
  int? get creationDate => _$this._creationDate;
  set creationDate(int? creationDate) => _$this._creationDate = creationDate;

  String? _travelId;
  String? get travelId => _$this._travelId;
  set travelId(String? travelId) => _$this._travelId = travelId;

  String? _startingReservationPoint;
  String? get startingReservationPoint => _$this._startingReservationPoint;
  set startingReservationPoint(String? startingReservationPoint) =>
      _$this._startingReservationPoint = startingReservationPoint;

  String? _endingReservationPoint;
  String? get endingReservationPoint => _$this._endingReservationPoint;
  set endingReservationPoint(String? endingReservationPoint) =>
      _$this._endingReservationPoint = endingReservationPoint;

  String? _reserverAccountId;
  String? get reserverAccountId => _$this._reserverAccountId;
  set reserverAccountId(String? reserverAccountId) =>
      _$this._reserverAccountId = reserverAccountId;

  TravelReservationBuilder() {
    TravelReservation._initializeBuilder(this);
  }

  TravelReservationBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _creationDate = $v.creationDate;
      _travelId = $v.travelId;
      _startingReservationPoint = $v.startingReservationPoint;
      _endingReservationPoint = $v.endingReservationPoint;
      _reserverAccountId = $v.reserverAccountId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TravelReservation other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TravelReservation;
  }

  @override
  void update(void Function(TravelReservationBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TravelReservation build() {
    final _$result = _$v ??
        new _$TravelReservation._(
            id: id,
            creationDate: creationDate,
            travelId: travelId,
            startingReservationPoint: startingReservationPoint,
            endingReservationPoint: endingReservationPoint,
            reserverAccountId: reserverAccountId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
