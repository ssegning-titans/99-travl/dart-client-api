// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'base_friend.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$BaseFriend extends BaseFriend {
  @override
  final String id;
  @override
  final int creationDate;
  @override
  final FriendShipStatus? status;
  @override
  final FriendType? type;
  @override
  final FriendProcessType processType;

  factory _$BaseFriend([void Function(BaseFriendBuilder)? updates]) =>
      (new BaseFriendBuilder()..update(updates)).build();

  _$BaseFriend._(
      {required this.id,
      required this.creationDate,
      this.status,
      this.type,
      required this.processType})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(id, 'BaseFriend', 'id');
    BuiltValueNullFieldError.checkNotNull(
        creationDate, 'BaseFriend', 'creationDate');
    BuiltValueNullFieldError.checkNotNull(
        processType, 'BaseFriend', 'processType');
  }

  @override
  BaseFriend rebuild(void Function(BaseFriendBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  BaseFriendBuilder toBuilder() => new BaseFriendBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is BaseFriend &&
        id == other.id &&
        creationDate == other.creationDate &&
        status == other.status &&
        type == other.type &&
        processType == other.processType;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, id.hashCode), creationDate.hashCode),
                status.hashCode),
            type.hashCode),
        processType.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('BaseFriend')
          ..add('id', id)
          ..add('creationDate', creationDate)
          ..add('status', status)
          ..add('type', type)
          ..add('processType', processType))
        .toString();
  }
}

class BaseFriendBuilder implements Builder<BaseFriend, BaseFriendBuilder> {
  _$BaseFriend? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _creationDate;
  int? get creationDate => _$this._creationDate;
  set creationDate(int? creationDate) => _$this._creationDate = creationDate;

  FriendShipStatus? _status;
  FriendShipStatus? get status => _$this._status;
  set status(FriendShipStatus? status) => _$this._status = status;

  FriendType? _type;
  FriendType? get type => _$this._type;
  set type(FriendType? type) => _$this._type = type;

  FriendProcessType? _processType;
  FriendProcessType? get processType => _$this._processType;
  set processType(FriendProcessType? processType) =>
      _$this._processType = processType;

  BaseFriendBuilder() {
    BaseFriend._initializeBuilder(this);
  }

  BaseFriendBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _creationDate = $v.creationDate;
      _status = $v.status;
      _type = $v.type;
      _processType = $v.processType;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(BaseFriend other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$BaseFriend;
  }

  @override
  void update(void Function(BaseFriendBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$BaseFriend build() {
    final _$result = _$v ??
        new _$BaseFriend._(
            id: BuiltValueNullFieldError.checkNotNull(id, 'BaseFriend', 'id'),
            creationDate: BuiltValueNullFieldError.checkNotNull(
                creationDate, 'BaseFriend', 'creationDate'),
            status: status,
            type: type,
            processType: BuiltValueNullFieldError.checkNotNull(
                processType, 'BaseFriend', 'processType'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
