// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'change_personal_data_request.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ChangePersonalDataRequest extends ChangePersonalDataRequest {
  @override
  final String? email;
  @override
  final bool? emailVerified;
  @override
  final String? firstName;
  @override
  final String? lastName;
  @override
  final String? bio;
  @override
  final String? phoneNumber;
  @override
  final String? avatarUrl;
  @override
  final bool? phoneNumberValid;
  @override
  final String? locale;
  @override
  final AccountGender? gender;
  @override
  final AccountType? accountType;
  @override
  final AccountStatus? status;

  factory _$ChangePersonalDataRequest(
          [void Function(ChangePersonalDataRequestBuilder)? updates]) =>
      (new ChangePersonalDataRequestBuilder()..update(updates)).build();

  _$ChangePersonalDataRequest._(
      {this.email,
      this.emailVerified,
      this.firstName,
      this.lastName,
      this.bio,
      this.phoneNumber,
      this.avatarUrl,
      this.phoneNumberValid,
      this.locale,
      this.gender,
      this.accountType,
      this.status})
      : super._();

  @override
  ChangePersonalDataRequest rebuild(
          void Function(ChangePersonalDataRequestBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChangePersonalDataRequestBuilder toBuilder() =>
      new ChangePersonalDataRequestBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ChangePersonalDataRequest &&
        email == other.email &&
        emailVerified == other.emailVerified &&
        firstName == other.firstName &&
        lastName == other.lastName &&
        bio == other.bio &&
        phoneNumber == other.phoneNumber &&
        avatarUrl == other.avatarUrl &&
        phoneNumberValid == other.phoneNumberValid &&
        locale == other.locale &&
        gender == other.gender &&
        accountType == other.accountType &&
        status == other.status;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc($jc(0, email.hashCode),
                                                emailVerified.hashCode),
                                            firstName.hashCode),
                                        lastName.hashCode),
                                    bio.hashCode),
                                phoneNumber.hashCode),
                            avatarUrl.hashCode),
                        phoneNumberValid.hashCode),
                    locale.hashCode),
                gender.hashCode),
            accountType.hashCode),
        status.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ChangePersonalDataRequest')
          ..add('email', email)
          ..add('emailVerified', emailVerified)
          ..add('firstName', firstName)
          ..add('lastName', lastName)
          ..add('bio', bio)
          ..add('phoneNumber', phoneNumber)
          ..add('avatarUrl', avatarUrl)
          ..add('phoneNumberValid', phoneNumberValid)
          ..add('locale', locale)
          ..add('gender', gender)
          ..add('accountType', accountType)
          ..add('status', status))
        .toString();
  }
}

class ChangePersonalDataRequestBuilder
    implements
        Builder<ChangePersonalDataRequest, ChangePersonalDataRequestBuilder> {
  _$ChangePersonalDataRequest? _$v;

  String? _email;
  String? get email => _$this._email;
  set email(String? email) => _$this._email = email;

  bool? _emailVerified;
  bool? get emailVerified => _$this._emailVerified;
  set emailVerified(bool? emailVerified) =>
      _$this._emailVerified = emailVerified;

  String? _firstName;
  String? get firstName => _$this._firstName;
  set firstName(String? firstName) => _$this._firstName = firstName;

  String? _lastName;
  String? get lastName => _$this._lastName;
  set lastName(String? lastName) => _$this._lastName = lastName;

  String? _bio;
  String? get bio => _$this._bio;
  set bio(String? bio) => _$this._bio = bio;

  String? _phoneNumber;
  String? get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String? phoneNumber) => _$this._phoneNumber = phoneNumber;

  String? _avatarUrl;
  String? get avatarUrl => _$this._avatarUrl;
  set avatarUrl(String? avatarUrl) => _$this._avatarUrl = avatarUrl;

  bool? _phoneNumberValid;
  bool? get phoneNumberValid => _$this._phoneNumberValid;
  set phoneNumberValid(bool? phoneNumberValid) =>
      _$this._phoneNumberValid = phoneNumberValid;

  String? _locale;
  String? get locale => _$this._locale;
  set locale(String? locale) => _$this._locale = locale;

  AccountGender? _gender;
  AccountGender? get gender => _$this._gender;
  set gender(AccountGender? gender) => _$this._gender = gender;

  AccountType? _accountType;
  AccountType? get accountType => _$this._accountType;
  set accountType(AccountType? accountType) =>
      _$this._accountType = accountType;

  AccountStatus? _status;
  AccountStatus? get status => _$this._status;
  set status(AccountStatus? status) => _$this._status = status;

  ChangePersonalDataRequestBuilder() {
    ChangePersonalDataRequest._initializeBuilder(this);
  }

  ChangePersonalDataRequestBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _email = $v.email;
      _emailVerified = $v.emailVerified;
      _firstName = $v.firstName;
      _lastName = $v.lastName;
      _bio = $v.bio;
      _phoneNumber = $v.phoneNumber;
      _avatarUrl = $v.avatarUrl;
      _phoneNumberValid = $v.phoneNumberValid;
      _locale = $v.locale;
      _gender = $v.gender;
      _accountType = $v.accountType;
      _status = $v.status;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ChangePersonalDataRequest other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ChangePersonalDataRequest;
  }

  @override
  void update(void Function(ChangePersonalDataRequestBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ChangePersonalDataRequest build() {
    final _$result = _$v ??
        new _$ChangePersonalDataRequest._(
            email: email,
            emailVerified: emailVerified,
            firstName: firstName,
            lastName: lastName,
            bio: bio,
            phoneNumber: phoneNumber,
            avatarUrl: avatarUrl,
            phoneNumberValid: phoneNumberValid,
            locale: locale,
            gender: gender,
            accountType: accountType,
            status: status);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
