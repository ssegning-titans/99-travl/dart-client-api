// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request_offline_friend_ship.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$RequestOfflineFriendShip extends RequestOfflineFriendShip {
  @override
  final String? phoneNumber;
  @override
  final String? description;
  @override
  final String? firstName;
  @override
  final String? lastName;
  @override
  final String? avatar;

  factory _$RequestOfflineFriendShip(
          [void Function(RequestOfflineFriendShipBuilder)? updates]) =>
      (new RequestOfflineFriendShipBuilder()..update(updates)).build();

  _$RequestOfflineFriendShip._(
      {this.phoneNumber,
      this.description,
      this.firstName,
      this.lastName,
      this.avatar})
      : super._();

  @override
  RequestOfflineFriendShip rebuild(
          void Function(RequestOfflineFriendShipBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RequestOfflineFriendShipBuilder toBuilder() =>
      new RequestOfflineFriendShipBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RequestOfflineFriendShip &&
        phoneNumber == other.phoneNumber &&
        description == other.description &&
        firstName == other.firstName &&
        lastName == other.lastName &&
        avatar == other.avatar;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, phoneNumber.hashCode), description.hashCode),
                firstName.hashCode),
            lastName.hashCode),
        avatar.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RequestOfflineFriendShip')
          ..add('phoneNumber', phoneNumber)
          ..add('description', description)
          ..add('firstName', firstName)
          ..add('lastName', lastName)
          ..add('avatar', avatar))
        .toString();
  }
}

class RequestOfflineFriendShipBuilder
    implements
        Builder<RequestOfflineFriendShip, RequestOfflineFriendShipBuilder> {
  _$RequestOfflineFriendShip? _$v;

  String? _phoneNumber;
  String? get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String? phoneNumber) => _$this._phoneNumber = phoneNumber;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _firstName;
  String? get firstName => _$this._firstName;
  set firstName(String? firstName) => _$this._firstName = firstName;

  String? _lastName;
  String? get lastName => _$this._lastName;
  set lastName(String? lastName) => _$this._lastName = lastName;

  String? _avatar;
  String? get avatar => _$this._avatar;
  set avatar(String? avatar) => _$this._avatar = avatar;

  RequestOfflineFriendShipBuilder() {
    RequestOfflineFriendShip._initializeBuilder(this);
  }

  RequestOfflineFriendShipBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _phoneNumber = $v.phoneNumber;
      _description = $v.description;
      _firstName = $v.firstName;
      _lastName = $v.lastName;
      _avatar = $v.avatar;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RequestOfflineFriendShip other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RequestOfflineFriendShip;
  }

  @override
  void update(void Function(RequestOfflineFriendShipBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RequestOfflineFriendShip build() {
    final _$result = _$v ??
        new _$RequestOfflineFriendShip._(
            phoneNumber: phoneNumber,
            description: description,
            firstName: firstName,
            lastName: lastName,
            avatar: avatar);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
