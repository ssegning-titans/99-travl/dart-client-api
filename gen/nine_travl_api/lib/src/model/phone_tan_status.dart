//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'phone_tan_status.g.dart';

abstract class PhoneTanStatus
    implements Built<PhoneTanStatus, PhoneTanStatusBuilder> {
  @BuiltValueField(wireName: r'status')
  bool? get status;

  PhoneTanStatus._();

  static void _initializeBuilder(PhoneTanStatusBuilder b) => b;

  factory PhoneTanStatus([void updates(PhoneTanStatusBuilder b)]) =
      _$PhoneTanStatus;

  @BuiltValueSerializer(custom: true)
  static Serializer<PhoneTanStatus> get serializer =>
      _$PhoneTanStatusSerializer();
}

class _$PhoneTanStatusSerializer
    implements StructuredSerializer<PhoneTanStatus> {
  @override
  final Iterable<Type> types = const [PhoneTanStatus, _$PhoneTanStatus];

  @override
  final String wireName = r'PhoneTanStatus';

  @override
  Iterable<Object?> serialize(Serializers serializers, PhoneTanStatus object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.status != null) {
      result
        ..add(r'status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(bool)));
    }
    return result;
  }

  @override
  PhoneTanStatus deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = PhoneTanStatusBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
      }
    }
    return result.build();
  }
}
