// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rating_travel.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$RatingTravel extends RatingTravel {
  @override
  final String? description;
  @override
  final String authorId;
  @override
  final RatingValue value;
  @override
  final int? createdAt;
  @override
  final RatingType type;
  @override
  final String travelId;

  factory _$RatingTravel([void Function(RatingTravelBuilder)? updates]) =>
      (new RatingTravelBuilder()..update(updates)).build();

  _$RatingTravel._(
      {this.description,
      required this.authorId,
      required this.value,
      this.createdAt,
      required this.type,
      required this.travelId})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(authorId, 'RatingTravel', 'authorId');
    BuiltValueNullFieldError.checkNotNull(value, 'RatingTravel', 'value');
    BuiltValueNullFieldError.checkNotNull(type, 'RatingTravel', 'type');
    BuiltValueNullFieldError.checkNotNull(travelId, 'RatingTravel', 'travelId');
  }

  @override
  RatingTravel rebuild(void Function(RatingTravelBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  RatingTravelBuilder toBuilder() => new RatingTravelBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is RatingTravel &&
        description == other.description &&
        authorId == other.authorId &&
        value == other.value &&
        createdAt == other.createdAt &&
        type == other.type &&
        travelId == other.travelId;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, description.hashCode), authorId.hashCode),
                    value.hashCode),
                createdAt.hashCode),
            type.hashCode),
        travelId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('RatingTravel')
          ..add('description', description)
          ..add('authorId', authorId)
          ..add('value', value)
          ..add('createdAt', createdAt)
          ..add('type', type)
          ..add('travelId', travelId))
        .toString();
  }
}

class RatingTravelBuilder
    implements Builder<RatingTravel, RatingTravelBuilder> {
  _$RatingTravel? _$v;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  RatingValue? _value;
  RatingValue? get value => _$this._value;
  set value(RatingValue? value) => _$this._value = value;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  RatingType? _type;
  RatingType? get type => _$this._type;
  set type(RatingType? type) => _$this._type = type;

  String? _travelId;
  String? get travelId => _$this._travelId;
  set travelId(String? travelId) => _$this._travelId = travelId;

  RatingTravelBuilder() {
    RatingTravel._initializeBuilder(this);
  }

  RatingTravelBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _description = $v.description;
      _authorId = $v.authorId;
      _value = $v.value;
      _createdAt = $v.createdAt;
      _type = $v.type;
      _travelId = $v.travelId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(RatingTravel other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$RatingTravel;
  }

  @override
  void update(void Function(RatingTravelBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$RatingTravel build() {
    final _$result = _$v ??
        new _$RatingTravel._(
            description: description,
            authorId: BuiltValueNullFieldError.checkNotNull(
                authorId, 'RatingTravel', 'authorId'),
            value: BuiltValueNullFieldError.checkNotNull(
                value, 'RatingTravel', 'value'),
            createdAt: createdAt,
            type: BuiltValueNullFieldError.checkNotNull(
                type, 'RatingTravel', 'type'),
            travelId: BuiltValueNullFieldError.checkNotNull(
                travelId, 'RatingTravel', 'travelId'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
