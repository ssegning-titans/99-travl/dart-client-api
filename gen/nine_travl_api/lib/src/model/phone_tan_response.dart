//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'phone_tan_response.g.dart';

abstract class PhoneTanResponse
    implements Built<PhoneTanResponse, PhoneTanResponseBuilder> {
  @BuiltValueField(wireName: r'secret')
  String? get secret;

  PhoneTanResponse._();

  static void _initializeBuilder(PhoneTanResponseBuilder b) => b;

  factory PhoneTanResponse([void updates(PhoneTanResponseBuilder b)]) =
      _$PhoneTanResponse;

  @BuiltValueSerializer(custom: true)
  static Serializer<PhoneTanResponse> get serializer =>
      _$PhoneTanResponseSerializer();
}

class _$PhoneTanResponseSerializer
    implements StructuredSerializer<PhoneTanResponse> {
  @override
  final Iterable<Type> types = const [PhoneTanResponse, _$PhoneTanResponse];

  @override
  final String wireName = r'PhoneTanResponse';

  @override
  Iterable<Object?> serialize(Serializers serializers, PhoneTanResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.secret != null) {
      result
        ..add(r'secret')
        ..add(serializers.serialize(object.secret,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  PhoneTanResponse deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = PhoneTanResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'secret':
          result.secret = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
