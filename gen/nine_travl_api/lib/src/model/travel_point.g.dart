// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'travel_point.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$TravelPoint extends TravelPoint {
  @override
  final String? id;
  @override
  final int? priority;
  @override
  final int? creationDate;
  @override
  final int? passageTime;
  @override
  final String? travelId;
  @override
  final String? region;
  @override
  final String? formattedName;
  @override
  final String? street;
  @override
  final String? houseNumber;
  @override
  final String? city;
  @override
  final String? zip;
  @override
  final String country;
  @override
  final LatLng location;

  factory _$TravelPoint([void Function(TravelPointBuilder)? updates]) =>
      (new TravelPointBuilder()..update(updates)).build();

  _$TravelPoint._(
      {this.id,
      this.priority,
      this.creationDate,
      this.passageTime,
      this.travelId,
      this.region,
      this.formattedName,
      this.street,
      this.houseNumber,
      this.city,
      this.zip,
      required this.country,
      required this.location})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(country, 'TravelPoint', 'country');
    BuiltValueNullFieldError.checkNotNull(location, 'TravelPoint', 'location');
  }

  @override
  TravelPoint rebuild(void Function(TravelPointBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TravelPointBuilder toBuilder() => new TravelPointBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TravelPoint &&
        id == other.id &&
        priority == other.priority &&
        creationDate == other.creationDate &&
        passageTime == other.passageTime &&
        travelId == other.travelId &&
        region == other.region &&
        formattedName == other.formattedName &&
        street == other.street &&
        houseNumber == other.houseNumber &&
        city == other.city &&
        zip == other.zip &&
        country == other.country &&
        location == other.location;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc(
                                    $jc(
                                        $jc(
                                            $jc(
                                                $jc($jc(0, id.hashCode),
                                                    priority.hashCode),
                                                creationDate.hashCode),
                                            passageTime.hashCode),
                                        travelId.hashCode),
                                    region.hashCode),
                                formattedName.hashCode),
                            street.hashCode),
                        houseNumber.hashCode),
                    city.hashCode),
                zip.hashCode),
            country.hashCode),
        location.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TravelPoint')
          ..add('id', id)
          ..add('priority', priority)
          ..add('creationDate', creationDate)
          ..add('passageTime', passageTime)
          ..add('travelId', travelId)
          ..add('region', region)
          ..add('formattedName', formattedName)
          ..add('street', street)
          ..add('houseNumber', houseNumber)
          ..add('city', city)
          ..add('zip', zip)
          ..add('country', country)
          ..add('location', location))
        .toString();
  }
}

class TravelPointBuilder implements Builder<TravelPoint, TravelPointBuilder> {
  _$TravelPoint? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _priority;
  int? get priority => _$this._priority;
  set priority(int? priority) => _$this._priority = priority;

  int? _creationDate;
  int? get creationDate => _$this._creationDate;
  set creationDate(int? creationDate) => _$this._creationDate = creationDate;

  int? _passageTime;
  int? get passageTime => _$this._passageTime;
  set passageTime(int? passageTime) => _$this._passageTime = passageTime;

  String? _travelId;
  String? get travelId => _$this._travelId;
  set travelId(String? travelId) => _$this._travelId = travelId;

  String? _region;
  String? get region => _$this._region;
  set region(String? region) => _$this._region = region;

  String? _formattedName;
  String? get formattedName => _$this._formattedName;
  set formattedName(String? formattedName) =>
      _$this._formattedName = formattedName;

  String? _street;
  String? get street => _$this._street;
  set street(String? street) => _$this._street = street;

  String? _houseNumber;
  String? get houseNumber => _$this._houseNumber;
  set houseNumber(String? houseNumber) => _$this._houseNumber = houseNumber;

  String? _city;
  String? get city => _$this._city;
  set city(String? city) => _$this._city = city;

  String? _zip;
  String? get zip => _$this._zip;
  set zip(String? zip) => _$this._zip = zip;

  String? _country;
  String? get country => _$this._country;
  set country(String? country) => _$this._country = country;

  LatLngBuilder? _location;
  LatLngBuilder get location => _$this._location ??= new LatLngBuilder();
  set location(LatLngBuilder? location) => _$this._location = location;

  TravelPointBuilder() {
    TravelPoint._initializeBuilder(this);
  }

  TravelPointBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _priority = $v.priority;
      _creationDate = $v.creationDate;
      _passageTime = $v.passageTime;
      _travelId = $v.travelId;
      _region = $v.region;
      _formattedName = $v.formattedName;
      _street = $v.street;
      _houseNumber = $v.houseNumber;
      _city = $v.city;
      _zip = $v.zip;
      _country = $v.country;
      _location = $v.location.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TravelPoint other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TravelPoint;
  }

  @override
  void update(void Function(TravelPointBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TravelPoint build() {
    _$TravelPoint _$result;
    try {
      _$result = _$v ??
          new _$TravelPoint._(
              id: id,
              priority: priority,
              creationDate: creationDate,
              passageTime: passageTime,
              travelId: travelId,
              region: region,
              formattedName: formattedName,
              street: street,
              houseNumber: houseNumber,
              city: city,
              zip: zip,
              country: BuiltValueNullFieldError.checkNotNull(
                  country, 'TravelPoint', 'country'),
              location: location.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'location';
        location.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'TravelPoint', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
