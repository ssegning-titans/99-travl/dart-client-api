// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'text_chat_message.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$TextChatMessage extends TextChatMessage {
  @override
  final String? id;
  @override
  final int? createdAt;
  @override
  final String authorId;
  @override
  final String chatId;
  @override
  final ChatMessageType messageType;
  @override
  final bool? seen;
  @override
  final String message;

  factory _$TextChatMessage([void Function(TextChatMessageBuilder)? updates]) =>
      (new TextChatMessageBuilder()..update(updates)).build();

  _$TextChatMessage._(
      {this.id,
      this.createdAt,
      required this.authorId,
      required this.chatId,
      required this.messageType,
      this.seen,
      required this.message})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        authorId, 'TextChatMessage', 'authorId');
    BuiltValueNullFieldError.checkNotNull(chatId, 'TextChatMessage', 'chatId');
    BuiltValueNullFieldError.checkNotNull(
        messageType, 'TextChatMessage', 'messageType');
    BuiltValueNullFieldError.checkNotNull(
        message, 'TextChatMessage', 'message');
  }

  @override
  TextChatMessage rebuild(void Function(TextChatMessageBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TextChatMessageBuilder toBuilder() =>
      new TextChatMessageBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TextChatMessage &&
        id == other.id &&
        createdAt == other.createdAt &&
        authorId == other.authorId &&
        chatId == other.chatId &&
        messageType == other.messageType &&
        seen == other.seen &&
        message == other.message;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc($jc($jc(0, id.hashCode), createdAt.hashCode),
                        authorId.hashCode),
                    chatId.hashCode),
                messageType.hashCode),
            seen.hashCode),
        message.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TextChatMessage')
          ..add('id', id)
          ..add('createdAt', createdAt)
          ..add('authorId', authorId)
          ..add('chatId', chatId)
          ..add('messageType', messageType)
          ..add('seen', seen)
          ..add('message', message))
        .toString();
  }
}

class TextChatMessageBuilder
    implements Builder<TextChatMessage, TextChatMessageBuilder> {
  _$TextChatMessage? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _createdAt;
  int? get createdAt => _$this._createdAt;
  set createdAt(int? createdAt) => _$this._createdAt = createdAt;

  String? _authorId;
  String? get authorId => _$this._authorId;
  set authorId(String? authorId) => _$this._authorId = authorId;

  String? _chatId;
  String? get chatId => _$this._chatId;
  set chatId(String? chatId) => _$this._chatId = chatId;

  ChatMessageType? _messageType;
  ChatMessageType? get messageType => _$this._messageType;
  set messageType(ChatMessageType? messageType) =>
      _$this._messageType = messageType;

  bool? _seen;
  bool? get seen => _$this._seen;
  set seen(bool? seen) => _$this._seen = seen;

  String? _message;
  String? get message => _$this._message;
  set message(String? message) => _$this._message = message;

  TextChatMessageBuilder() {
    TextChatMessage._initializeBuilder(this);
  }

  TextChatMessageBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _createdAt = $v.createdAt;
      _authorId = $v.authorId;
      _chatId = $v.chatId;
      _messageType = $v.messageType;
      _seen = $v.seen;
      _message = $v.message;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TextChatMessage other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TextChatMessage;
  }

  @override
  void update(void Function(TextChatMessageBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TextChatMessage build() {
    final _$result = _$v ??
        new _$TextChatMessage._(
            id: id,
            createdAt: createdAt,
            authorId: BuiltValueNullFieldError.checkNotNull(
                authorId, 'TextChatMessage', 'authorId'),
            chatId: BuiltValueNullFieldError.checkNotNull(
                chatId, 'TextChatMessage', 'chatId'),
            messageType: BuiltValueNullFieldError.checkNotNull(
                messageType, 'TextChatMessage', 'messageType'),
            seen: seen,
            message: BuiltValueNullFieldError.checkNotNull(
                message, 'TextChatMessage', 'message'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
