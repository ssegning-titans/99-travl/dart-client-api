//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'validate_friend_ship_code.g.dart';

abstract class ValidateFriendShipCode
    implements Built<ValidateFriendShipCode, ValidateFriendShipCodeBuilder> {
  @BuiltValueField(wireName: r'code')
  String? get code;

  ValidateFriendShipCode._();

  static void _initializeBuilder(ValidateFriendShipCodeBuilder b) => b;

  factory ValidateFriendShipCode(
          [void updates(ValidateFriendShipCodeBuilder b)]) =
      _$ValidateFriendShipCode;

  @BuiltValueSerializer(custom: true)
  static Serializer<ValidateFriendShipCode> get serializer =>
      _$ValidateFriendShipCodeSerializer();
}

class _$ValidateFriendShipCodeSerializer
    implements StructuredSerializer<ValidateFriendShipCode> {
  @override
  final Iterable<Type> types = const [
    ValidateFriendShipCode,
    _$ValidateFriendShipCode
  ];

  @override
  final String wireName = r'ValidateFriendShipCode';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, ValidateFriendShipCode object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.code != null) {
      result
        ..add(r'code')
        ..add(serializers.serialize(object.code,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  ValidateFriendShipCode deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = ValidateFriendShipCodeBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'code':
          result.code = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
