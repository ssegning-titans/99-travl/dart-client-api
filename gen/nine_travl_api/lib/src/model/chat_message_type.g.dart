// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_message_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ChatMessageType _$MEDIA = const ChatMessageType._('MEDIA');
const ChatMessageType _$TEXT = const ChatMessageType._('TEXT');
const ChatMessageType _$PROPOSITION = const ChatMessageType._('PROPOSITION');
const ChatMessageType _$CONTACT = const ChatMessageType._('CONTACT');

ChatMessageType _$valueOf(String name) {
  switch (name) {
    case 'MEDIA':
      return _$MEDIA;
    case 'TEXT':
      return _$TEXT;
    case 'PROPOSITION':
      return _$PROPOSITION;
    case 'CONTACT':
      return _$CONTACT;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ChatMessageType> _$values =
    new BuiltSet<ChatMessageType>(const <ChatMessageType>[
  _$MEDIA,
  _$TEXT,
  _$PROPOSITION,
  _$CONTACT,
]);

class _$ChatMessageTypeMeta {
  const _$ChatMessageTypeMeta();
  ChatMessageType get MEDIA => _$MEDIA;
  ChatMessageType get TEXT => _$TEXT;
  ChatMessageType get PROPOSITION => _$PROPOSITION;
  ChatMessageType get CONTACT => _$CONTACT;
  ChatMessageType valueOf(String name) => _$valueOf(name);
  BuiltSet<ChatMessageType> get values => _$values;
}

abstract class _$ChatMessageTypeMixin {
  // ignore: non_constant_identifier_names
  _$ChatMessageTypeMeta get ChatMessageType => const _$ChatMessageTypeMeta();
}

Serializer<ChatMessageType> _$chatMessageTypeSerializer =
    new _$ChatMessageTypeSerializer();

class _$ChatMessageTypeSerializer
    implements PrimitiveSerializer<ChatMessageType> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'MEDIA': 'MEDIA',
    'TEXT': 'TEXT',
    'PROPOSITION': 'PROPOSITION',
    'CONTACT': 'CONTACT',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'MEDIA': 'MEDIA',
    'TEXT': 'TEXT',
    'PROPOSITION': 'PROPOSITION',
    'CONTACT': 'CONTACT',
  };

  @override
  final Iterable<Type> types = const <Type>[ChatMessageType];
  @override
  final String wireName = 'ChatMessageType';

  @override
  Object serialize(Serializers serializers, ChatMessageType object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ChatMessageType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ChatMessageType.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
