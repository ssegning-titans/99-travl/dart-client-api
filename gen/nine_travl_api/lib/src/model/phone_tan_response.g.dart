// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'phone_tan_response.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$PhoneTanResponse extends PhoneTanResponse {
  @override
  final String? secret;

  factory _$PhoneTanResponse(
          [void Function(PhoneTanResponseBuilder)? updates]) =>
      (new PhoneTanResponseBuilder()..update(updates)).build();

  _$PhoneTanResponse._({this.secret}) : super._();

  @override
  PhoneTanResponse rebuild(void Function(PhoneTanResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  PhoneTanResponseBuilder toBuilder() =>
      new PhoneTanResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is PhoneTanResponse && secret == other.secret;
  }

  @override
  int get hashCode {
    return $jf($jc(0, secret.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('PhoneTanResponse')
          ..add('secret', secret))
        .toString();
  }
}

class PhoneTanResponseBuilder
    implements Builder<PhoneTanResponse, PhoneTanResponseBuilder> {
  _$PhoneTanResponse? _$v;

  String? _secret;
  String? get secret => _$this._secret;
  set secret(String? secret) => _$this._secret = secret;

  PhoneTanResponseBuilder() {
    PhoneTanResponse._initializeBuilder(this);
  }

  PhoneTanResponseBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _secret = $v.secret;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(PhoneTanResponse other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$PhoneTanResponse;
  }

  @override
  void update(void Function(PhoneTanResponseBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$PhoneTanResponse build() {
    final _$result = _$v ?? new _$PhoneTanResponse._(secret: secret);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
