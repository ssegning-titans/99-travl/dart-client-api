//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'rating_account_all_of.g.dart';

abstract class RatingAccountAllOf
    implements Built<RatingAccountAllOf, RatingAccountAllOfBuilder> {
  @BuiltValueField(wireName: r'personId')
  String get personId;

  RatingAccountAllOf._();

  static void _initializeBuilder(RatingAccountAllOfBuilder b) => b;

  factory RatingAccountAllOf([void updates(RatingAccountAllOfBuilder b)]) =
      _$RatingAccountAllOf;

  @BuiltValueSerializer(custom: true)
  static Serializer<RatingAccountAllOf> get serializer =>
      _$RatingAccountAllOfSerializer();
}

class _$RatingAccountAllOfSerializer
    implements StructuredSerializer<RatingAccountAllOf> {
  @override
  final Iterable<Type> types = const [RatingAccountAllOf, _$RatingAccountAllOf];

  @override
  final String wireName = r'RatingAccountAllOf';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, RatingAccountAllOf object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'personId')
      ..add(serializers.serialize(object.personId,
          specifiedType: const FullType(String)));
    return result;
  }

  @override
  RatingAccountAllOf deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = RatingAccountAllOfBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'personId':
          result.personId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
