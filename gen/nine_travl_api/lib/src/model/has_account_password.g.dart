// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'has_account_password.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$HasAccountPassword extends HasAccountPassword {
  @override
  final bool state;

  factory _$HasAccountPassword(
          [void Function(HasAccountPasswordBuilder)? updates]) =>
      (new HasAccountPasswordBuilder()..update(updates)).build();

  _$HasAccountPassword._({required this.state}) : super._() {
    BuiltValueNullFieldError.checkNotNull(state, 'HasAccountPassword', 'state');
  }

  @override
  HasAccountPassword rebuild(
          void Function(HasAccountPasswordBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  HasAccountPasswordBuilder toBuilder() =>
      new HasAccountPasswordBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is HasAccountPassword && state == other.state;
  }

  @override
  int get hashCode {
    return $jf($jc(0, state.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('HasAccountPassword')
          ..add('state', state))
        .toString();
  }
}

class HasAccountPasswordBuilder
    implements Builder<HasAccountPassword, HasAccountPasswordBuilder> {
  _$HasAccountPassword? _$v;

  bool? _state;
  bool? get state => _$this._state;
  set state(bool? state) => _$this._state = state;

  HasAccountPasswordBuilder() {
    HasAccountPassword._initializeBuilder(this);
  }

  HasAccountPasswordBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _state = $v.state;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(HasAccountPassword other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$HasAccountPassword;
  }

  @override
  void update(void Function(HasAccountPasswordBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$HasAccountPassword build() {
    final _$result = _$v ??
        new _$HasAccountPassword._(
            state: BuiltValueNullFieldError.checkNotNull(
                state, 'HasAccountPassword', 'state'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
