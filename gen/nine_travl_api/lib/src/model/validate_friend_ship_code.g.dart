// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'validate_friend_ship_code.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ValidateFriendShipCode extends ValidateFriendShipCode {
  @override
  final String? code;

  factory _$ValidateFriendShipCode(
          [void Function(ValidateFriendShipCodeBuilder)? updates]) =>
      (new ValidateFriendShipCodeBuilder()..update(updates)).build();

  _$ValidateFriendShipCode._({this.code}) : super._();

  @override
  ValidateFriendShipCode rebuild(
          void Function(ValidateFriendShipCodeBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ValidateFriendShipCodeBuilder toBuilder() =>
      new ValidateFriendShipCodeBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ValidateFriendShipCode && code == other.code;
  }

  @override
  int get hashCode {
    return $jf($jc(0, code.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ValidateFriendShipCode')
          ..add('code', code))
        .toString();
  }
}

class ValidateFriendShipCodeBuilder
    implements Builder<ValidateFriendShipCode, ValidateFriendShipCodeBuilder> {
  _$ValidateFriendShipCode? _$v;

  String? _code;
  String? get code => _$this._code;
  set code(String? code) => _$this._code = code;

  ValidateFriendShipCodeBuilder() {
    ValidateFriendShipCode._initializeBuilder(this);
  }

  ValidateFriendShipCodeBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _code = $v.code;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ValidateFriendShipCode other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ValidateFriendShipCode;
  }

  @override
  void update(void Function(ValidateFriendShipCodeBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ValidateFriendShipCode build() {
    final _$result = _$v ?? new _$ValidateFriendShipCode._(code: code);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
