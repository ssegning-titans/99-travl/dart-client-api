//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'stripe_payment_intent_response.g.dart';

abstract class StripePaymentIntentResponse
    implements
        Built<StripePaymentIntentResponse, StripePaymentIntentResponseBuilder> {
  @BuiltValueField(wireName: r'secret')
  String? get secret;

  StripePaymentIntentResponse._();

  static void _initializeBuilder(StripePaymentIntentResponseBuilder b) => b;

  factory StripePaymentIntentResponse(
          [void updates(StripePaymentIntentResponseBuilder b)]) =
      _$StripePaymentIntentResponse;

  @BuiltValueSerializer(custom: true)
  static Serializer<StripePaymentIntentResponse> get serializer =>
      _$StripePaymentIntentResponseSerializer();
}

class _$StripePaymentIntentResponseSerializer
    implements StructuredSerializer<StripePaymentIntentResponse> {
  @override
  final Iterable<Type> types = const [
    StripePaymentIntentResponse,
    _$StripePaymentIntentResponse
  ];

  @override
  final String wireName = r'StripePaymentIntentResponse';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, StripePaymentIntentResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.secret != null) {
      result
        ..add(r'secret')
        ..add(serializers.serialize(object.secret,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  StripePaymentIntentResponse deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = StripePaymentIntentResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'secret':
          result.secret = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
