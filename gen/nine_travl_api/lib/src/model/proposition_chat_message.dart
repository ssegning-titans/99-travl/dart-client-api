//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/proposition_chat_message_all_of.dart';
import 'package:nine_travl_api/src/model/chat_message_type.dart';
import 'package:nine_travl_api/src/model/base_chat_message.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'proposition_chat_message.g.dart';

// ignore_for_file: unused_import

abstract class PropositionChatMessage
    implements Built<PropositionChatMessage, PropositionChatMessageBuilder> {
  @BuiltValueField(wireName: r'id')
  String? get id;

  @BuiltValueField(wireName: r'createdAt')
  int? get createdAt;

  @BuiltValueField(wireName: r'authorId')
  String get authorId;

  @BuiltValueField(wireName: r'chatId')
  String get chatId;

  @BuiltValueField(wireName: r'messageType')
  ChatMessageType get messageType;
  // enum messageTypeEnum {  MEDIA,  TEXT,  PROPOSITION,  CONTACT,  };

  @BuiltValueField(wireName: r'seen')
  bool? get seen;

  @BuiltValueField(wireName: r'propositionId')
  String get propositionId;

  @BuiltValueField(wireName: r'proposerAccountId')
  String get proposerAccountId;

  PropositionChatMessage._();

  static void _initializeBuilder(PropositionChatMessageBuilder b) =>
      b..seen = false;

  factory PropositionChatMessage(
          [void updates(PropositionChatMessageBuilder b)]) =
      _$PropositionChatMessage;

  @BuiltValueSerializer(custom: true)
  static Serializer<PropositionChatMessage> get serializer =>
      _$PropositionChatMessageSerializer();
}

class _$PropositionChatMessageSerializer
    implements StructuredSerializer<PropositionChatMessage> {
  @override
  final Iterable<Type> types = const [
    PropositionChatMessage,
    _$PropositionChatMessage
  ];

  @override
  final String wireName = r'PropositionChatMessage';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, PropositionChatMessage object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.id != null) {
      result
        ..add(r'id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.createdAt != null) {
      result
        ..add(r'createdAt')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(int)));
    }
    result
      ..add(r'authorId')
      ..add(serializers.serialize(object.authorId,
          specifiedType: const FullType(String)));
    result
      ..add(r'chatId')
      ..add(serializers.serialize(object.chatId,
          specifiedType: const FullType(String)));
    result
      ..add(r'messageType')
      ..add(serializers.serialize(object.messageType,
          specifiedType: const FullType(ChatMessageType)));
    if (object.seen != null) {
      result
        ..add(r'seen')
        ..add(serializers.serialize(object.seen,
            specifiedType: const FullType(bool)));
    }
    result
      ..add(r'propositionId')
      ..add(serializers.serialize(object.propositionId,
          specifiedType: const FullType(String)));
    result
      ..add(r'proposerAccountId')
      ..add(serializers.serialize(object.proposerAccountId,
          specifiedType: const FullType(String)));
    return result;
  }

  @override
  PropositionChatMessage deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = PropositionChatMessageBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'authorId':
          result.authorId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'chatId':
          result.chatId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'messageType':
          result.messageType = serializers.deserialize(value,
                  specifiedType: const FullType(ChatMessageType))
              as ChatMessageType;
          break;
        case r'seen':
          result.seen = serializers.deserialize(value,
              specifiedType: const FullType(bool)) as bool;
          break;
        case r'propositionId':
          result.propositionId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'proposerAccountId':
          result.proposerAccountId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
