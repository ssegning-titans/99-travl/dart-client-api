//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_report_account.g.dart';

abstract class CreateReportAccount
    implements Built<CreateReportAccount, CreateReportAccountBuilder> {
  @BuiltValueField(wireName: r'authorId')
  String get authorId;

  @BuiltValueField(wireName: r'accountId')
  String? get accountId;

  @BuiltValueField(wireName: r'description')
  String? get description;

  CreateReportAccount._();

  static void _initializeBuilder(CreateReportAccountBuilder b) => b;

  factory CreateReportAccount([void updates(CreateReportAccountBuilder b)]) =
      _$CreateReportAccount;

  @BuiltValueSerializer(custom: true)
  static Serializer<CreateReportAccount> get serializer =>
      _$CreateReportAccountSerializer();
}

class _$CreateReportAccountSerializer
    implements StructuredSerializer<CreateReportAccount> {
  @override
  final Iterable<Type> types = const [
    CreateReportAccount,
    _$CreateReportAccount
  ];

  @override
  final String wireName = r'CreateReportAccount';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, CreateReportAccount object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'authorId')
      ..add(serializers.serialize(object.authorId,
          specifiedType: const FullType(String)));
    if (object.accountId != null) {
      result
        ..add(r'accountId')
        ..add(serializers.serialize(object.accountId,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add(r'description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CreateReportAccount deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = CreateReportAccountBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'authorId':
          result.authorId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'accountId':
          result.accountId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
