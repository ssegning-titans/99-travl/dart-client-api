//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/base_favorite.dart';
import 'package:nine_travl_api/src/model/favorite_type.dart';
import 'package:nine_travl_api/src/model/report_account_all_of.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'favorite_account.g.dart';

// ignore_for_file: unused_import

abstract class FavoriteAccount
    implements Built<FavoriteAccount, FavoriteAccountBuilder> {
  @BuiltValueField(wireName: r'ownerId')
  String get ownerId;

  @BuiltValueField(wireName: r'createdAt')
  int? get createdAt;

  @BuiltValueField(wireName: r'favoriteType')
  FavoriteType get favoriteType;
  // enum favoriteTypeEnum {  TRAVEL,  ACCOUNT,  };

  @BuiltValueField(wireName: r'accountId')
  String get accountId;

  FavoriteAccount._();

  static void _initializeBuilder(FavoriteAccountBuilder b) => b;

  factory FavoriteAccount([void updates(FavoriteAccountBuilder b)]) =
      _$FavoriteAccount;

  @BuiltValueSerializer(custom: true)
  static Serializer<FavoriteAccount> get serializer =>
      _$FavoriteAccountSerializer();
}

class _$FavoriteAccountSerializer
    implements StructuredSerializer<FavoriteAccount> {
  @override
  final Iterable<Type> types = const [FavoriteAccount, _$FavoriteAccount];

  @override
  final String wireName = r'FavoriteAccount';

  @override
  Iterable<Object?> serialize(Serializers serializers, FavoriteAccount object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'ownerId')
      ..add(serializers.serialize(object.ownerId,
          specifiedType: const FullType(String)));
    if (object.createdAt != null) {
      result
        ..add(r'createdAt')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(int)));
    }
    result
      ..add(r'favoriteType')
      ..add(serializers.serialize(object.favoriteType,
          specifiedType: const FullType(FavoriteType)));
    result
      ..add(r'accountId')
      ..add(serializers.serialize(object.accountId,
          specifiedType: const FullType(String)));
    return result;
  }

  @override
  FavoriteAccount deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = FavoriteAccountBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'ownerId':
          result.ownerId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'favoriteType':
          result.favoriteType = serializers.deserialize(value,
              specifiedType: const FullType(FavoriteType)) as FavoriteType;
          break;
        case r'accountId':
          result.accountId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
