// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'friend_process_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const FriendProcessType _$OFFLINE = const FriendProcessType._('OFFLINE');
const FriendProcessType _$ONLINE = const FriendProcessType._('ONLINE');

FriendProcessType _$valueOf(String name) {
  switch (name) {
    case 'OFFLINE':
      return _$OFFLINE;
    case 'ONLINE':
      return _$ONLINE;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<FriendProcessType> _$values =
    new BuiltSet<FriendProcessType>(const <FriendProcessType>[
  _$OFFLINE,
  _$ONLINE,
]);

class _$FriendProcessTypeMeta {
  const _$FriendProcessTypeMeta();
  FriendProcessType get OFFLINE => _$OFFLINE;
  FriendProcessType get ONLINE => _$ONLINE;
  FriendProcessType valueOf(String name) => _$valueOf(name);
  BuiltSet<FriendProcessType> get values => _$values;
}

abstract class _$FriendProcessTypeMixin {
  // ignore: non_constant_identifier_names
  _$FriendProcessTypeMeta get FriendProcessType =>
      const _$FriendProcessTypeMeta();
}

Serializer<FriendProcessType> _$friendProcessTypeSerializer =
    new _$FriendProcessTypeSerializer();

class _$FriendProcessTypeSerializer
    implements PrimitiveSerializer<FriendProcessType> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'OFFLINE': 'OFFLINE',
    'ONLINE': 'ONLINE',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'OFFLINE': 'OFFLINE',
    'ONLINE': 'ONLINE',
  };

  @override
  final Iterable<Type> types = const <Type>[FriendProcessType];
  @override
  final String wireName = 'FriendProcessType';

  @override
  Object serialize(Serializers serializers, FriendProcessType object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  FriendProcessType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      FriendProcessType.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
