// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_credential_input.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CreateCredentialInput extends CreateCredentialInput {
  @override
  final String challenge;
  @override
  final BuiltMap<String, String>? data;

  factory _$CreateCredentialInput(
          [void Function(CreateCredentialInputBuilder)? updates]) =>
      (new CreateCredentialInputBuilder()..update(updates)).build();

  _$CreateCredentialInput._({required this.challenge, this.data}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        challenge, 'CreateCredentialInput', 'challenge');
  }

  @override
  CreateCredentialInput rebuild(
          void Function(CreateCredentialInputBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreateCredentialInputBuilder toBuilder() =>
      new CreateCredentialInputBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreateCredentialInput &&
        challenge == other.challenge &&
        data == other.data;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, challenge.hashCode), data.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CreateCredentialInput')
          ..add('challenge', challenge)
          ..add('data', data))
        .toString();
  }
}

class CreateCredentialInputBuilder
    implements Builder<CreateCredentialInput, CreateCredentialInputBuilder> {
  _$CreateCredentialInput? _$v;

  String? _challenge;
  String? get challenge => _$this._challenge;
  set challenge(String? challenge) => _$this._challenge = challenge;

  MapBuilder<String, String>? _data;
  MapBuilder<String, String> get data =>
      _$this._data ??= new MapBuilder<String, String>();
  set data(MapBuilder<String, String>? data) => _$this._data = data;

  CreateCredentialInputBuilder() {
    CreateCredentialInput._initializeBuilder(this);
  }

  CreateCredentialInputBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _challenge = $v.challenge;
      _data = $v.data?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreateCredentialInput other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CreateCredentialInput;
  }

  @override
  void update(void Function(CreateCredentialInputBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CreateCredentialInput build() {
    _$CreateCredentialInput _$result;
    try {
      _$result = _$v ??
          new _$CreateCredentialInput._(
              challenge: BuiltValueNullFieldError.checkNotNull(
                  challenge, 'CreateCredentialInput', 'challenge'),
              data: _data?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'data';
        _data?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'CreateCredentialInput', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
