// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stripe_account.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$StripeAccount extends StripeAccount {
  @override
  final String? stripeAccountId;

  factory _$StripeAccount([void Function(StripeAccountBuilder)? updates]) =>
      (new StripeAccountBuilder()..update(updates)).build();

  _$StripeAccount._({this.stripeAccountId}) : super._();

  @override
  StripeAccount rebuild(void Function(StripeAccountBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  StripeAccountBuilder toBuilder() => new StripeAccountBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is StripeAccount && stripeAccountId == other.stripeAccountId;
  }

  @override
  int get hashCode {
    return $jf($jc(0, stripeAccountId.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('StripeAccount')
          ..add('stripeAccountId', stripeAccountId))
        .toString();
  }
}

class StripeAccountBuilder
    implements Builder<StripeAccount, StripeAccountBuilder> {
  _$StripeAccount? _$v;

  String? _stripeAccountId;
  String? get stripeAccountId => _$this._stripeAccountId;
  set stripeAccountId(String? stripeAccountId) =>
      _$this._stripeAccountId = stripeAccountId;

  StripeAccountBuilder() {
    StripeAccount._initializeBuilder(this);
  }

  StripeAccountBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _stripeAccountId = $v.stripeAccountId;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(StripeAccount other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$StripeAccount;
  }

  @override
  void update(void Function(StripeAccountBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$StripeAccount build() {
    final _$result =
        _$v ?? new _$StripeAccount._(stripeAccountId: stripeAccountId);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
