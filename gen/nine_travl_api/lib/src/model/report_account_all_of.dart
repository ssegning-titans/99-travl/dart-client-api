//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'report_account_all_of.g.dart';

abstract class ReportAccountAllOf
    implements Built<ReportAccountAllOf, ReportAccountAllOfBuilder> {
  @BuiltValueField(wireName: r'accountId')
  String get accountId;

  ReportAccountAllOf._();

  static void _initializeBuilder(ReportAccountAllOfBuilder b) => b;

  factory ReportAccountAllOf([void updates(ReportAccountAllOfBuilder b)]) =
      _$ReportAccountAllOf;

  @BuiltValueSerializer(custom: true)
  static Serializer<ReportAccountAllOf> get serializer =>
      _$ReportAccountAllOfSerializer();
}

class _$ReportAccountAllOfSerializer
    implements StructuredSerializer<ReportAccountAllOf> {
  @override
  final Iterable<Type> types = const [ReportAccountAllOf, _$ReportAccountAllOf];

  @override
  final String wireName = r'ReportAccountAllOf';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, ReportAccountAllOf object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'accountId')
      ..add(serializers.serialize(object.accountId,
          specifiedType: const FullType(String)));
    return result;
  }

  @override
  ReportAccountAllOf deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = ReportAccountAllOfBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'accountId':
          result.accountId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
