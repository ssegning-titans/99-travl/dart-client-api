//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'request_online_friend_ship.g.dart';

abstract class RequestOnlineFriendShip
    implements Built<RequestOnlineFriendShip, RequestOnlineFriendShipBuilder> {
  @BuiltValueField(wireName: r'accountId')
  String? get accountId;

  RequestOnlineFriendShip._();

  static void _initializeBuilder(RequestOnlineFriendShipBuilder b) => b;

  factory RequestOnlineFriendShip(
          [void updates(RequestOnlineFriendShipBuilder b)]) =
      _$RequestOnlineFriendShip;

  @BuiltValueSerializer(custom: true)
  static Serializer<RequestOnlineFriendShip> get serializer =>
      _$RequestOnlineFriendShipSerializer();
}

class _$RequestOnlineFriendShipSerializer
    implements StructuredSerializer<RequestOnlineFriendShip> {
  @override
  final Iterable<Type> types = const [
    RequestOnlineFriendShip,
    _$RequestOnlineFriendShip
  ];

  @override
  final String wireName = r'RequestOnlineFriendShip';

  @override
  Iterable<Object?> serialize(
      Serializers serializers, RequestOnlineFriendShip object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.accountId != null) {
      result
        ..add(r'accountId')
        ..add(serializers.serialize(object.accountId,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  RequestOnlineFriendShip deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = RequestOnlineFriendShipBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'accountId':
          result.accountId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
