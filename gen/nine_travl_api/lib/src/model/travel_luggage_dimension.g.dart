// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'travel_luggage_dimension.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const TravelLuggageDimension _$S = const TravelLuggageDimension._('S');
const TravelLuggageDimension _$M = const TravelLuggageDimension._('M');
const TravelLuggageDimension _$L = const TravelLuggageDimension._('L');
const TravelLuggageDimension _$XL = const TravelLuggageDimension._('XL');

TravelLuggageDimension _$valueOf(String name) {
  switch (name) {
    case 'S':
      return _$S;
    case 'M':
      return _$M;
    case 'L':
      return _$L;
    case 'XL':
      return _$XL;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<TravelLuggageDimension> _$values =
    new BuiltSet<TravelLuggageDimension>(const <TravelLuggageDimension>[
  _$S,
  _$M,
  _$L,
  _$XL,
]);

class _$TravelLuggageDimensionMeta {
  const _$TravelLuggageDimensionMeta();
  TravelLuggageDimension get S => _$S;
  TravelLuggageDimension get M => _$M;
  TravelLuggageDimension get L => _$L;
  TravelLuggageDimension get XL => _$XL;
  TravelLuggageDimension valueOf(String name) => _$valueOf(name);
  BuiltSet<TravelLuggageDimension> get values => _$values;
}

abstract class _$TravelLuggageDimensionMixin {
  // ignore: non_constant_identifier_names
  _$TravelLuggageDimensionMeta get TravelLuggageDimension =>
      const _$TravelLuggageDimensionMeta();
}

Serializer<TravelLuggageDimension> _$travelLuggageDimensionSerializer =
    new _$TravelLuggageDimensionSerializer();

class _$TravelLuggageDimensionSerializer
    implements PrimitiveSerializer<TravelLuggageDimension> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'S': 'S',
    'M': 'M',
    'L': 'L',
    'XL': 'XL',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'S': 'S',
    'M': 'M',
    'L': 'L',
    'XL': 'XL',
  };

  @override
  final Iterable<Type> types = const <Type>[TravelLuggageDimension];
  @override
  final String wireName = 'TravelLuggageDimension';

  @override
  Object serialize(Serializers serializers, TravelLuggageDimension object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  TravelLuggageDimension deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      TravelLuggageDimension.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
