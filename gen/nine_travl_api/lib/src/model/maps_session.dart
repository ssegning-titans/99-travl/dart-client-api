//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'maps_session.g.dart';

abstract class MapsSession implements Built<MapsSession, MapsSessionBuilder> {
  @BuiltValueField(wireName: r'sessionToken')
  String? get sessionToken;

  MapsSession._();

  static void _initializeBuilder(MapsSessionBuilder b) => b;

  factory MapsSession([void updates(MapsSessionBuilder b)]) = _$MapsSession;

  @BuiltValueSerializer(custom: true)
  static Serializer<MapsSession> get serializer => _$MapsSessionSerializer();
}

class _$MapsSessionSerializer implements StructuredSerializer<MapsSession> {
  @override
  final Iterable<Type> types = const [MapsSession, _$MapsSession];

  @override
  final String wireName = r'MapsSession';

  @override
  Iterable<Object?> serialize(Serializers serializers, MapsSession object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.sessionToken != null) {
      result
        ..add(r'sessionToken')
        ..add(serializers.serialize(object.sessionToken,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  MapsSession deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = MapsSessionBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'sessionToken':
          result.sessionToken = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
