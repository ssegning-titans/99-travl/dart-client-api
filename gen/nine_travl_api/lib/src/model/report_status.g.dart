// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_status.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const ReportStatus _$PENDING = const ReportStatus._('PENDING');
const ReportStatus _$HANDLED = const ReportStatus._('HANDLED');
const ReportStatus _$IN_PROGRESS = const ReportStatus._('IN_PROGRESS');

ReportStatus _$valueOf(String name) {
  switch (name) {
    case 'PENDING':
      return _$PENDING;
    case 'HANDLED':
      return _$HANDLED;
    case 'IN_PROGRESS':
      return _$IN_PROGRESS;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<ReportStatus> _$values =
    new BuiltSet<ReportStatus>(const <ReportStatus>[
  _$PENDING,
  _$HANDLED,
  _$IN_PROGRESS,
]);

class _$ReportStatusMeta {
  const _$ReportStatusMeta();
  ReportStatus get PENDING => _$PENDING;
  ReportStatus get HANDLED => _$HANDLED;
  ReportStatus get IN_PROGRESS => _$IN_PROGRESS;
  ReportStatus valueOf(String name) => _$valueOf(name);
  BuiltSet<ReportStatus> get values => _$values;
}

abstract class _$ReportStatusMixin {
  // ignore: non_constant_identifier_names
  _$ReportStatusMeta get ReportStatus => const _$ReportStatusMeta();
}

Serializer<ReportStatus> _$reportStatusSerializer =
    new _$ReportStatusSerializer();

class _$ReportStatusSerializer implements PrimitiveSerializer<ReportStatus> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'PENDING': 'PENDING',
    'HANDLED': 'HANDLED',
    'IN_PROGRESS': 'IN_PROGRESS',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'PENDING': 'PENDING',
    'HANDLED': 'HANDLED',
    'IN_PROGRESS': 'IN_PROGRESS',
  };

  @override
  final Iterable<Type> types = const <Type>[ReportStatus];
  @override
  final String wireName = 'ReportStatus';

  @override
  Object serialize(Serializers serializers, ReportStatus object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  ReportStatus deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      ReportStatus.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
