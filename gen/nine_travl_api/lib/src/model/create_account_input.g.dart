// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_account_input.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$CreateAccountInput extends CreateAccountInput {
  @override
  final String? firstName;
  @override
  final String? lastName;
  @override
  final String? title;
  @override
  final String? email;
  @override
  final String? phoneNumber;
  @override
  final AccountGender? gender;
  @override
  final String? bio;
  @override
  final String? avatarUrl;
  @override
  final AccountType? accountType;

  factory _$CreateAccountInput(
          [void Function(CreateAccountInputBuilder)? updates]) =>
      (new CreateAccountInputBuilder()..update(updates)).build();

  _$CreateAccountInput._(
      {this.firstName,
      this.lastName,
      this.title,
      this.email,
      this.phoneNumber,
      this.gender,
      this.bio,
      this.avatarUrl,
      this.accountType})
      : super._();

  @override
  CreateAccountInput rebuild(
          void Function(CreateAccountInputBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  CreateAccountInputBuilder toBuilder() =>
      new CreateAccountInputBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is CreateAccountInput &&
        firstName == other.firstName &&
        lastName == other.lastName &&
        title == other.title &&
        email == other.email &&
        phoneNumber == other.phoneNumber &&
        gender == other.gender &&
        bio == other.bio &&
        avatarUrl == other.avatarUrl &&
        accountType == other.accountType;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc(
                                $jc($jc(0, firstName.hashCode),
                                    lastName.hashCode),
                                title.hashCode),
                            email.hashCode),
                        phoneNumber.hashCode),
                    gender.hashCode),
                bio.hashCode),
            avatarUrl.hashCode),
        accountType.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('CreateAccountInput')
          ..add('firstName', firstName)
          ..add('lastName', lastName)
          ..add('title', title)
          ..add('email', email)
          ..add('phoneNumber', phoneNumber)
          ..add('gender', gender)
          ..add('bio', bio)
          ..add('avatarUrl', avatarUrl)
          ..add('accountType', accountType))
        .toString();
  }
}

class CreateAccountInputBuilder
    implements Builder<CreateAccountInput, CreateAccountInputBuilder> {
  _$CreateAccountInput? _$v;

  String? _firstName;
  String? get firstName => _$this._firstName;
  set firstName(String? firstName) => _$this._firstName = firstName;

  String? _lastName;
  String? get lastName => _$this._lastName;
  set lastName(String? lastName) => _$this._lastName = lastName;

  String? _title;
  String? get title => _$this._title;
  set title(String? title) => _$this._title = title;

  String? _email;
  String? get email => _$this._email;
  set email(String? email) => _$this._email = email;

  String? _phoneNumber;
  String? get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String? phoneNumber) => _$this._phoneNumber = phoneNumber;

  AccountGender? _gender;
  AccountGender? get gender => _$this._gender;
  set gender(AccountGender? gender) => _$this._gender = gender;

  String? _bio;
  String? get bio => _$this._bio;
  set bio(String? bio) => _$this._bio = bio;

  String? _avatarUrl;
  String? get avatarUrl => _$this._avatarUrl;
  set avatarUrl(String? avatarUrl) => _$this._avatarUrl = avatarUrl;

  AccountType? _accountType;
  AccountType? get accountType => _$this._accountType;
  set accountType(AccountType? accountType) =>
      _$this._accountType = accountType;

  CreateAccountInputBuilder() {
    CreateAccountInput._initializeBuilder(this);
  }

  CreateAccountInputBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _firstName = $v.firstName;
      _lastName = $v.lastName;
      _title = $v.title;
      _email = $v.email;
      _phoneNumber = $v.phoneNumber;
      _gender = $v.gender;
      _bio = $v.bio;
      _avatarUrl = $v.avatarUrl;
      _accountType = $v.accountType;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(CreateAccountInput other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$CreateAccountInput;
  }

  @override
  void update(void Function(CreateAccountInputBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$CreateAccountInput build() {
    final _$result = _$v ??
        new _$CreateAccountInput._(
            firstName: firstName,
            lastName: lastName,
            title: title,
            email: email,
            phoneNumber: phoneNumber,
            gender: gender,
            bio: bio,
            avatarUrl: avatarUrl,
            accountType: accountType);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
