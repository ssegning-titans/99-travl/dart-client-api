//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/travel_point.dart';
import 'package:built_collection/built_collection.dart';
import 'package:nine_travl_api/src/model/account.dart';
import 'package:nine_travl_api/src/model/travel.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'search_travel.g.dart';

abstract class SearchTravel
    implements Built<SearchTravel, SearchTravelBuilder> {
  @BuiltValueField(wireName: r'points')
  BuiltList<TravelPoint> get points;

  @BuiltValueField(wireName: r'travel')
  Travel get travel;

  @BuiltValueField(wireName: r'provider')
  Account get provider;

  SearchTravel._();

  static void _initializeBuilder(SearchTravelBuilder b) => b;

  factory SearchTravel([void updates(SearchTravelBuilder b)]) = _$SearchTravel;

  @BuiltValueSerializer(custom: true)
  static Serializer<SearchTravel> get serializer => _$SearchTravelSerializer();
}

class _$SearchTravelSerializer implements StructuredSerializer<SearchTravel> {
  @override
  final Iterable<Type> types = const [SearchTravel, _$SearchTravel];

  @override
  final String wireName = r'SearchTravel';

  @override
  Iterable<Object?> serialize(Serializers serializers, SearchTravel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'points')
      ..add(serializers.serialize(object.points,
          specifiedType: const FullType(BuiltList, [FullType(TravelPoint)])));
    result
      ..add(r'travel')
      ..add(serializers.serialize(object.travel,
          specifiedType: const FullType(Travel)));
    result
      ..add(r'provider')
      ..add(serializers.serialize(object.provider,
          specifiedType: const FullType(Account)));
    return result;
  }

  @override
  SearchTravel deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = SearchTravelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'points':
          result.points.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, [FullType(TravelPoint)]))
              as BuiltList<TravelPoint>);
          break;
        case r'travel':
          result.travel.replace(serializers.deserialize(value,
              specifiedType: const FullType(Travel)) as Travel);
          break;
        case r'provider':
          result.provider.replace(serializers.deserialize(value,
              specifiedType: const FullType(Account)) as Account);
          break;
      }
    }
    return result.build();
  }
}
