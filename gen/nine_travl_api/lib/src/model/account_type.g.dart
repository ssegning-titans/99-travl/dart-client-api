// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_type.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const AccountType _$c2C = const AccountType._('c2C');
const AccountType _$b2C = const AccountType._('b2C');
const AccountType _$b2B = const AccountType._('b2B');

AccountType _$valueOf(String name) {
  switch (name) {
    case 'c2C':
      return _$c2C;
    case 'b2C':
      return _$b2C;
    case 'b2B':
      return _$b2B;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<AccountType> _$values =
    new BuiltSet<AccountType>(const <AccountType>[
  _$c2C,
  _$b2C,
  _$b2B,
]);

class _$AccountTypeMeta {
  const _$AccountTypeMeta();
  AccountType get c2C => _$c2C;
  AccountType get b2C => _$b2C;
  AccountType get b2B => _$b2B;
  AccountType valueOf(String name) => _$valueOf(name);
  BuiltSet<AccountType> get values => _$values;
}

abstract class _$AccountTypeMixin {
  // ignore: non_constant_identifier_names
  _$AccountTypeMeta get AccountType => const _$AccountTypeMeta();
}

Serializer<AccountType> _$accountTypeSerializer = new _$AccountTypeSerializer();

class _$AccountTypeSerializer implements PrimitiveSerializer<AccountType> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'c2C': 'C2C',
    'b2C': 'B2C',
    'b2B': 'B2B',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'C2C': 'c2C',
    'B2C': 'b2C',
    'B2B': 'b2B',
  };

  @override
  final Iterable<Type> types = const <Type>[AccountType];
  @override
  final String wireName = 'AccountType';

  @override
  Object serialize(Serializers serializers, AccountType object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  AccountType deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      AccountType.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
