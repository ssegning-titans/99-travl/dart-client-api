//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/travel_repetition.dart';
import 'package:nine_travl_api/src/model/travel_luggage_dimension.dart';
import 'package:nine_travl_api/src/model/travel_point.dart';
import 'package:built_collection/built_collection.dart';
import 'package:nine_travl_api/src/model/travel_mode_unit.dart';
import 'package:nine_travl_api/src/model/travel_quantity.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'create_travel.g.dart';

abstract class CreateTravel
    implements Built<CreateTravel, CreateTravelBuilder> {
  @BuiltValueField(wireName: r'passagePoints')
  BuiltList<TravelPoint> get passagePoints;

  @BuiltValueField(wireName: r'repeating')
  TravelRepetition? get repeating;

  @BuiltValueField(wireName: r'maxQuantity')
  TravelQuantity get maxQuantity;

  @BuiltValueField(wireName: r'transportMode')
  TravelModeUnit? get transportMode;
  // enum transportModeEnum {  PLANE,  CAR,  BOAT,  };

  @BuiltValueField(wireName: r'luggageDimension')
  TravelLuggageDimension get luggageDimension;
  // enum luggageDimensionEnum {  S,  M,  L,  XL,  };

  @BuiltValueField(wireName: r'description')
  String? get description;

  CreateTravel._();

  static void _initializeBuilder(CreateTravelBuilder b) => b;

  factory CreateTravel([void updates(CreateTravelBuilder b)]) = _$CreateTravel;

  @BuiltValueSerializer(custom: true)
  static Serializer<CreateTravel> get serializer => _$CreateTravelSerializer();
}

class _$CreateTravelSerializer implements StructuredSerializer<CreateTravel> {
  @override
  final Iterable<Type> types = const [CreateTravel, _$CreateTravel];

  @override
  final String wireName = r'CreateTravel';

  @override
  Iterable<Object?> serialize(Serializers serializers, CreateTravel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'passagePoints')
      ..add(serializers.serialize(object.passagePoints,
          specifiedType: const FullType(BuiltList, [FullType(TravelPoint)])));
    if (object.repeating != null) {
      result
        ..add(r'repeating')
        ..add(serializers.serialize(object.repeating,
            specifiedType: const FullType(TravelRepetition)));
    }
    result
      ..add(r'maxQuantity')
      ..add(serializers.serialize(object.maxQuantity,
          specifiedType: const FullType(TravelQuantity)));
    if (object.transportMode != null) {
      result
        ..add(r'transportMode')
        ..add(serializers.serialize(object.transportMode,
            specifiedType: const FullType(TravelModeUnit)));
    }
    result
      ..add(r'luggageDimension')
      ..add(serializers.serialize(object.luggageDimension,
          specifiedType: const FullType(TravelLuggageDimension)));
    if (object.description != null) {
      result
        ..add(r'description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  CreateTravel deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = CreateTravelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'passagePoints':
          result.passagePoints.replace(serializers.deserialize(value,
                  specifiedType:
                      const FullType(BuiltList, [FullType(TravelPoint)]))
              as BuiltList<TravelPoint>);
          break;
        case r'repeating':
          result.repeating.replace(serializers.deserialize(value,
                  specifiedType: const FullType(TravelRepetition))
              as TravelRepetition);
          break;
        case r'maxQuantity':
          result.maxQuantity.replace(serializers.deserialize(value,
              specifiedType: const FullType(TravelQuantity)) as TravelQuantity);
          break;
        case r'transportMode':
          result.transportMode = serializers.deserialize(value,
              specifiedType: const FullType(TravelModeUnit)) as TravelModeUnit;
          break;
        case r'luggageDimension':
          result.luggageDimension = serializers.deserialize(value,
                  specifiedType: const FullType(TravelLuggageDimension))
              as TravelLuggageDimension;
          break;
        case r'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
