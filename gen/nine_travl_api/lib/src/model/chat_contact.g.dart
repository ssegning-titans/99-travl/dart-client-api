// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_contact.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$ChatContact extends ChatContact {
  @override
  final String? id;
  @override
  final int? creationDate;
  @override
  final bool isAppUser;
  @override
  final String? accountId;
  @override
  final String phoneNumber;
  @override
  final String? description;
  @override
  final String firstName;
  @override
  final String lastName;
  @override
  final String? avatar;

  factory _$ChatContact([void Function(ChatContactBuilder)? updates]) =>
      (new ChatContactBuilder()..update(updates)).build();

  _$ChatContact._(
      {this.id,
      this.creationDate,
      required this.isAppUser,
      this.accountId,
      required this.phoneNumber,
      this.description,
      required this.firstName,
      required this.lastName,
      this.avatar})
      : super._() {
    BuiltValueNullFieldError.checkNotNull(
        isAppUser, 'ChatContact', 'isAppUser');
    BuiltValueNullFieldError.checkNotNull(
        phoneNumber, 'ChatContact', 'phoneNumber');
    BuiltValueNullFieldError.checkNotNull(
        firstName, 'ChatContact', 'firstName');
    BuiltValueNullFieldError.checkNotNull(lastName, 'ChatContact', 'lastName');
  }

  @override
  ChatContact rebuild(void Function(ChatContactBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ChatContactBuilder toBuilder() => new ChatContactBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ChatContact &&
        id == other.id &&
        creationDate == other.creationDate &&
        isAppUser == other.isAppUser &&
        accountId == other.accountId &&
        phoneNumber == other.phoneNumber &&
        description == other.description &&
        firstName == other.firstName &&
        lastName == other.lastName &&
        avatar == other.avatar;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc(
                    $jc(
                        $jc(
                            $jc($jc($jc(0, id.hashCode), creationDate.hashCode),
                                isAppUser.hashCode),
                            accountId.hashCode),
                        phoneNumber.hashCode),
                    description.hashCode),
                firstName.hashCode),
            lastName.hashCode),
        avatar.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ChatContact')
          ..add('id', id)
          ..add('creationDate', creationDate)
          ..add('isAppUser', isAppUser)
          ..add('accountId', accountId)
          ..add('phoneNumber', phoneNumber)
          ..add('description', description)
          ..add('firstName', firstName)
          ..add('lastName', lastName)
          ..add('avatar', avatar))
        .toString();
  }
}

class ChatContactBuilder implements Builder<ChatContact, ChatContactBuilder> {
  _$ChatContact? _$v;

  String? _id;
  String? get id => _$this._id;
  set id(String? id) => _$this._id = id;

  int? _creationDate;
  int? get creationDate => _$this._creationDate;
  set creationDate(int? creationDate) => _$this._creationDate = creationDate;

  bool? _isAppUser;
  bool? get isAppUser => _$this._isAppUser;
  set isAppUser(bool? isAppUser) => _$this._isAppUser = isAppUser;

  String? _accountId;
  String? get accountId => _$this._accountId;
  set accountId(String? accountId) => _$this._accountId = accountId;

  String? _phoneNumber;
  String? get phoneNumber => _$this._phoneNumber;
  set phoneNumber(String? phoneNumber) => _$this._phoneNumber = phoneNumber;

  String? _description;
  String? get description => _$this._description;
  set description(String? description) => _$this._description = description;

  String? _firstName;
  String? get firstName => _$this._firstName;
  set firstName(String? firstName) => _$this._firstName = firstName;

  String? _lastName;
  String? get lastName => _$this._lastName;
  set lastName(String? lastName) => _$this._lastName = lastName;

  String? _avatar;
  String? get avatar => _$this._avatar;
  set avatar(String? avatar) => _$this._avatar = avatar;

  ChatContactBuilder() {
    ChatContact._initializeBuilder(this);
  }

  ChatContactBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _id = $v.id;
      _creationDate = $v.creationDate;
      _isAppUser = $v.isAppUser;
      _accountId = $v.accountId;
      _phoneNumber = $v.phoneNumber;
      _description = $v.description;
      _firstName = $v.firstName;
      _lastName = $v.lastName;
      _avatar = $v.avatar;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ChatContact other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$ChatContact;
  }

  @override
  void update(void Function(ChatContactBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ChatContact build() {
    final _$result = _$v ??
        new _$ChatContact._(
            id: id,
            creationDate: creationDate,
            isAppUser: BuiltValueNullFieldError.checkNotNull(
                isAppUser, 'ChatContact', 'isAppUser'),
            accountId: accountId,
            phoneNumber: BuiltValueNullFieldError.checkNotNull(
                phoneNumber, 'ChatContact', 'phoneNumber'),
            description: description,
            firstName: BuiltValueNullFieldError.checkNotNull(
                firstName, 'ChatContact', 'firstName'),
            lastName: BuiltValueNullFieldError.checkNotNull(
                lastName, 'ChatContact', 'lastName'),
            avatar: avatar);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
