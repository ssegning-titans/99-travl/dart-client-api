//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/report_type.dart';
import 'package:nine_travl_api/src/model/report_status.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'base_report.g.dart';

abstract class BaseReport implements Built<BaseReport, BaseReportBuilder> {
  @BuiltValueField(wireName: r'reportType')
  ReportType get reportType;
  // enum reportTypeEnum {  TRAVEL,  ACCOUNT,  BUG,  };

  @BuiltValueField(wireName: r'authorId')
  String get authorId;

  @BuiltValueField(wireName: r'status')
  ReportStatus get status;
  // enum statusEnum {  PENDING,  HANDLED,  IN_PROGRESS,  };

  @BuiltValueField(wireName: r'createdAt')
  int? get createdAt;

  @BuiltValueField(wireName: r'id')
  String? get id;

  @BuiltValueField(wireName: r'description')
  String? get description;

  BaseReport._();

  static void _initializeBuilder(BaseReportBuilder b) => b;

  factory BaseReport([void updates(BaseReportBuilder b)]) = _$BaseReport;

  @BuiltValueSerializer(custom: true)
  static Serializer<BaseReport> get serializer => _$BaseReportSerializer();
}

class _$BaseReportSerializer implements StructuredSerializer<BaseReport> {
  @override
  final Iterable<Type> types = const [BaseReport, _$BaseReport];

  @override
  final String wireName = r'BaseReport';

  @override
  Iterable<Object?> serialize(Serializers serializers, BaseReport object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'reportType')
      ..add(serializers.serialize(object.reportType,
          specifiedType: const FullType(ReportType)));
    result
      ..add(r'authorId')
      ..add(serializers.serialize(object.authorId,
          specifiedType: const FullType(String)));
    result
      ..add(r'status')
      ..add(serializers.serialize(object.status,
          specifiedType: const FullType(ReportStatus)));
    if (object.createdAt != null) {
      result
        ..add(r'createdAt')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(int)));
    }
    if (object.id != null) {
      result
        ..add(r'id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.description != null) {
      result
        ..add(r'description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    return result;
  }

  @override
  BaseReport deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = BaseReportBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'reportType':
          result.reportType = serializers.deserialize(value,
              specifiedType: const FullType(ReportType)) as ReportType;
          break;
        case r'authorId':
          result.authorId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(ReportStatus)) as ReportStatus;
          break;
        case r'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
