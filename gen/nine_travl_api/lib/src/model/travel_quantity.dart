//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/travel_weight_unit.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'travel_quantity.g.dart';

abstract class TravelQuantity
    implements Built<TravelQuantity, TravelQuantityBuilder> {
  @BuiltValueField(wireName: r'weight')
  int get weight;

  @BuiltValueField(wireName: r'unit')
  TravelWeightUnit get unit;
  // enum unitEnum {  KG,  Lb,  };

  @BuiltValueField(wireName: r'pricePerUnit')
  int get pricePerUnit;

  TravelQuantity._();

  static void _initializeBuilder(TravelQuantityBuilder b) => b
    ..weight = 1
    ..pricePerUnit = 1;

  factory TravelQuantity([void updates(TravelQuantityBuilder b)]) =
      _$TravelQuantity;

  @BuiltValueSerializer(custom: true)
  static Serializer<TravelQuantity> get serializer =>
      _$TravelQuantitySerializer();
}

class _$TravelQuantitySerializer
    implements StructuredSerializer<TravelQuantity> {
  @override
  final Iterable<Type> types = const [TravelQuantity, _$TravelQuantity];

  @override
  final String wireName = r'TravelQuantity';

  @override
  Iterable<Object?> serialize(Serializers serializers, TravelQuantity object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    result
      ..add(r'weight')
      ..add(serializers.serialize(object.weight,
          specifiedType: const FullType(int)));
    result
      ..add(r'unit')
      ..add(serializers.serialize(object.unit,
          specifiedType: const FullType(TravelWeightUnit)));
    result
      ..add(r'pricePerUnit')
      ..add(serializers.serialize(object.pricePerUnit,
          specifiedType: const FullType(int)));
    return result;
  }

  @override
  TravelQuantity deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = TravelQuantityBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'weight':
          result.weight = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'unit':
          result.unit = serializers.deserialize(value,
                  specifiedType: const FullType(TravelWeightUnit))
              as TravelWeightUnit;
          break;
        case r'pricePerUnit':
          result.pricePerUnit = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
      }
    }
    return result.build();
  }
}
