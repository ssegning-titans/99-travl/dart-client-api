// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'text_chat_message_all_of.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$TextChatMessageAllOf extends TextChatMessageAllOf {
  @override
  final String message;

  factory _$TextChatMessageAllOf(
          [void Function(TextChatMessageAllOfBuilder)? updates]) =>
      (new TextChatMessageAllOfBuilder()..update(updates)).build();

  _$TextChatMessageAllOf._({required this.message}) : super._() {
    BuiltValueNullFieldError.checkNotNull(
        message, 'TextChatMessageAllOf', 'message');
  }

  @override
  TextChatMessageAllOf rebuild(
          void Function(TextChatMessageAllOfBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TextChatMessageAllOfBuilder toBuilder() =>
      new TextChatMessageAllOfBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TextChatMessageAllOf && message == other.message;
  }

  @override
  int get hashCode {
    return $jf($jc(0, message.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TextChatMessageAllOf')
          ..add('message', message))
        .toString();
  }
}

class TextChatMessageAllOfBuilder
    implements Builder<TextChatMessageAllOf, TextChatMessageAllOfBuilder> {
  _$TextChatMessageAllOf? _$v;

  String? _message;
  String? get message => _$this._message;
  set message(String? message) => _$this._message = message;

  TextChatMessageAllOfBuilder() {
    TextChatMessageAllOf._initializeBuilder(this);
  }

  TextChatMessageAllOfBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _message = $v.message;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TextChatMessageAllOf other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$TextChatMessageAllOf;
  }

  @override
  void update(void Function(TextChatMessageAllOfBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TextChatMessageAllOf build() {
    final _$result = _$v ??
        new _$TextChatMessageAllOf._(
            message: BuiltValueNullFieldError.checkNotNull(
                message, 'TextChatMessageAllOf', 'message'));
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
