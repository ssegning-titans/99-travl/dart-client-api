//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/report_travel_all_of.dart';
import 'package:nine_travl_api/src/model/rating_value.dart';
import 'package:nine_travl_api/src/model/rating_type.dart';
import 'package:nine_travl_api/src/model/base_rating.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'rating_travel.g.dart';

// ignore_for_file: unused_import

abstract class RatingTravel
    implements Built<RatingTravel, RatingTravelBuilder> {
  @BuiltValueField(wireName: r'description')
  String? get description;

  @BuiltValueField(wireName: r'authorId')
  String get authorId;

  @BuiltValueField(wireName: r'value')
  RatingValue get value;
  // enum valueEnum {  ONE,  TWO,  THREE,  FOUR,  FIVE,  };

  @BuiltValueField(wireName: r'createdAt')
  int? get createdAt;

  @BuiltValueField(wireName: r'type')
  RatingType get type;
  // enum typeEnum {  TRAVEL,  ACCOUNT,  };

  @BuiltValueField(wireName: r'travelId')
  String get travelId;

  RatingTravel._();

  static void _initializeBuilder(RatingTravelBuilder b) => b;

  factory RatingTravel([void updates(RatingTravelBuilder b)]) = _$RatingTravel;

  @BuiltValueSerializer(custom: true)
  static Serializer<RatingTravel> get serializer => _$RatingTravelSerializer();
}

class _$RatingTravelSerializer implements StructuredSerializer<RatingTravel> {
  @override
  final Iterable<Type> types = const [RatingTravel, _$RatingTravel];

  @override
  final String wireName = r'RatingTravel';

  @override
  Iterable<Object?> serialize(Serializers serializers, RatingTravel object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.description != null) {
      result
        ..add(r'description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    result
      ..add(r'authorId')
      ..add(serializers.serialize(object.authorId,
          specifiedType: const FullType(String)));
    result
      ..add(r'value')
      ..add(serializers.serialize(object.value,
          specifiedType: const FullType(RatingValue)));
    if (object.createdAt != null) {
      result
        ..add(r'createdAt')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(int)));
    }
    result
      ..add(r'type')
      ..add(serializers.serialize(object.type,
          specifiedType: const FullType(RatingType)));
    result
      ..add(r'travelId')
      ..add(serializers.serialize(object.travelId,
          specifiedType: const FullType(String)));
    return result;
  }

  @override
  RatingTravel deserialize(
      Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = RatingTravelBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'authorId':
          result.authorId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(RatingValue)) as RatingValue;
          break;
        case r'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(RatingType)) as RatingType;
          break;
        case r'travelId':
          result.travelId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }
    return result.build();
  }
}
