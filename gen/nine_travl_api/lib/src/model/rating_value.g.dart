// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'rating_value.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

const RatingValue _$ONE = const RatingValue._('ONE');
const RatingValue _$TWO = const RatingValue._('TWO');
const RatingValue _$THREE = const RatingValue._('THREE');
const RatingValue _$FOUR = const RatingValue._('FOUR');
const RatingValue _$FIVE = const RatingValue._('FIVE');

RatingValue _$valueOf(String name) {
  switch (name) {
    case 'ONE':
      return _$ONE;
    case 'TWO':
      return _$TWO;
    case 'THREE':
      return _$THREE;
    case 'FOUR':
      return _$FOUR;
    case 'FIVE':
      return _$FIVE;
    default:
      throw new ArgumentError(name);
  }
}

final BuiltSet<RatingValue> _$values =
    new BuiltSet<RatingValue>(const <RatingValue>[
  _$ONE,
  _$TWO,
  _$THREE,
  _$FOUR,
  _$FIVE,
]);

class _$RatingValueMeta {
  const _$RatingValueMeta();
  RatingValue get ONE => _$ONE;
  RatingValue get TWO => _$TWO;
  RatingValue get THREE => _$THREE;
  RatingValue get FOUR => _$FOUR;
  RatingValue get FIVE => _$FIVE;
  RatingValue valueOf(String name) => _$valueOf(name);
  BuiltSet<RatingValue> get values => _$values;
}

abstract class _$RatingValueMixin {
  // ignore: non_constant_identifier_names
  _$RatingValueMeta get RatingValue => const _$RatingValueMeta();
}

Serializer<RatingValue> _$ratingValueSerializer = new _$RatingValueSerializer();

class _$RatingValueSerializer implements PrimitiveSerializer<RatingValue> {
  static const Map<String, Object> _toWire = const <String, Object>{
    'ONE': 'ONE',
    'TWO': 'TWO',
    'THREE': 'THREE',
    'FOUR': 'FOUR',
    'FIVE': 'FIVE',
  };
  static const Map<Object, String> _fromWire = const <Object, String>{
    'ONE': 'ONE',
    'TWO': 'TWO',
    'THREE': 'THREE',
    'FOUR': 'FOUR',
    'FIVE': 'FIVE',
  };

  @override
  final Iterable<Type> types = const <Type>[RatingValue];
  @override
  final String wireName = 'RatingValue';

  @override
  Object serialize(Serializers serializers, RatingValue object,
          {FullType specifiedType = FullType.unspecified}) =>
      _toWire[object.name] ?? object.name;

  @override
  RatingValue deserialize(Serializers serializers, Object serialized,
          {FullType specifiedType = FullType.unspecified}) =>
      RatingValue.valueOf(
          _fromWire[serialized] ?? (serialized is String ? serialized : ''));
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
