//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/rating_value.dart';
import 'package:nine_travl_api/src/model/rating_type.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'base_rating.g.dart';

abstract class BaseRating implements Built<BaseRating, BaseRatingBuilder> {
  @BuiltValueField(wireName: r'description')
  String? get description;

  @BuiltValueField(wireName: r'authorId')
  String get authorId;

  @BuiltValueField(wireName: r'value')
  RatingValue get value;
  // enum valueEnum {  ONE,  TWO,  THREE,  FOUR,  FIVE,  };

  @BuiltValueField(wireName: r'createdAt')
  int? get createdAt;

  @BuiltValueField(wireName: r'type')
  RatingType get type;
  // enum typeEnum {  TRAVEL,  ACCOUNT,  };

  BaseRating._();

  static void _initializeBuilder(BaseRatingBuilder b) => b;

  factory BaseRating([void updates(BaseRatingBuilder b)]) = _$BaseRating;

  @BuiltValueSerializer(custom: true)
  static Serializer<BaseRating> get serializer => _$BaseRatingSerializer();
}

class _$BaseRatingSerializer implements StructuredSerializer<BaseRating> {
  @override
  final Iterable<Type> types = const [BaseRating, _$BaseRating];

  @override
  final String wireName = r'BaseRating';

  @override
  Iterable<Object?> serialize(Serializers serializers, BaseRating object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.description != null) {
      result
        ..add(r'description')
        ..add(serializers.serialize(object.description,
            specifiedType: const FullType(String)));
    }
    result
      ..add(r'authorId')
      ..add(serializers.serialize(object.authorId,
          specifiedType: const FullType(String)));
    result
      ..add(r'value')
      ..add(serializers.serialize(object.value,
          specifiedType: const FullType(RatingValue)));
    if (object.createdAt != null) {
      result
        ..add(r'createdAt')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(int)));
    }
    result
      ..add(r'type')
      ..add(serializers.serialize(object.type,
          specifiedType: const FullType(RatingType)));
    return result;
  }

  @override
  BaseRating deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = BaseRatingBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'authorId':
          result.authorId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'value':
          result.value = serializers.deserialize(value,
              specifiedType: const FullType(RatingValue)) as RatingValue;
          break;
        case r'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'type':
          result.type = serializers.deserialize(value,
              specifiedType: const FullType(RatingType)) as RatingType;
          break;
      }
    }
    return result.build();
  }
}
