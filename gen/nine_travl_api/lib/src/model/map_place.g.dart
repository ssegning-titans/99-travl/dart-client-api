// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'map_place.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$MapPlace extends MapPlace {
  @override
  final String? placeId;
  @override
  final double? latitude;
  @override
  final double? longitude;
  @override
  final String? formattedAddress;
  @override
  final BuiltList<AddressComponent>? addressComponent;

  factory _$MapPlace([void Function(MapPlaceBuilder)? updates]) =>
      (new MapPlaceBuilder()..update(updates)).build();

  _$MapPlace._(
      {this.placeId,
      this.latitude,
      this.longitude,
      this.formattedAddress,
      this.addressComponent})
      : super._();

  @override
  MapPlace rebuild(void Function(MapPlaceBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  MapPlaceBuilder toBuilder() => new MapPlaceBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is MapPlace &&
        placeId == other.placeId &&
        latitude == other.latitude &&
        longitude == other.longitude &&
        formattedAddress == other.formattedAddress &&
        addressComponent == other.addressComponent;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, placeId.hashCode), latitude.hashCode),
                longitude.hashCode),
            formattedAddress.hashCode),
        addressComponent.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('MapPlace')
          ..add('placeId', placeId)
          ..add('latitude', latitude)
          ..add('longitude', longitude)
          ..add('formattedAddress', formattedAddress)
          ..add('addressComponent', addressComponent))
        .toString();
  }
}

class MapPlaceBuilder implements Builder<MapPlace, MapPlaceBuilder> {
  _$MapPlace? _$v;

  String? _placeId;
  String? get placeId => _$this._placeId;
  set placeId(String? placeId) => _$this._placeId = placeId;

  double? _latitude;
  double? get latitude => _$this._latitude;
  set latitude(double? latitude) => _$this._latitude = latitude;

  double? _longitude;
  double? get longitude => _$this._longitude;
  set longitude(double? longitude) => _$this._longitude = longitude;

  String? _formattedAddress;
  String? get formattedAddress => _$this._formattedAddress;
  set formattedAddress(String? formattedAddress) =>
      _$this._formattedAddress = formattedAddress;

  ListBuilder<AddressComponent>? _addressComponent;
  ListBuilder<AddressComponent> get addressComponent =>
      _$this._addressComponent ??= new ListBuilder<AddressComponent>();
  set addressComponent(ListBuilder<AddressComponent>? addressComponent) =>
      _$this._addressComponent = addressComponent;

  MapPlaceBuilder() {
    MapPlace._initializeBuilder(this);
  }

  MapPlaceBuilder get _$this {
    final $v = _$v;
    if ($v != null) {
      _placeId = $v.placeId;
      _latitude = $v.latitude;
      _longitude = $v.longitude;
      _formattedAddress = $v.formattedAddress;
      _addressComponent = $v.addressComponent?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(MapPlace other) {
    ArgumentError.checkNotNull(other, 'other');
    _$v = other as _$MapPlace;
  }

  @override
  void update(void Function(MapPlaceBuilder)? updates) {
    if (updates != null) updates(this);
  }

  @override
  _$MapPlace build() {
    _$MapPlace _$result;
    try {
      _$result = _$v ??
          new _$MapPlace._(
              placeId: placeId,
              latitude: latitude,
              longitude: longitude,
              formattedAddress: formattedAddress,
              addressComponent: _addressComponent?.build());
    } catch (_) {
      late String _$failedField;
      try {
        _$failedField = 'addressComponent';
        _addressComponent?.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'MapPlace', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
