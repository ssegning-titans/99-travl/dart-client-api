//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:nine_travl_api/src/model/chat_status.dart';
import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'chat.g.dart';

abstract class Chat implements Built<Chat, ChatBuilder> {
  @BuiltValueField(wireName: r'id')
  String? get id;

  @BuiltValueField(wireName: r'createdAt')
  int? get createdAt;

  @BuiltValueField(wireName: r'members')
  BuiltList<String>? get members;

  @BuiltValueField(wireName: r'travelId')
  String? get travelId;

  @BuiltValueField(wireName: r'status')
  ChatStatus? get status;
  // enum statusEnum {  ACTIVE,  ARCHIVED,  };

  Chat._();

  static void _initializeBuilder(ChatBuilder b) => b;

  factory Chat([void updates(ChatBuilder b)]) = _$Chat;

  @BuiltValueSerializer(custom: true)
  static Serializer<Chat> get serializer => _$ChatSerializer();
}

class _$ChatSerializer implements StructuredSerializer<Chat> {
  @override
  final Iterable<Type> types = const [Chat, _$Chat];

  @override
  final String wireName = r'Chat';

  @override
  Iterable<Object?> serialize(Serializers serializers, Chat object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object?>[];
    if (object.id != null) {
      result
        ..add(r'id')
        ..add(serializers.serialize(object.id,
            specifiedType: const FullType(String)));
    }
    if (object.createdAt != null) {
      result
        ..add(r'createdAt')
        ..add(serializers.serialize(object.createdAt,
            specifiedType: const FullType(int)));
    }
    if (object.members != null) {
      result
        ..add(r'members')
        ..add(serializers.serialize(object.members,
            specifiedType: const FullType(BuiltList, [FullType(String)])));
    }
    if (object.travelId != null) {
      result
        ..add(r'travelId')
        ..add(serializers.serialize(object.travelId,
            specifiedType: const FullType(String)));
    }
    if (object.status != null) {
      result
        ..add(r'status')
        ..add(serializers.serialize(object.status,
            specifiedType: const FullType(ChatStatus)));
    }
    return result;
  }

  @override
  Chat deserialize(Serializers serializers, Iterable<Object?> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = ChatBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final Object? value = iterator.current;
      switch (key) {
        case r'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'createdAt':
          result.createdAt = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case r'members':
          result.members.replace(serializers.deserialize(value,
                  specifiedType: const FullType(BuiltList, [FullType(String)]))
              as BuiltList<String>);
          break;
        case r'travelId':
          result.travelId = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case r'status':
          result.status = serializers.deserialize(value,
              specifiedType: const FullType(ChatStatus)) as ChatStatus;
          break;
      }
    }
    return result.build();
  }
}
