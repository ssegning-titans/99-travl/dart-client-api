//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

import 'package:dio/dio.dart';
import 'package:built_value/serializer.dart';
import 'package:nine_travl_api/src/serializers.dart';
import 'package:nine_travl_api/src/auth/api_key_auth.dart';
import 'package:nine_travl_api/src/auth/basic_auth.dart';
import 'package:nine_travl_api/src/auth/oauth.dart';
import 'package:nine_travl_api/src/api/account_address_api.dart';
import 'package:nine_travl_api/src/api/accounts_api.dart';
import 'package:nine_travl_api/src/api/chat_messages_api.dart';
import 'package:nine_travl_api/src/api/chats_api.dart';
import 'package:nine_travl_api/src/api/credentials_api.dart';
import 'package:nine_travl_api/src/api/document_api.dart';
import 'package:nine_travl_api/src/api/favorites_api.dart';
import 'package:nine_travl_api/src/api/friends_api.dart';
import 'package:nine_travl_api/src/api/maps_api.dart';
import 'package:nine_travl_api/src/api/phone_number_api.dart';
import 'package:nine_travl_api/src/api/ratings_api.dart';
import 'package:nine_travl_api/src/api/reports_api.dart';
import 'package:nine_travl_api/src/api/reservations_api.dart';
import 'package:nine_travl_api/src/api/stripe_api.dart';
import 'package:nine_travl_api/src/api/travels_api.dart';

class NineTravlApi {
  static const String basePath = r'http://localhost:8080';

  final Dio dio;
  final Serializers serializers;

  NineTravlApi({
    Dio? dio,
    Serializers? serializers,
    String? basePathOverride,
    List<Interceptor>? interceptors,
  })  : this.serializers = serializers ?? standardSerializers,
        this.dio = dio ??
            Dio(BaseOptions(
              baseUrl: basePathOverride ?? basePath,
              connectTimeout: 5000,
              receiveTimeout: 3000,
            )) {
    if (interceptors == null) {
      this.dio.interceptors.addAll([
        OAuthInterceptor(),
        BasicAuthInterceptor(),
        ApiKeyAuthInterceptor(),
      ]);
    } else {
      this.dio.interceptors.addAll(interceptors);
    }
  }

  void setOAuthToken(String name, String token) {
    if (this.dio.interceptors.any((i) => i is OAuthInterceptor)) {
      (this.dio.interceptors.firstWhere((i) => i is OAuthInterceptor)
              as OAuthInterceptor)
          .tokens[name] = token;
    }
  }

  void setBasicAuth(String name, String username, String password) {
    if (this.dio.interceptors.any((i) => i is BasicAuthInterceptor)) {
      (this.dio.interceptors.firstWhere((i) => i is BasicAuthInterceptor)
              as BasicAuthInterceptor)
          .authInfo[name] = BasicAuthInfo(username, password);
    }
  }

  void setApiKey(String name, String apiKey) {
    if (this.dio.interceptors.any((i) => i is ApiKeyAuthInterceptor)) {
      (this
                  .dio
                  .interceptors
                  .firstWhere((element) => element is ApiKeyAuthInterceptor)
              as ApiKeyAuthInterceptor)
          .apiKeys[name] = apiKey;
    }
  }

  /// Get AccountAddressApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  AccountAddressApi getAccountAddressApi() {
    return AccountAddressApi(dio, serializers);
  }

  /// Get AccountsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  AccountsApi getAccountsApi() {
    return AccountsApi(dio, serializers);
  }

  /// Get ChatMessagesApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  ChatMessagesApi getChatMessagesApi() {
    return ChatMessagesApi(dio, serializers);
  }

  /// Get ChatsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  ChatsApi getChatsApi() {
    return ChatsApi(dio, serializers);
  }

  /// Get CredentialsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  CredentialsApi getCredentialsApi() {
    return CredentialsApi(dio, serializers);
  }

  /// Get DocumentApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  DocumentApi getDocumentApi() {
    return DocumentApi(dio, serializers);
  }

  /// Get FavoritesApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  FavoritesApi getFavoritesApi() {
    return FavoritesApi(dio, serializers);
  }

  /// Get FriendsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  FriendsApi getFriendsApi() {
    return FriendsApi(dio, serializers);
  }

  /// Get MapsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  MapsApi getMapsApi() {
    return MapsApi(dio, serializers);
  }

  /// Get PhoneNumberApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  PhoneNumberApi getPhoneNumberApi() {
    return PhoneNumberApi(dio, serializers);
  }

  /// Get RatingsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  RatingsApi getRatingsApi() {
    return RatingsApi(dio, serializers);
  }

  /// Get ReportsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  ReportsApi getReportsApi() {
    return ReportsApi(dio, serializers);
  }

  /// Get ReservationsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  ReservationsApi getReservationsApi() {
    return ReservationsApi(dio, serializers);
  }

  /// Get StripeApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  StripeApi getStripeApi() {
    return StripeApi(dio, serializers);
  }

  /// Get TravelsApi instance, base route and serializer can be overridden by a given but be careful,
  /// by doing that all interceptors will not be executed
  TravelsApi getTravelsApi() {
    return TravelsApi(dio, serializers);
  }
}
