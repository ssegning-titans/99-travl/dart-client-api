//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//

// ignore_for_file: unused_import

import 'package:built_collection/built_collection.dart';
import 'package:built_value/json_object.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:built_value/iso_8601_date_time_serializer.dart';
import 'package:nine_travl_api/src/date_serializer.dart';
import 'package:nine_travl_api/src/model/date.dart';

import 'package:nine_travl_api/src/model/account.dart';
import 'package:nine_travl_api/src/model/account_address.dart';
import 'package:nine_travl_api/src/model/account_gender.dart';
import 'package:nine_travl_api/src/model/account_status.dart';
import 'package:nine_travl_api/src/model/account_type.dart';
import 'package:nine_travl_api/src/model/address_component.dart';
import 'package:nine_travl_api/src/model/average_rating_person.dart';
import 'package:nine_travl_api/src/model/average_rating_travel.dart';
import 'package:nine_travl_api/src/model/base_chat_message.dart';
import 'package:nine_travl_api/src/model/base_favorite.dart';
import 'package:nine_travl_api/src/model/base_friend.dart';
import 'package:nine_travl_api/src/model/base_rating.dart';
import 'package:nine_travl_api/src/model/base_report.dart';
import 'package:nine_travl_api/src/model/bug_media.dart';
import 'package:nine_travl_api/src/model/change_personal_data_request.dart';
import 'package:nine_travl_api/src/model/chat.dart';
import 'package:nine_travl_api/src/model/chat_contact.dart';
import 'package:nine_travl_api/src/model/chat_media.dart';
import 'package:nine_travl_api/src/model/chat_message.dart';
import 'package:nine_travl_api/src/model/chat_message_type.dart';
import 'package:nine_travl_api/src/model/chat_status.dart';
import 'package:nine_travl_api/src/model/contact_chat_message.dart';
import 'package:nine_travl_api/src/model/contact_chat_message_all_of.dart';
import 'package:nine_travl_api/src/model/create_account_input.dart';
import 'package:nine_travl_api/src/model/create_credential_input.dart';
import 'package:nine_travl_api/src/model/create_credential_response.dart';
import 'package:nine_travl_api/src/model/create_report_account.dart';
import 'package:nine_travl_api/src/model/create_report_bug.dart';
import 'package:nine_travl_api/src/model/create_report_travel.dart';
import 'package:nine_travl_api/src/model/create_travel.dart';
import 'package:nine_travl_api/src/model/credential_type.dart';
import 'package:nine_travl_api/src/model/disable_credential_response.dart';
import 'package:nine_travl_api/src/model/document.dart';
import 'package:nine_travl_api/src/model/favorite.dart';
import 'package:nine_travl_api/src/model/favorite_account.dart';
import 'package:nine_travl_api/src/model/favorite_travel.dart';
import 'package:nine_travl_api/src/model/favorite_type.dart';
import 'package:nine_travl_api/src/model/friend.dart';
import 'package:nine_travl_api/src/model/friend_process_type.dart';
import 'package:nine_travl_api/src/model/friend_ship_status.dart';
import 'package:nine_travl_api/src/model/friend_type.dart';
import 'package:nine_travl_api/src/model/has_account_password.dart';
import 'package:nine_travl_api/src/model/lat_lng.dart';
import 'package:nine_travl_api/src/model/map_place.dart';
import 'package:nine_travl_api/src/model/maps_session.dart';
import 'package:nine_travl_api/src/model/media_chat_message.dart';
import 'package:nine_travl_api/src/model/media_chat_message_all_of.dart';
import 'package:nine_travl_api/src/model/offline_friend.dart';
import 'package:nine_travl_api/src/model/online_friend.dart';
import 'package:nine_travl_api/src/model/online_friend_all_of.dart';
import 'package:nine_travl_api/src/model/phone_tan_request.dart';
import 'package:nine_travl_api/src/model/phone_tan_response.dart';
import 'package:nine_travl_api/src/model/phone_tan_status.dart';
import 'package:nine_travl_api/src/model/phone_tan_validate_request.dart';
import 'package:nine_travl_api/src/model/place_type.dart';
import 'package:nine_travl_api/src/model/proposition_chat_message.dart';
import 'package:nine_travl_api/src/model/proposition_chat_message_all_of.dart';
import 'package:nine_travl_api/src/model/rating_account.dart';
import 'package:nine_travl_api/src/model/rating_account_all_of.dart';
import 'package:nine_travl_api/src/model/rating_travel.dart';
import 'package:nine_travl_api/src/model/rating_type.dart';
import 'package:nine_travl_api/src/model/rating_value.dart';
import 'package:nine_travl_api/src/model/report_account.dart';
import 'package:nine_travl_api/src/model/report_account_all_of.dart';
import 'package:nine_travl_api/src/model/report_bug.dart';
import 'package:nine_travl_api/src/model/report_status.dart';
import 'package:nine_travl_api/src/model/report_travel.dart';
import 'package:nine_travl_api/src/model/report_travel_all_of.dart';
import 'package:nine_travl_api/src/model/report_type.dart';
import 'package:nine_travl_api/src/model/request_offline_friend_ship.dart';
import 'package:nine_travl_api/src/model/request_online_friend_ship.dart';
import 'package:nine_travl_api/src/model/routing_direction.dart';
import 'package:nine_travl_api/src/model/routing_map_place.dart';
import 'package:nine_travl_api/src/model/routing_map_place_all_of.dart';
import 'package:nine_travl_api/src/model/search_travel.dart';
import 'package:nine_travl_api/src/model/stripe_account.dart';
import 'package:nine_travl_api/src/model/stripe_payment_intent_request.dart';
import 'package:nine_travl_api/src/model/stripe_payment_intent_response.dart';
import 'package:nine_travl_api/src/model/text_chat_message.dart';
import 'package:nine_travl_api/src/model/text_chat_message_all_of.dart';
import 'package:nine_travl_api/src/model/travel.dart';
import 'package:nine_travl_api/src/model/travel_days.dart';
import 'package:nine_travl_api/src/model/travel_luggage_dimension.dart';
import 'package:nine_travl_api/src/model/travel_mode_unit.dart';
import 'package:nine_travl_api/src/model/travel_point.dart';
import 'package:nine_travl_api/src/model/travel_quantity.dart';
import 'package:nine_travl_api/src/model/travel_repetition.dart';
import 'package:nine_travl_api/src/model/travel_reservation.dart';
import 'package:nine_travl_api/src/model/travel_reservation_count.dart';
import 'package:nine_travl_api/src/model/travel_weight_unit.dart';
import 'package:nine_travl_api/src/model/update_friend.dart';
import 'package:nine_travl_api/src/model/update_friend_status.dart';
import 'package:nine_travl_api/src/model/uploaded_response.dart';
import 'package:nine_travl_api/src/model/validate_friend_ship_code.dart';
import 'package:nine_travl_api/src/model/validate_password_input.dart';
import 'package:nine_travl_api/src/model/validate_password_response.dart';

part 'serializers.g.dart';

@SerializersFor([
  Account,
  AccountAddress,
  AccountGender,
  AccountStatus,
  AccountType,
  AddressComponent,
  AverageRatingPerson,
  AverageRatingTravel,
  BaseChatMessage,
  BaseFavorite,
  BaseFriend,
  BaseRating,
  BaseReport,
  BugMedia,
  ChangePersonalDataRequest,
  Chat,
  ChatContact,
  ChatMedia,
  ChatMessage,
  ChatMessageType,
  ChatStatus,
  ContactChatMessage,
  ContactChatMessageAllOf,
  CreateAccountInput,
  CreateCredentialInput,
  CreateCredentialResponse,
  CreateReportAccount,
  CreateReportBug,
  CreateReportTravel,
  CreateTravel,
  CredentialType,
  DisableCredentialResponse,
  Document,
  Favorite,
  FavoriteAccount,
  FavoriteTravel,
  FavoriteType,
  Friend,
  FriendProcessType,
  FriendShipStatus,
  FriendType,
  HasAccountPassword,
  LatLng,
  MapPlace,
  MapsSession,
  MediaChatMessage,
  MediaChatMessageAllOf,
  OfflineFriend,
  OnlineFriend,
  OnlineFriendAllOf,
  PhoneTanRequest,
  PhoneTanResponse,
  PhoneTanStatus,
  PhoneTanValidateRequest,
  PlaceType,
  PropositionChatMessage,
  PropositionChatMessageAllOf,
  RatingAccount,
  RatingAccountAllOf,
  RatingTravel,
  RatingType,
  RatingValue,
  ReportAccount,
  ReportAccountAllOf,
  ReportBug,
  ReportStatus,
  ReportTravel,
  ReportTravelAllOf,
  ReportType,
  RequestOfflineFriendShip,
  RequestOnlineFriendShip,
  RoutingDirection,
  RoutingMapPlace,
  RoutingMapPlaceAllOf,
  SearchTravel,
  StripeAccount,
  StripePaymentIntentRequest,
  StripePaymentIntentResponse,
  TextChatMessage,
  TextChatMessageAllOf,
  Travel,
  TravelDays,
  TravelLuggageDimension,
  TravelModeUnit,
  TravelPoint,
  TravelQuantity,
  TravelRepetition,
  TravelReservation,
  TravelReservationCount,
  TravelWeightUnit,
  UpdateFriend,
  UpdateFriendStatus,
  UploadedResponse,
  ValidateFriendShipCode,
  ValidatePasswordInput,
  ValidatePasswordResponse,
])
Serializers serializers = (_$serializers.toBuilder()
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(AccountAddress)]),
        () => ListBuilder<AccountAddress>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Account)]),
        () => ListBuilder<Account>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(ChatMessage)]),
        () => ListBuilder<ChatMessage>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Chat)]),
        () => ListBuilder<Chat>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltMap, [FullType(String), FullType(String)]),
        () => MapBuilder<String, String>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(UploadedResponse)]),
        () => ListBuilder<UploadedResponse>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Favorite)]),
        () => ListBuilder<Favorite>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Friend)]),
        () => ListBuilder<Friend>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(MapPlace)]),
        () => ListBuilder<MapPlace>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(RoutingDirection)]),
        () => ListBuilder<RoutingDirection>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(RatingTravel)]),
        () => ListBuilder<RatingTravel>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(RatingAccount)]),
        () => ListBuilder<RatingAccount>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(BaseRating)]),
        () => ListBuilder<BaseRating>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(ReportAccount)]),
        () => ListBuilder<ReportAccount>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(BaseReport)]),
        () => ListBuilder<BaseReport>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(ReportTravel)]),
        () => ListBuilder<ReportTravel>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(ReportBug)]),
        () => ListBuilder<ReportBug>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(TravelReservation)]),
        () => ListBuilder<TravelReservation>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(Travel)]),
        () => ListBuilder<Travel>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(TravelPoint)]),
        () => ListBuilder<TravelPoint>(),
      )
      ..addBuilderFactory(
        const FullType(BuiltList, [FullType(SearchTravel)]),
        () => ListBuilder<SearchTravel>(),
      )
      ..add(const DateSerializer())
      ..add(Iso8601DateTimeSerializer()))
    .build();

Serializers standardSerializers =
    (serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
