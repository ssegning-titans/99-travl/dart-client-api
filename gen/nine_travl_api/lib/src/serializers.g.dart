// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'serializers.dart';

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializers _$serializers = (new Serializers().toBuilder()
      ..add(Account.serializer)
      ..add(AccountAddress.serializer)
      ..add(AccountGender.serializer)
      ..add(AccountStatus.serializer)
      ..add(AccountType.serializer)
      ..add(AddressComponent.serializer)
      ..add(AverageRatingPerson.serializer)
      ..add(AverageRatingTravel.serializer)
      ..add(BaseChatMessage.serializer)
      ..add(BaseFavorite.serializer)
      ..add(BaseFriend.serializer)
      ..add(BaseRating.serializer)
      ..add(BaseReport.serializer)
      ..add(BugMedia.serializer)
      ..add(ChangePersonalDataRequest.serializer)
      ..add(Chat.serializer)
      ..add(ChatContact.serializer)
      ..add(ChatMedia.serializer)
      ..add(ChatMessage.serializer)
      ..add(ChatMessageType.serializer)
      ..add(ChatStatus.serializer)
      ..add(ContactChatMessage.serializer)
      ..add(ContactChatMessageAllOf.serializer)
      ..add(CreateAccountInput.serializer)
      ..add(CreateCredentialInput.serializer)
      ..add(CreateCredentialResponse.serializer)
      ..add(CreateReportAccount.serializer)
      ..add(CreateReportBug.serializer)
      ..add(CreateReportTravel.serializer)
      ..add(CreateTravel.serializer)
      ..add(CredentialType.serializer)
      ..add(DisableCredentialResponse.serializer)
      ..add(Document.serializer)
      ..add(Favorite.serializer)
      ..add(FavoriteAccount.serializer)
      ..add(FavoriteTravel.serializer)
      ..add(FavoriteType.serializer)
      ..add(Friend.serializer)
      ..add(FriendProcessType.serializer)
      ..add(FriendShipStatus.serializer)
      ..add(FriendType.serializer)
      ..add(HasAccountPassword.serializer)
      ..add(LatLng.serializer)
      ..add(MapPlace.serializer)
      ..add(MapsSession.serializer)
      ..add(MediaChatMessage.serializer)
      ..add(MediaChatMessageAllOf.serializer)
      ..add(OfflineFriend.serializer)
      ..add(OnlineFriend.serializer)
      ..add(OnlineFriendAllOf.serializer)
      ..add(PhoneTanRequest.serializer)
      ..add(PhoneTanResponse.serializer)
      ..add(PhoneTanStatus.serializer)
      ..add(PhoneTanValidateRequest.serializer)
      ..add(PlaceType.serializer)
      ..add(PropositionChatMessage.serializer)
      ..add(PropositionChatMessageAllOf.serializer)
      ..add(RatingAccount.serializer)
      ..add(RatingAccountAllOf.serializer)
      ..add(RatingTravel.serializer)
      ..add(RatingType.serializer)
      ..add(RatingValue.serializer)
      ..add(ReportAccount.serializer)
      ..add(ReportAccountAllOf.serializer)
      ..add(ReportBug.serializer)
      ..add(ReportStatus.serializer)
      ..add(ReportTravel.serializer)
      ..add(ReportTravelAllOf.serializer)
      ..add(ReportType.serializer)
      ..add(RequestOfflineFriendShip.serializer)
      ..add(RequestOnlineFriendShip.serializer)
      ..add(RoutingDirection.serializer)
      ..add(RoutingMapPlace.serializer)
      ..add(RoutingMapPlaceAllOf.serializer)
      ..add(SearchTravel.serializer)
      ..add(StripeAccount.serializer)
      ..add(StripePaymentIntentRequest.serializer)
      ..add(StripePaymentIntentResponse.serializer)
      ..add(TextChatMessage.serializer)
      ..add(TextChatMessageAllOf.serializer)
      ..add(Travel.serializer)
      ..add(TravelDays.serializer)
      ..add(TravelLuggageDimension.serializer)
      ..add(TravelModeUnit.serializer)
      ..add(TravelPoint.serializer)
      ..add(TravelQuantity.serializer)
      ..add(TravelRepetition.serializer)
      ..add(TravelReservation.serializer)
      ..add(TravelReservationCount.serializer)
      ..add(TravelWeightUnit.serializer)
      ..add(UpdateFriend.serializer)
      ..add(UpdateFriendStatus.serializer)
      ..add(UploadedResponse.serializer)
      ..add(ValidateFriendShipCode.serializer)
      ..add(ValidatePasswordInput.serializer)
      ..add(ValidatePasswordResponse.serializer)
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(AccountAddress)]),
          () => new ListBuilder<AccountAddress>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(AddressComponent)]),
          () => new ListBuilder<AddressComponent>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(AddressComponent)]),
          () => new ListBuilder<AddressComponent>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(PlaceType)]),
          () => new ListBuilder<PlaceType>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(RoutingMapPlace)]),
          () => new ListBuilder<RoutingMapPlace>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(String)]),
          () => new ListBuilder<String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(TravelDays)]),
          () => new ListBuilder<TravelDays>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(TravelPoint)]),
          () => new ListBuilder<TravelPoint>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(TravelPoint)]),
          () => new ListBuilder<TravelPoint>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(TravelPoint)]),
          () => new ListBuilder<TravelPoint>())
      ..addBuilderFactory(
          const FullType(
              BuiltMap, const [const FullType(String), const FullType(String)]),
          () => new MapBuilder<String, String>())
      ..addBuilderFactory(
          const FullType(
              BuiltMap, const [const FullType(String), const FullType(String)]),
          () => new MapBuilder<String, String>())
      ..addBuilderFactory(
          const FullType(
              BuiltMap, const [const FullType(String), const FullType(String)]),
          () => new MapBuilder<String, String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(BugMedia)]),
          () => new ListBuilder<BugMedia>())
      ..addBuilderFactory(
          const FullType(
              BuiltMap, const [const FullType(String), const FullType(String)]),
          () => new MapBuilder<String, String>())
      ..addBuilderFactory(
          const FullType(BuiltList, const [const FullType(BugMedia)]),
          () => new ListBuilder<BugMedia>())
      ..addBuilderFactory(
          const FullType(
              BuiltMap, const [const FullType(String), const FullType(String)]),
          () => new MapBuilder<String, String>())
      ..addBuilderFactory(
          const FullType(
              BuiltMap, const [const FullType(String), const FullType(String)]),
          () => new MapBuilder<String, String>())
      ..addBuilderFactory(
          const FullType(BuiltSet, const [const FullType(ChatContact)]),
          () => new SetBuilder<ChatContact>())
      ..addBuilderFactory(
          const FullType(BuiltSet, const [const FullType(ChatContact)]),
          () => new SetBuilder<ChatContact>())
      ..addBuilderFactory(
          const FullType(BuiltSet, const [const FullType(ChatMedia)]),
          () => new SetBuilder<ChatMedia>())
      ..addBuilderFactory(
          const FullType(BuiltSet, const [const FullType(ChatMedia)]),
          () => new SetBuilder<ChatMedia>())
      ..addBuilderFactory(
          const FullType(BuiltSet, const [const FullType(ChatMedia)]),
          () => new SetBuilder<ChatMedia>())
      ..addBuilderFactory(
          const FullType(BuiltSet, const [const FullType(ChatContact)]),
          () => new SetBuilder<ChatContact>()))
    .build();

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
