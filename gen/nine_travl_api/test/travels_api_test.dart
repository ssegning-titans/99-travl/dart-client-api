import 'package:test/test.dart';
import 'package:nine_travl_api/nine_travl_api.dart';

/// tests for TravelsApi
void main() {
  final instance = NineTravlApi().getTravelsApi();

  group(TravelsApi, () {
    // Create travel
    //
    //Future<Travel> createTravel(CreateTravel createTravel) async
    test('test createTravel', () async {
      // TODO
    });

    // Delete travel
    //
    // Delete one travel
    //
    //Future deleteTravelById(String travelId) async
    test('test deleteTravelById', () async {
      // TODO
    });

    // Get accounts travels
    //
    //Future<BuiltList<Travel>> getAccountTravels(String accountId, { int size, int page, BuiltList<String> sort }) async
    test('test getAccountTravels', () async {
      // TODO
    });

    // Get travel randomly
    //
    //Future<BuiltList<TravelPoint>> getRandomTravel(BuiltList<double> coord, double radius, { int size, int page, BuiltList<String> sort }) async
    test('test getRandomTravel', () async {
      // TODO
    });

    // Get travel
    //
    // Get one travel
    //
    //Future<Travel> getTravelById(String travelId) async
    test('test getTravelById', () async {
      // TODO
    });

    // Search for travel between two points
    //
    //Future<BuiltList<SearchTravel>> searchTravel(BuiltList<double> startCoord, BuiltList<double> endCoord, double startRadius, double endRadius, int startDate, int endDate, { int size, int page, BuiltList<String> sort }) async
    test('test searchTravel', () async {
      // TODO
    });

    // Update travel
    //
    // Update one travel
    //
    //Future<Travel> updateTravelById(Travel travel) async
    test('test updateTravelById', () async {
      // TODO
    });
  });
}
