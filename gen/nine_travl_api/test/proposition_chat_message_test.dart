import 'package:test/test.dart';
import 'package:nine_travl_api/nine_travl_api.dart';

// tests for PropositionChatMessage
void main() {
  final instance = PropositionChatMessageBuilder();
  // TODO add properties to the builder and call build()

  group(PropositionChatMessage, () {
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // int createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // String authorId
    test('to test the property `authorId`', () async {
      // TODO
    });

    // String chatId
    test('to test the property `chatId`', () async {
      // TODO
    });

    // ChatMessageType messageType
    test('to test the property `messageType`', () async {
      // TODO
    });

    // bool seen (default value: false)
    test('to test the property `seen`', () async {
      // TODO
    });

    // String propositionId
    test('to test the property `propositionId`', () async {
      // TODO
    });

    // String proposerAccountId
    test('to test the property `proposerAccountId`', () async {
      // TODO
    });
  });
}
