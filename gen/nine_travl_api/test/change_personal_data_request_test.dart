import 'package:test/test.dart';
import 'package:nine_travl_api/nine_travl_api.dart';

// tests for ChangePersonalDataRequest
void main() {
  final instance = ChangePersonalDataRequestBuilder();
  // TODO add properties to the builder and call build()

  group(ChangePersonalDataRequest, () {
    // String email
    test('to test the property `email`', () async {
      // TODO
    });

    // bool emailVerified
    test('to test the property `emailVerified`', () async {
      // TODO
    });

    // String firstName
    test('to test the property `firstName`', () async {
      // TODO
    });

    // String lastName
    test('to test the property `lastName`', () async {
      // TODO
    });

    // String bio
    test('to test the property `bio`', () async {
      // TODO
    });

    // String phoneNumber
    test('to test the property `phoneNumber`', () async {
      // TODO
    });

    // String avatarUrl
    test('to test the property `avatarUrl`', () async {
      // TODO
    });

    // bool phoneNumberValid
    test('to test the property `phoneNumberValid`', () async {
      // TODO
    });

    // String locale
    test('to test the property `locale`', () async {
      // TODO
    });

    // AccountGender gender
    test('to test the property `gender`', () async {
      // TODO
    });

    // AccountType accountType
    test('to test the property `accountType`', () async {
      // TODO
    });

    // AccountStatus status
    test('to test the property `status`', () async {
      // TODO
    });
  });
}
