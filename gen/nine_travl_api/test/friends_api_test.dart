import 'package:test/test.dart';
import 'package:nine_travl_api/nine_travl_api.dart';

/// tests for FriendsApi
void main() {
  final instance = NineTravlApi().getFriendsApi();

  group(FriendsApi, () {
    //Future<OfflineFriend> createOfflineFriend({ RequestOfflineFriendShip requestOfflineFriendShip }) async
    test('test createOfflineFriend', () async {
      // TODO
    });

    //Future<OnlineFriend> createOnlineFriend({ RequestOnlineFriendShip requestOnlineFriendShip }) async
    test('test createOnlineFriend', () async {
      // TODO
    });

    //Future deleteFriendById(String friendId) async
    test('test deleteFriendById', () async {
      // TODO
    });

    //Future<Friend> getFriendById(String friendId) async
    test('test getFriendById', () async {
      // TODO
    });

    //Future<BuiltList<Friend>> getFriends({ FriendType type, FriendShipStatus status, int size, int page, BuiltList<String> sort }) async
    test('test getFriends', () async {
      // TODO
    });

    //Future<Friend> updateFriend(String friendId, { UpdateFriend updateFriend }) async
    test('test updateFriend', () async {
      // TODO
    });

    // The receiver which in this case uses the app is notified to accept the relationship
    //
    //Future<Friend> updateFriendStatus(String friendId, { UpdateFriendStatus updateFriendStatus }) async
    test('test updateFriendStatus', () async {
      // TODO
    });

    // The requester is given the code which is send to the friend through offline process
    //
    //Future<Friend> validateFriendShip(String friendId, { ValidateFriendShipCode validateFriendShipCode }) async
    test('test validateFriendShip', () async {
      // TODO
    });
  });
}
