import 'package:test/test.dart';
import 'package:nine_travl_api/nine_travl_api.dart';

/// tests for AccountAddressApi
void main() {
  final instance = NineTravlApi().getAccountAddressApi();

  group(AccountAddressApi, () {
    // Create address
    //
    // Create an address for an account
    //
    //Future<AccountAddress> createAddress(String accountId, AccountAddress accountAddress) async
    test('test createAddress', () async {
      // TODO
    });

    // Delete address
    //
    // Delete an address for an account
    //
    //Future deleteAddressById(String accountId, String addressId) async
    test('test deleteAddressById', () async {
      // TODO
    });

    // Get specific address
    //
    // Get one address for an account by it's Id
    //
    //Future<AccountAddress> findAddressById(String accountId, String addressId) async
    test('test findAddressById', () async {
      // TODO
    });

    // List addresses
    //
    // List accounts addresses using a pageable
    //
    //Future<BuiltList<AccountAddress>> findAddressesByAccountId(String accountId, { int size, int page, BuiltList<String> sort }) async
    test('test findAddressesByAccountId', () async {
      // TODO
    });

    // Get address
    //
    // Get one address for an account
    //
    //Future<AccountAddress> findLastAddressByAccountId(String accountId) async
    test('test findLastAddressByAccountId', () async {
      // TODO
    });

    // Update address
    //
    // Update an address for an account
    //
    //Future<AccountAddress> updateAddress(String accountId, AccountAddress accountAddress) async
    test('test updateAddress', () async {
      // TODO
    });
  });
}
