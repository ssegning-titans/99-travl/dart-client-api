import 'package:test/test.dart';
import 'package:nine_travl_api/nine_travl_api.dart';

// tests for ChatMedia
void main() {
  final instance = ChatMediaBuilder();
  // TODO add properties to the builder and call build()

  group(ChatMedia, () {
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // int creationDate
    test('to test the property `creationDate`', () async {
      // TODO
    });

    // String documentId
    test('to test the property `documentId`', () async {
      // TODO
    });

    // String path
    test('to test the property `path`', () async {
      // TODO
    });

    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // BuiltMap<String, String> metaData
    test('to test the property `metaData`', () async {
      // TODO
    });

    // int width (default value: 1)
    test('to test the property `width`', () async {
      // TODO
    });

    // int height (default value: 1)
    test('to test the property `height`', () async {
      // TODO
    });

    // int priority (default value: 1)
    test('to test the property `priority`', () async {
      // TODO
    });
  });
}
