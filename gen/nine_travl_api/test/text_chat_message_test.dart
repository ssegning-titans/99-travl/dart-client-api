import 'package:test/test.dart';
import 'package:nine_travl_api/nine_travl_api.dart';

// tests for TextChatMessage
void main() {
  final instance = TextChatMessageBuilder();
  // TODO add properties to the builder and call build()

  group(TextChatMessage, () {
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // int createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // String authorId
    test('to test the property `authorId`', () async {
      // TODO
    });

    // String chatId
    test('to test the property `chatId`', () async {
      // TODO
    });

    // ChatMessageType messageType
    test('to test the property `messageType`', () async {
      // TODO
    });

    // bool seen (default value: false)
    test('to test the property `seen`', () async {
      // TODO
    });

    // String message
    test('to test the property `message`', () async {
      // TODO
    });
  });
}
