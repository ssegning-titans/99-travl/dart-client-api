import 'package:test/test.dart';
import 'package:nine_travl_api/nine_travl_api.dart';

// tests for RoutingMapPlace
void main() {
  final instance = RoutingMapPlaceBuilder();
  // TODO add properties to the builder and call build()

  group(RoutingMapPlace, () {
    // String placeId
    test('to test the property `placeId`', () async {
      // TODO
    });

    // double latitude
    test('to test the property `latitude`', () async {
      // TODO
    });

    // double longitude
    test('to test the property `longitude`', () async {
      // TODO
    });

    // String formattedAddress
    test('to test the property `formattedAddress`', () async {
      // TODO
    });

    // BuiltList<AddressComponent> addressComponent
    test('to test the property `addressComponent`', () async {
      // TODO
    });

    // double order
    test('to test the property `order`', () async {
      // TODO
    });
  });
}
