import 'package:test/test.dart';
import 'package:nine_travl_api/nine_travl_api.dart';

// tests for ContactChatMessage
void main() {
  final instance = ContactChatMessageBuilder();
  // TODO add properties to the builder and call build()

  group(ContactChatMessage, () {
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // int createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // String authorId
    test('to test the property `authorId`', () async {
      // TODO
    });

    // String chatId
    test('to test the property `chatId`', () async {
      // TODO
    });

    // ChatMessageType messageType
    test('to test the property `messageType`', () async {
      // TODO
    });

    // bool seen (default value: false)
    test('to test the property `seen`', () async {
      // TODO
    });

    // BuiltSet<ChatContact> contacts
    test('to test the property `contacts`', () async {
      // TODO
    });
  });
}
