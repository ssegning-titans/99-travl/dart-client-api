import 'package:test/test.dart';
import 'package:nine_travl_api/nine_travl_api.dart';

// tests for CreateAccountInput
void main() {
  final instance = CreateAccountInputBuilder();
  // TODO add properties to the builder and call build()

  group(CreateAccountInput, () {
    // String firstName
    test('to test the property `firstName`', () async {
      // TODO
    });

    // String lastName
    test('to test the property `lastName`', () async {
      // TODO
    });

    // String title
    test('to test the property `title`', () async {
      // TODO
    });

    // String email
    test('to test the property `email`', () async {
      // TODO
    });

    // String phoneNumber
    test('to test the property `phoneNumber`', () async {
      // TODO
    });

    // AccountGender gender
    test('to test the property `gender`', () async {
      // TODO
    });

    // String bio
    test('to test the property `bio`', () async {
      // TODO
    });

    // String avatarUrl
    test('to test the property `avatarUrl`', () async {
      // TODO
    });

    // AccountType accountType
    test('to test the property `accountType`', () async {
      // TODO
    });
  });
}
