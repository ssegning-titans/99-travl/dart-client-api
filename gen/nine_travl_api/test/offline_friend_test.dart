import 'package:test/test.dart';
import 'package:nine_travl_api/nine_travl_api.dart';

// tests for OfflineFriend
void main() {
  final instance = OfflineFriendBuilder();
  // TODO add properties to the builder and call build()

  group(OfflineFriend, () {
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // int creationDate
    test('to test the property `creationDate`', () async {
      // TODO
    });

    // FriendShipStatus status
    test('to test the property `status`', () async {
      // TODO
    });

    // FriendType type
    test('to test the property `type`', () async {
      // TODO
    });

    // FriendProcessType processType
    test('to test the property `processType`', () async {
      // TODO
    });

    // String phoneNumber
    test('to test the property `phoneNumber`', () async {
      // TODO
    });

    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // String firstName
    test('to test the property `firstName`', () async {
      // TODO
    });

    // String lastName
    test('to test the property `lastName`', () async {
      // TODO
    });

    // String avatar
    test('to test the property `avatar`', () async {
      // TODO
    });
  });
}
