import 'package:test/test.dart';
import 'package:nine_travl_api/nine_travl_api.dart';

/// tests for ChatMessagesApi
void main() {
  final instance = NineTravlApi().getChatMessagesApi();

  group(ChatMessagesApi, () {
    // Create chat message
    //
    // Create chat message
    //
    //Future<ChatMessage> createContactChatMessage(ContactChatMessage contactChatMessage) async
    test('test createContactChatMessage', () async {
      // TODO
    });

    // Create chat message
    //
    // Create chat message
    //
    //Future<ChatMessage> createMediaChatMessage(MediaChatMessage mediaChatMessage) async
    test('test createMediaChatMessage', () async {
      // TODO
    });

    // Create chat message
    //
    // Create chat message
    //
    //Future<ChatMessage> createPropositionChatMessage(PropositionChatMessage propositionChatMessage) async
    test('test createPropositionChatMessage', () async {
      // TODO
    });

    // Create chat message
    //
    // Create chat message
    //
    //Future<ChatMessage> createTextChatMessage(TextChatMessage textChatMessage) async
    test('test createTextChatMessage', () async {
      // TODO
    });

    // Delete chat message
    //
    // Find and delete one chat message by Id
    //
    //Future deleteOneChatMessages(String messageId) async
    test('test deleteOneChatMessages', () async {
      // TODO
    });

    // Get account's chat
    //
    // Get all chats for an account
    //
    //Future<BuiltList<ChatMessage>> getAllChatMessages(String chatId, { int size, int page, BuiltList<String> sort }) async
    test('test getAllChatMessages', () async {
      // TODO
    });

    // Get account's chat
    //
    // Get all chats for an account
    //
    //Future<BuiltList<ChatMessage>> getAllUnreadChatMessages(String chatId, { int size, int page, BuiltList<String> sort }) async
    test('test getAllUnreadChatMessages', () async {
      // TODO
    });

    // Get account chat's message
    //
    // Get all chats for an account
    //
    //Future<ChatMessage> getOneChatMessage(String messageId) async
    test('test getOneChatMessage', () async {
      // TODO
    });

    // Get account's unread chat messages
    //
    // For the account, for each chat, take the latest message if it's unread
    //
    //Future<BuiltList<ChatMessage>> getUnreadChatMessages(String accountId, { int size, int page, BuiltList<String> sort }) async
    test('test getUnreadChatMessages', () async {
      // TODO
    });

    // Mark as read
    //
    // Mark as read
    //
    //Future markMessageAsRead(String messageId) async
    test('test markMessageAsRead', () async {
      // TODO
    });
  });
}
