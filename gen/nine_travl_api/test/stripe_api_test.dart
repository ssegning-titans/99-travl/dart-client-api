import 'package:test/test.dart';
import 'package:nine_travl_api/nine_travl_api.dart';

/// tests for StripeApi
void main() {
  final instance = NineTravlApi().getStripeApi();

  group(StripeApi, () {
    // Setup
    //
    // Create stripe account and set-it-up and return redirection's url for configuration
    //
    //Future<StripePaymentIntentResponse> createStripePayment(String accountId, StripePaymentIntentRequest stripePaymentIntentRequest) async
    test('test createStripePayment', () async {
      // TODO
    });

    // Get stripe info
    //
    // Get the stripe info related to an account
    //
    //Future<StripeAccount> getAccountStripeInfo() async
    test('test getAccountStripeInfo', () async {
      // TODO
    });

    // Refresh account link
    //
    // Refresh stripe process for account link
    //
    //Future refreshStripeUrl(String tokenId, String accountId) async
    test('test refreshStripeUrl', () async {
      // TODO
    });

    // Setup
    //
    // Create stripe account and set-it-up and return redirection's url for configuration
    //
    //Future setupStripe({ String redirectUrl }) async
    test('test setupStripe', () async {
      // TODO
    });

    // Return url
    //
    // Perform validations and verifications after stripe process
    //
    //Future stripeReturnUrl(String tokenId, String accountId) async
    test('test stripeReturnUrl', () async {
      // TODO
    });
  });
}
