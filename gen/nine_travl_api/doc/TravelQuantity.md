# nine_travl_api.model.TravelQuantity

## Load the model package
```dart
import 'package:nine_travl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**weight** | **int** |  | [default to 1]
**unit** | [**TravelWeightUnit**](TravelWeightUnit.md) |  | 
**pricePerUnit** | **int** |  | [default to 1]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


