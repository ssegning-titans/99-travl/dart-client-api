# nine_travl_api.api.DocumentApi

## Load the API package
```dart
import 'package:nine_travl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getOnDocument**](DocumentApi.md#getondocument) | **get** /api/v1/documents/{documentId} | 
[**uploadOne**](DocumentApi.md#uploadone) | **post** /api/v1/documents/upload | 


# **getOnDocument**
> Document getOnDocument(documentId)



### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new DocumentApi();
var documentId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    var result = api_instance.getOnDocument(documentId);
    print(result);
} catch (e) {
    print('Exception when calling DocumentApi->getOnDocument: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **documentId** | [**String**](.md)|  | 

### Return type

[**Document**](Document.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **uploadOne**
> BuiltList<UploadedResponse> uploadOne(file)



### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new DocumentApi();
var file = BINARY_DATA_HERE; // Uint8List | 

try { 
    var result = api_instance.uploadOne(file);
    print(result);
} catch (e) {
    print('Exception when calling DocumentApi->uploadOne: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **Uint8List**|  | [optional] 

### Return type

[**BuiltList<UploadedResponse>**](UploadedResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

