# nine_travl_api.api.ReservationsApi

## Load the API package
```dart
import 'package:nine_travl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**countTravelReservation**](ReservationsApi.md#counttravelreservation) | **get** /api/v1/travels/{travelId}/reservations/count | Count travel&#39;s reservation
[**createTravelReservation**](ReservationsApi.md#createtravelreservation) | **post** /api/v1/travels/{travelId}/reservations | Create Travel Reservation
[**deleteReservationById**](ReservationsApi.md#deletereservationbyid) | **delete** /api/v1/travels/{travelId}/reservations/{reservationId} | Delete travel
[**getReservationById**](ReservationsApi.md#getreservationbyid) | **get** /api/v1/travels/{travelId}/reservations/{reservationId} | Get travel
[**getReservationForTravel**](ReservationsApi.md#getreservationfortravel) | **get** /api/v1/travels/{travelId}/reservations | Get Reservation for travel
[**updateReservationById**](ReservationsApi.md#updatereservationbyid) | **put** /api/v1/travels/{travelId}/reservations | Update travel


# **countTravelReservation**
> TravelReservationCount countTravelReservation(travelId)

Count travel's reservation

Count all travel's reservations

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new ReservationsApi();
var travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    var result = api_instance.countTravelReservation(travelId);
    print(result);
} catch (e) {
    print('Exception when calling ReservationsApi->countTravelReservation: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | [**String**](.md)|  | 

### Return type

[**TravelReservationCount**](TravelReservationCount.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createTravelReservation**
> TravelReservation createTravelReservation(travelId, travelReservation)

Create Travel Reservation

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new ReservationsApi();
var travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var travelReservation = new TravelReservation(); // TravelReservation | 

try { 
    var result = api_instance.createTravelReservation(travelId, travelReservation);
    print(result);
} catch (e) {
    print('Exception when calling ReservationsApi->createTravelReservation: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | [**String**](.md)|  | 
 **travelReservation** | [**TravelReservation**](TravelReservation.md)|  | 

### Return type

[**TravelReservation**](TravelReservation.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteReservationById**
> deleteReservationById(travelId, reservationId)

Delete travel

Delete one travel

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new ReservationsApi();
var travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var reservationId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    api_instance.deleteReservationById(travelId, reservationId);
} catch (e) {
    print('Exception when calling ReservationsApi->deleteReservationById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | [**String**](.md)|  | 
 **reservationId** | [**String**](.md)|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getReservationById**
> TravelReservation getReservationById(travelId, reservationId)

Get travel

Get one travel

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new ReservationsApi();
var travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var reservationId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    var result = api_instance.getReservationById(travelId, reservationId);
    print(result);
} catch (e) {
    print('Exception when calling ReservationsApi->getReservationById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | [**String**](.md)|  | 
 **reservationId** | [**String**](.md)|  | 

### Return type

[**TravelReservation**](TravelReservation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getReservationForTravel**
> BuiltList<TravelReservation> getReservationForTravel(travelId, size, page, sort)

Get Reservation for travel

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new ReservationsApi();
var travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var size = 56; // int | In the backend, this would always be defaulted at 20
var page = 56; // int | In the backend, this would always be defaulted at 0
var sort = []; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try { 
    var result = api_instance.getReservationForTravel(travelId, size, page, sort);
    print(result);
} catch (e) {
    print('Exception when calling ReservationsApi->getReservationForTravel: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | [**String**](.md)|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList<String>**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList<TravelReservation>**](TravelReservation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateReservationById**
> TravelReservation updateReservationById(travelId, travelReservation)

Update travel

Update one travel

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new ReservationsApi();
var travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var travelReservation = new TravelReservation(); // TravelReservation | 

try { 
    var result = api_instance.updateReservationById(travelId, travelReservation);
    print(result);
} catch (e) {
    print('Exception when calling ReservationsApi->updateReservationById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | [**String**](.md)|  | 
 **travelReservation** | [**TravelReservation**](TravelReservation.md)|  | 

### Return type

[**TravelReservation**](TravelReservation.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

