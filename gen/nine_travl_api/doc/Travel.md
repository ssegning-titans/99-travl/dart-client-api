# nine_travl_api.model.Travel

## Load the model package
```dart
import 'package:nine_travl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | **int** |  | [optional] 
**updatedAt** | **int** |  | [optional] 
**id** | **String** |  | [optional] 
**passagePoints** | [**BuiltList<TravelPoint>**](TravelPoint.md) |  | 
**providerAccountId** | **String** |  | 
**maxQuantity** | [**TravelQuantity**](TravelQuantity.md) |  | 
**transportMode** | [**TravelModeUnit**](TravelModeUnit.md) |  | [optional] 
**luggageDimension** | [**TravelLuggageDimension**](TravelLuggageDimension.md) |  | 
**description** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


