# nine_travl_api.model.SearchTravel

## Load the model package
```dart
import 'package:nine_travl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**points** | [**BuiltList<TravelPoint>**](TravelPoint.md) |  | 
**travel** | [**Travel**](Travel.md) |  | 
**provider** | [**Account**](Account.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


