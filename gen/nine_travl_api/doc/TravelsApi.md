# nine_travl_api.api.TravelsApi

## Load the API package
```dart
import 'package:nine_travl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createTravel**](TravelsApi.md#createtravel) | **post** /api/v1/travels | Create travel
[**deleteTravelById**](TravelsApi.md#deletetravelbyid) | **delete** /api/v1/travels/{travelId} | Delete travel
[**getAccountTravels**](TravelsApi.md#getaccounttravels) | **get** /api/v1/travels/accounts/{accountId} | Get accounts travels
[**getRandomTravel**](TravelsApi.md#getrandomtravel) | **get** /api/v1/travels | Get travel randomly
[**getTravelById**](TravelsApi.md#gettravelbyid) | **get** /api/v1/travels/{travelId} | Get travel
[**searchTravel**](TravelsApi.md#searchtravel) | **get** /api/v1/travels/search | Search for travel between two points
[**updateTravelById**](TravelsApi.md#updatetravelbyid) | **put** /api/v1/travels | Update travel


# **createTravel**
> Travel createTravel(createTravel)

Create travel

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new TravelsApi();
var createTravel = new CreateTravel(); // CreateTravel | 

try { 
    var result = api_instance.createTravel(createTravel);
    print(result);
} catch (e) {
    print('Exception when calling TravelsApi->createTravel: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **createTravel** | [**CreateTravel**](CreateTravel.md)|  | 

### Return type

[**Travel**](Travel.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteTravelById**
> deleteTravelById(travelId)

Delete travel

Delete one travel

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new TravelsApi();
var travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    api_instance.deleteTravelById(travelId);
} catch (e) {
    print('Exception when calling TravelsApi->deleteTravelById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | [**String**](.md)|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAccountTravels**
> BuiltList<Travel> getAccountTravels(accountId, size, page, sort)

Get accounts travels

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new TravelsApi();
var accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var size = 56; // int | In the backend, this would always be defaulted at 20
var page = 56; // int | In the backend, this would always be defaulted at 0
var sort = []; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try { 
    var result = api_instance.getAccountTravels(accountId, size, page, sort);
    print(result);
} catch (e) {
    print('Exception when calling TravelsApi->getAccountTravels: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | [**String**](.md)|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList<String>**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList<Travel>**](Travel.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getRandomTravel**
> BuiltList<TravelPoint> getRandomTravel(coord, radius, size, page, sort)

Get travel randomly

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new TravelsApi();
var coord = []; // BuiltList<double> | The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude
var radius = 1.2; // double | Know if these coordinates are starting or ending points
var size = 56; // int | In the backend, this would always be defaulted at 20
var page = 56; // int | In the backend, this would always be defaulted at 0
var sort = []; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try { 
    var result = api_instance.getRandomTravel(coord, radius, size, page, sort);
    print(result);
} catch (e) {
    print('Exception when calling TravelsApi->getRandomTravel: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **coord** | [**BuiltList<double>**](double.md)| The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude | 
 **radius** | **double**| Know if these coordinates are starting or ending points | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList<String>**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList<TravelPoint>**](TravelPoint.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTravelById**
> Travel getTravelById(travelId)

Get travel

Get one travel

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new TravelsApi();
var travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    var result = api_instance.getTravelById(travelId);
    print(result);
} catch (e) {
    print('Exception when calling TravelsApi->getTravelById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travelId** | [**String**](.md)|  | 

### Return type

[**Travel**](Travel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **searchTravel**
> BuiltList<SearchTravel> searchTravel(startCoord, endCoord, startRadius, endRadius, startDate, endDate, size, page, sort)

Search for travel between two points

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new TravelsApi();
var startCoord = []; // BuiltList<double> | The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude
var endCoord = []; // BuiltList<double> | The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude
var startRadius = 1.2; // double | 
var endRadius = 1.2; // double | 
var startDate = 789; // int | 
var endDate = 789; // int | 
var size = 56; // int | In the backend, this would always be defaulted at 20
var page = 56; // int | In the backend, this would always be defaulted at 0
var sort = []; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try { 
    var result = api_instance.searchTravel(startCoord, endCoord, startRadius, endRadius, startDate, endDate, size, page, sort);
    print(result);
} catch (e) {
    print('Exception when calling TravelsApi->searchTravel: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startCoord** | [**BuiltList<double>**](double.md)| The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude | 
 **endCoord** | [**BuiltList<double>**](double.md)| The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude | 
 **startRadius** | **double**|  | 
 **endRadius** | **double**|  | 
 **startDate** | **int**|  | 
 **endDate** | **int**|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList<String>**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList<SearchTravel>**](SearchTravel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateTravelById**
> Travel updateTravelById(travel)

Update travel

Update one travel

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new TravelsApi();
var travel = new Travel(); // Travel | 

try { 
    var result = api_instance.updateTravelById(travel);
    print(result);
} catch (e) {
    print('Exception when calling TravelsApi->updateTravelById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **travel** | [**Travel**](Travel.md)|  | 

### Return type

[**Travel**](Travel.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

