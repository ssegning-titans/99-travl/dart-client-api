# nine_travl_api.model.TravelRepetition

## Load the model package
```dart
import 'package:nine_travl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**startingRepetitionDate** | **int** |  | 
**endingRepetitionDate** | **int** |  | 
**days** | [**BuiltList<TravelDays>**](TravelDays.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


