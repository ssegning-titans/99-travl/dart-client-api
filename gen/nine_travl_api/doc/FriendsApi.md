# nine_travl_api.api.FriendsApi

## Load the API package
```dart
import 'package:nine_travl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createOfflineFriend**](FriendsApi.md#createofflinefriend) | **post** /api/v1/accounts/friends/offline | 
[**createOnlineFriend**](FriendsApi.md#createonlinefriend) | **post** /api/v1/accounts/friends/online | 
[**deleteFriendById**](FriendsApi.md#deletefriendbyid) | **delete** /api/v1/accounts/friends/{friendId} | 
[**getFriendById**](FriendsApi.md#getfriendbyid) | **get** /api/v1/accounts/friends/{friendId} | 
[**getFriends**](FriendsApi.md#getfriends) | **get** /api/v1/accounts/friends | 
[**updateFriend**](FriendsApi.md#updatefriend) | **put** /api/v1/accounts/friends/{friendId} | 
[**updateFriendStatus**](FriendsApi.md#updatefriendstatus) | **post** /api/v1/accounts/friends/{friendId}/validate-online | 
[**validateFriendShip**](FriendsApi.md#validatefriendship) | **post** /api/v1/accounts/friends/{friendId}/validate-offline | 


# **createOfflineFriend**
> OfflineFriend createOfflineFriend(requestOfflineFriendShip)



### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FriendsApi();
var requestOfflineFriendShip = new RequestOfflineFriendShip(); // RequestOfflineFriendShip | 

try { 
    var result = api_instance.createOfflineFriend(requestOfflineFriendShip);
    print(result);
} catch (e) {
    print('Exception when calling FriendsApi->createOfflineFriend: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestOfflineFriendShip** | [**RequestOfflineFriendShip**](RequestOfflineFriendShip.md)|  | [optional] 

### Return type

[**OfflineFriend**](OfflineFriend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createOnlineFriend**
> OnlineFriend createOnlineFriend(requestOnlineFriendShip)



### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FriendsApi();
var requestOnlineFriendShip = new RequestOnlineFriendShip(); // RequestOnlineFriendShip | 

try { 
    var result = api_instance.createOnlineFriend(requestOnlineFriendShip);
    print(result);
} catch (e) {
    print('Exception when calling FriendsApi->createOnlineFriend: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **requestOnlineFriendShip** | [**RequestOnlineFriendShip**](RequestOnlineFriendShip.md)|  | [optional] 

### Return type

[**OnlineFriend**](OnlineFriend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteFriendById**
> deleteFriendById(friendId)



### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FriendsApi();
var friendId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    api_instance.deleteFriendById(friendId);
} catch (e) {
    print('Exception when calling FriendsApi->deleteFriendById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **friendId** | [**String**](.md)|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getFriendById**
> Friend getFriendById(friendId)



### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FriendsApi();
var friendId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    var result = api_instance.getFriendById(friendId);
    print(result);
} catch (e) {
    print('Exception when calling FriendsApi->getFriendById: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **friendId** | [**String**](.md)|  | 

### Return type

[**Friend**](Friend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getFriends**
> BuiltList<Friend> getFriends(type, status, size, page, sort)



### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FriendsApi();
var type = ; // FriendType | 
var status = ; // FriendShipStatus | 
var size = 56; // int | In the backend, this would always be defaulted at 20
var page = 56; // int | In the backend, this would always be defaulted at 0
var sort = []; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try { 
    var result = api_instance.getFriends(type, status, size, page, sort);
    print(result);
} catch (e) {
    print('Exception when calling FriendsApi->getFriends: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **type** | [**FriendType**](.md)|  | [optional] 
 **status** | [**FriendShipStatus**](.md)|  | [optional] 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList<String>**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList<Friend>**](Friend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateFriend**
> Friend updateFriend(friendId, updateFriend)



### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FriendsApi();
var friendId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var updateFriend = new UpdateFriend(); // UpdateFriend | 

try { 
    var result = api_instance.updateFriend(friendId, updateFriend);
    print(result);
} catch (e) {
    print('Exception when calling FriendsApi->updateFriend: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **friendId** | [**String**](.md)|  | 
 **updateFriend** | [**UpdateFriend**](UpdateFriend.md)|  | [optional] 

### Return type

[**Friend**](Friend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateFriendStatus**
> Friend updateFriendStatus(friendId, updateFriendStatus)



The receiver which in this case uses the app is notified to accept the relationship 

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FriendsApi();
var friendId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var updateFriendStatus = new UpdateFriendStatus(); // UpdateFriendStatus | 

try { 
    var result = api_instance.updateFriendStatus(friendId, updateFriendStatus);
    print(result);
} catch (e) {
    print('Exception when calling FriendsApi->updateFriendStatus: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **friendId** | [**String**](.md)|  | 
 **updateFriendStatus** | [**UpdateFriendStatus**](UpdateFriendStatus.md)|  | [optional] 

### Return type

[**Friend**](Friend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **validateFriendShip**
> Friend validateFriendShip(friendId, validateFriendShipCode)



The requester is given the code which is send to the friend through offline process 

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FriendsApi();
var friendId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var validateFriendShipCode = new ValidateFriendShipCode(); // ValidateFriendShipCode | 

try { 
    var result = api_instance.validateFriendShip(friendId, validateFriendShipCode);
    print(result);
} catch (e) {
    print('Exception when calling FriendsApi->validateFriendShip: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **friendId** | [**String**](.md)|  | 
 **validateFriendShipCode** | [**ValidateFriendShipCode**](ValidateFriendShipCode.md)|  | [optional] 

### Return type

[**Friend**](Friend.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

