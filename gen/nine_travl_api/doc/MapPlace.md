# nine_travl_api.model.MapPlace

## Load the model package
```dart
import 'package:nine_travl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**placeId** | **String** |  | [optional] 
**latitude** | **double** |  | [optional] 
**longitude** | **double** |  | [optional] 
**formattedAddress** | **String** |  | [optional] 
**addressComponent** | [**BuiltList<AddressComponent>**](AddressComponent.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


