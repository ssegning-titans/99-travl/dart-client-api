# nine_travl_api.api.StripeApi

## Load the API package
```dart
import 'package:nine_travl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createStripePayment**](StripeApi.md#createstripepayment) | **post** /api/v1/payments/stripe/{accountId}/intent | Setup
[**getAccountStripeInfo**](StripeApi.md#getaccountstripeinfo) | **get** /api/v1/payments/stripe/account_info | Get stripe info
[**refreshStripeUrl**](StripeApi.md#refreshstripeurl) | **get** /api/v1/payments/stripe/{accountId}/setup/refresh_url | Refresh account link
[**setupStripe**](StripeApi.md#setupstripe) | **get** /api/v1/payments/stripe/setup | Setup
[**stripeReturnUrl**](StripeApi.md#stripereturnurl) | **get** /api/v1/payments/stripe/{accountId}/setup/return_url | Return url


# **createStripePayment**
> StripePaymentIntentResponse createStripePayment(accountId, stripePaymentIntentRequest)

Setup

Create stripe account and set-it-up and return redirection's url for configuration

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new StripeApi();
var accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var stripePaymentIntentRequest = new StripePaymentIntentRequest(); // StripePaymentIntentRequest | 

try { 
    var result = api_instance.createStripePayment(accountId, stripePaymentIntentRequest);
    print(result);
} catch (e) {
    print('Exception when calling StripeApi->createStripePayment: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | [**String**](.md)|  | 
 **stripePaymentIntentRequest** | [**StripePaymentIntentRequest**](StripePaymentIntentRequest.md)|  | 

### Return type

[**StripePaymentIntentResponse**](StripePaymentIntentResponse.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAccountStripeInfo**
> StripeAccount getAccountStripeInfo()

Get stripe info

Get the stripe info related to an account

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new StripeApi();

try { 
    var result = api_instance.getAccountStripeInfo();
    print(result);
} catch (e) {
    print('Exception when calling StripeApi->getAccountStripeInfo: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StripeAccount**](StripeAccount.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **refreshStripeUrl**
> refreshStripeUrl(tokenId, accountId)

Refresh account link

Refresh stripe process for account link

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new StripeApi();
var tokenId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    api_instance.refreshStripeUrl(tokenId, accountId);
} catch (e) {
    print('Exception when calling StripeApi->refreshStripeUrl: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tokenId** | [**String**](.md)|  | 
 **accountId** | [**String**](.md)|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setupStripe**
> setupStripe(redirectUrl)

Setup

Create stripe account and set-it-up and return redirection's url for configuration

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new StripeApi();
var redirectUrl = redirectUrl_example; // String | 

try { 
    api_instance.setupStripe(redirectUrl);
} catch (e) {
    print('Exception when calling StripeApi->setupStripe: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **redirectUrl** | **String**|  | [optional] 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **stripeReturnUrl**
> stripeReturnUrl(tokenId, accountId)

Return url

Perform validations and verifications after stripe process

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new StripeApi();
var tokenId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    api_instance.stripeReturnUrl(tokenId, accountId);
} catch (e) {
    print('Exception when calling StripeApi->stripeReturnUrl: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tokenId** | [**String**](.md)|  | 
 **accountId** | [**String**](.md)|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

