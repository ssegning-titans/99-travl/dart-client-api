# nine_travl_api.model.Document

## Load the model package
```dart
import 'package:nine_travl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bytes** | **String** |  | [optional] 
**contentType** | **String** |  | [optional] 
**filePath** | **String** |  | [optional] 
**metaData** | **BuiltMap<String, String>** |  | [optional] 
**headers** | **BuiltMap<String, String>** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


