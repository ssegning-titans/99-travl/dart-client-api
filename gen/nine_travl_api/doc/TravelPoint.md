# nine_travl_api.model.TravelPoint

## Load the model package
```dart
import 'package:nine_travl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**priority** | **int** |  | [optional] [default to 1]
**creationDate** | **int** |  | [optional] 
**passageTime** | **int** |  | [optional] 
**travelId** | **String** |  | [optional] 
**region** | **String** |  | [optional] 
**formattedName** | **String** |  | [optional] 
**street** | **String** |  | [optional] 
**houseNumber** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**zip** | **String** |  | [optional] 
**country** | **String** |  | 
**location** | [**LatLng**](LatLng.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


