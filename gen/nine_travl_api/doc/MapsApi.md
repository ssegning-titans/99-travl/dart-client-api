# nine_travl_api.api.MapsApi

## Load the API package
```dart
import 'package:nine_travl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**autocomplete**](MapsApi.md#autocomplete) | **get** /api/v1/maps/autocomplete | 
[**findPlaces**](MapsApi.md#findplaces) | **get** /api/v1/maps/by-formatted-name | Addresses by names
[**getAddressByCoordinate**](MapsApi.md#getaddressbycoordinate) | **get** /api/v1/maps/by-coordinate | Address by coordinates
[**getAddressByPlaceId**](MapsApi.md#getaddressbyplaceid) | **get** /api/v1/maps/by-place-id | Address by placeId
[**routing**](MapsApi.md#routing) | **get** /api/v1/maps/routing | 
[**sessionToken**](MapsApi.md#sessiontoken) | **get** /api/v1/maps/session-token | 


# **autocomplete**
> BuiltList<MapPlace> autocomplete(formattedAddress, acceptLanguage, sessionToken)



### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new MapsApi();
var formattedAddress = formattedAddress_example; // String | 
var acceptLanguage = acceptLanguage_example; // String | 
var sessionToken = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    var result = api_instance.autocomplete(formattedAddress, acceptLanguage, sessionToken);
    print(result);
} catch (e) {
    print('Exception when calling MapsApi->autocomplete: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **formattedAddress** | **String**|  | 
 **acceptLanguage** | **String**|  | 
 **sessionToken** | [**String**](.md)|  | 

### Return type

[**BuiltList<MapPlace>**](MapPlace.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **findPlaces**
> BuiltList<MapPlace> findPlaces(formattedAddress, acceptLanguage)

Addresses by names

Get address by place id

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new MapsApi();
var formattedAddress = formattedAddress_example; // String | 
var acceptLanguage = acceptLanguage_example; // String | 

try { 
    var result = api_instance.findPlaces(formattedAddress, acceptLanguage);
    print(result);
} catch (e) {
    print('Exception when calling MapsApi->findPlaces: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **formattedAddress** | **String**|  | 
 **acceptLanguage** | **String**|  | 

### Return type

[**BuiltList<MapPlace>**](MapPlace.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAddressByCoordinate**
> MapPlace getAddressByCoordinate(latitude, longitude, acceptLanguage)

Address by coordinates

Get address by coordinates

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new MapsApi();
var latitude = 1.2; // double | 
var longitude = 1.2; // double | 
var acceptLanguage = acceptLanguage_example; // String | 

try { 
    var result = api_instance.getAddressByCoordinate(latitude, longitude, acceptLanguage);
    print(result);
} catch (e) {
    print('Exception when calling MapsApi->getAddressByCoordinate: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **latitude** | **double**|  | [default to 10]
 **longitude** | **double**|  | [default to 10]
 **acceptLanguage** | **String**|  | 

### Return type

[**MapPlace**](MapPlace.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAddressByPlaceId**
> MapPlace getAddressByPlaceId(placeId, acceptLanguage)

Address by placeId

Get address by place id

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new MapsApi();
var placeId = placeId_example; // String | 
var acceptLanguage = acceptLanguage_example; // String | 

try { 
    var result = api_instance.getAddressByPlaceId(placeId, acceptLanguage);
    print(result);
} catch (e) {
    print('Exception when calling MapsApi->getAddressByPlaceId: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **placeId** | **String**|  | 
 **acceptLanguage** | **String**|  | 

### Return type

[**MapPlace**](MapPlace.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **routing**
> BuiltList<RoutingDirection> routing(startCoord, endCoord, acceptLanguage)



### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new MapsApi();
var startCoord = []; // BuiltList<double> | The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude
var endCoord = []; // BuiltList<double> | The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude
var acceptLanguage = acceptLanguage_example; // String | 

try { 
    var result = api_instance.routing(startCoord, endCoord, acceptLanguage);
    print(result);
} catch (e) {
    print('Exception when calling MapsApi->routing: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startCoord** | [**BuiltList<double>**](double.md)| The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude | 
 **endCoord** | [**BuiltList<double>**](double.md)| The first position in the array is the lat, the second is the lng. In the future, the third will be the altitude | 
 **acceptLanguage** | **String**|  | 

### Return type

[**BuiltList<RoutingDirection>**](RoutingDirection.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **sessionToken**
> MapsSession sessionToken()



### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new MapsApi();

try { 
    var result = api_instance.sessionToken();
    print(result);
} catch (e) {
    print('Exception when calling MapsApi->sessionToken: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**MapsSession**](MapsSession.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

