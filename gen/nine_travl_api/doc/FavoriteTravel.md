# nine_travl_api.model.FavoriteTravel

## Load the model package
```dart
import 'package:nine_travl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ownerId** | **String** |  | 
**createdAt** | **int** |  | [optional] 
**favoriteType** | [**FavoriteType**](FavoriteType.md) |  | 
**travelId** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


