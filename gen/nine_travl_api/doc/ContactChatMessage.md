# nine_travl_api.model.ContactChatMessage

## Load the model package
```dart
import 'package:nine_travl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**createdAt** | **int** |  | [optional] 
**authorId** | **String** |  | 
**chatId** | **String** |  | 
**messageType** | [**ChatMessageType**](ChatMessageType.md) |  | 
**seen** | **bool** |  | [optional] [default to false]
**contacts** | [**BuiltSet<ChatContact>**](ChatContact.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


