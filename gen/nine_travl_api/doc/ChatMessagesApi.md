# nine_travl_api.api.ChatMessagesApi

## Load the API package
```dart
import 'package:nine_travl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createContactChatMessage**](ChatMessagesApi.md#createcontactchatmessage) | **post** /api/v1/messages/contacts | Create chat message
[**createMediaChatMessage**](ChatMessagesApi.md#createmediachatmessage) | **post** /api/v1/messages/medias | Create chat message
[**createPropositionChatMessage**](ChatMessagesApi.md#createpropositionchatmessage) | **post** /api/v1/messages/propositions | Create chat message
[**createTextChatMessage**](ChatMessagesApi.md#createtextchatmessage) | **post** /api/v1/messages/texts | Create chat message
[**deleteOneChatMessages**](ChatMessagesApi.md#deleteonechatmessages) | **delete** /api/v1/messages/{messageId} | Delete chat message
[**getAllChatMessages**](ChatMessagesApi.md#getallchatmessages) | **get** /api/v1/messages/_/chats/{chatId} | Get account&#39;s chat
[**getAllUnreadChatMessages**](ChatMessagesApi.md#getallunreadchatmessages) | **get** /api/v1/messages/_/chats/{chatId}/unread | Get account&#39;s chat
[**getOneChatMessage**](ChatMessagesApi.md#getonechatmessage) | **get** /api/v1/messages/{messageId} | Get account chat&#39;s message
[**getUnreadChatMessages**](ChatMessagesApi.md#getunreadchatmessages) | **get** /api/v1/messages/_/accounts/{accountId}/unread | Get account&#39;s unread chat messages
[**markMessageAsRead**](ChatMessagesApi.md#markmessageasread) | **put** /api/v1/messages/{messageId}/read | Mark as read


# **createContactChatMessage**
> ChatMessage createContactChatMessage(contactChatMessage)

Create chat message

Create chat message

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new ChatMessagesApi();
var contactChatMessage = new ContactChatMessage(); // ContactChatMessage | 

try { 
    var result = api_instance.createContactChatMessage(contactChatMessage);
    print(result);
} catch (e) {
    print('Exception when calling ChatMessagesApi->createContactChatMessage: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **contactChatMessage** | [**ContactChatMessage**](ContactChatMessage.md)|  | 

### Return type

[**ChatMessage**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createMediaChatMessage**
> ChatMessage createMediaChatMessage(mediaChatMessage)

Create chat message

Create chat message

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new ChatMessagesApi();
var mediaChatMessage = new MediaChatMessage(); // MediaChatMessage | 

try { 
    var result = api_instance.createMediaChatMessage(mediaChatMessage);
    print(result);
} catch (e) {
    print('Exception when calling ChatMessagesApi->createMediaChatMessage: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **mediaChatMessage** | [**MediaChatMessage**](MediaChatMessage.md)|  | 

### Return type

[**ChatMessage**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createPropositionChatMessage**
> ChatMessage createPropositionChatMessage(propositionChatMessage)

Create chat message

Create chat message

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new ChatMessagesApi();
var propositionChatMessage = new PropositionChatMessage(); // PropositionChatMessage | 

try { 
    var result = api_instance.createPropositionChatMessage(propositionChatMessage);
    print(result);
} catch (e) {
    print('Exception when calling ChatMessagesApi->createPropositionChatMessage: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **propositionChatMessage** | [**PropositionChatMessage**](PropositionChatMessage.md)|  | 

### Return type

[**ChatMessage**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createTextChatMessage**
> ChatMessage createTextChatMessage(textChatMessage)

Create chat message

Create chat message

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new ChatMessagesApi();
var textChatMessage = new TextChatMessage(); // TextChatMessage | 

try { 
    var result = api_instance.createTextChatMessage(textChatMessage);
    print(result);
} catch (e) {
    print('Exception when calling ChatMessagesApi->createTextChatMessage: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **textChatMessage** | [**TextChatMessage**](TextChatMessage.md)|  | 

### Return type

[**ChatMessage**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteOneChatMessages**
> deleteOneChatMessages(messageId)

Delete chat message

Find and delete one chat message by Id

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new ChatMessagesApi();
var messageId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    api_instance.deleteOneChatMessages(messageId);
} catch (e) {
    print('Exception when calling ChatMessagesApi->deleteOneChatMessages: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **messageId** | [**String**](.md)|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllChatMessages**
> BuiltList<ChatMessage> getAllChatMessages(chatId, size, page, sort)

Get account's chat

Get all chats for an account

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new ChatMessagesApi();
var chatId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var size = 56; // int | In the backend, this would always be defaulted at 20
var page = 56; // int | In the backend, this would always be defaulted at 0
var sort = []; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try { 
    var result = api_instance.getAllChatMessages(chatId, size, page, sort);
    print(result);
} catch (e) {
    print('Exception when calling ChatMessagesApi->getAllChatMessages: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chatId** | [**String**](.md)|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList<String>**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList<ChatMessage>**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getAllUnreadChatMessages**
> BuiltList<ChatMessage> getAllUnreadChatMessages(chatId, size, page, sort)

Get account's chat

Get all chats for an account

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new ChatMessagesApi();
var chatId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var size = 56; // int | In the backend, this would always be defaulted at 20
var page = 56; // int | In the backend, this would always be defaulted at 0
var sort = []; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try { 
    var result = api_instance.getAllUnreadChatMessages(chatId, size, page, sort);
    print(result);
} catch (e) {
    print('Exception when calling ChatMessagesApi->getAllUnreadChatMessages: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chatId** | [**String**](.md)|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList<String>**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList<ChatMessage>**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOneChatMessage**
> ChatMessage getOneChatMessage(messageId)

Get account chat's message

Get all chats for an account

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new ChatMessagesApi();
var messageId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    var result = api_instance.getOneChatMessage(messageId);
    print(result);
} catch (e) {
    print('Exception when calling ChatMessagesApi->getOneChatMessage: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **messageId** | [**String**](.md)|  | 

### Return type

[**ChatMessage**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getUnreadChatMessages**
> BuiltList<ChatMessage> getUnreadChatMessages(accountId, size, page, sort)

Get account's unread chat messages

For the account, for each chat, take the latest message if it's unread

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new ChatMessagesApi();
var accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var size = 56; // int | In the backend, this would always be defaulted at 20
var page = 56; // int | In the backend, this would always be defaulted at 0
var sort = []; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try { 
    var result = api_instance.getUnreadChatMessages(accountId, size, page, sort);
    print(result);
} catch (e) {
    print('Exception when calling ChatMessagesApi->getUnreadChatMessages: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | [**String**](.md)|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList<String>**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList<ChatMessage>**](ChatMessage.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **markMessageAsRead**
> markMessageAsRead(messageId)

Mark as read

Mark as read

### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new ChatMessagesApi();
var messageId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    api_instance.markMessageAsRead(messageId);
} catch (e) {
    print('Exception when calling ChatMessagesApi->markMessageAsRead: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **messageId** | [**String**](.md)|  | 

### Return type

void (empty response body)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

