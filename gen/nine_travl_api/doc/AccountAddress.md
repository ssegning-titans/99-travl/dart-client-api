# nine_travl_api.model.AccountAddress

## Load the model package
```dart
import 'package:nine_travl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**creationDate** | **int** |  | [optional] 
**phoneNumber** | **String** |  | [optional] 
**accountId** | **String** |  | [optional] 
**region** | **String** |  | [optional] 
**formattedName** | **String** |  | [optional] 
**street** | **String** |  | [optional] 
**houseNumber** | **String** |  | [optional] 
**city** | **String** |  | [optional] 
**zip** | **String** |  | [optional] 
**country** | **String** |  | [optional] 
**location** | [**LatLng**](LatLng.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


