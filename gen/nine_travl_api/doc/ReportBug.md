# nine_travl_api.model.ReportBug

## Load the model package
```dart
import 'package:nine_travl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reportType** | [**ReportType**](ReportType.md) |  | 
**authorId** | **String** |  | 
**status** | [**ReportStatus**](ReportStatus.md) |  | 
**createdAt** | **int** |  | [optional] 
**id** | **String** |  | [optional] 
**description** | **String** |  | [optional] 
**title** | **String** |  | 
**deviceInfo** | **BuiltMap<String, String>** |  | 
**screenshot** | [**BuiltList<BugMedia>**](BugMedia.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


