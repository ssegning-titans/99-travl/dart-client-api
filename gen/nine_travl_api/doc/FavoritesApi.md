# nine_travl_api.api.FavoritesApi

## Load the API package
```dart
import 'package:nine_travl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createFavoriteAccount**](FavoritesApi.md#createfavoriteaccount) | **post** /api/v1/accounts/{accountId}/favorites/accounts/{personId} | Create person favorite
[**createTravelFavorite**](FavoritesApi.md#createtravelfavorite) | **post** /api/v1/accounts/{accountId}/favorites/travels/{travelId} | Create travel favorite
[**deleteFavoriteAccount**](FavoritesApi.md#deletefavoriteaccount) | **delete** /api/v1/accounts/{accountId}/favorites/accounts/{personId} | Delete account travel
[**deleteTravelFavorite**](FavoritesApi.md#deletetravelfavorite) | **delete** /api/v1/accounts/{accountId}/favorites/travels/{travelId} | Delete favorite travel
[**getFavoriteAccount**](FavoritesApi.md#getfavoriteaccount) | **get** /api/v1/accounts/{accountId}/favorites/accounts/{personId} | Get account favorite
[**getOwnerFavorites**](FavoritesApi.md#getownerfavorites) | **get** /api/v1/accounts/{accountId}/favorites | Get owner&#39;s favorite
[**getTravelFavorite**](FavoritesApi.md#gettravelfavorite) | **get** /api/v1/accounts/{accountId}/favorites/travels/{travelId} | Get travel favorite


# **createFavoriteAccount**
> FavoriteAccount createFavoriteAccount(accountId, personId)

Create person favorite

Create a person's favorite

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FavoritesApi();
var accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var personId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    var result = api_instance.createFavoriteAccount(accountId, personId);
    print(result);
} catch (e) {
    print('Exception when calling FavoritesApi->createFavoriteAccount: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | [**String**](.md)|  | 
 **personId** | [**String**](.md)|  | 

### Return type

[**FavoriteAccount**](FavoriteAccount.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **createTravelFavorite**
> FavoriteTravel createTravelFavorite(accountId, travelId)

Create travel favorite

Create a travel's favorite

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FavoritesApi();
var accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    var result = api_instance.createTravelFavorite(accountId, travelId);
    print(result);
} catch (e) {
    print('Exception when calling FavoritesApi->createTravelFavorite: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | [**String**](.md)|  | 
 **travelId** | [**String**](.md)|  | 

### Return type

[**FavoriteTravel**](FavoriteTravel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteFavoriteAccount**
> deleteFavoriteAccount(accountId, personId)

Delete account travel

Get a account's favorite

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FavoritesApi();
var accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var personId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    api_instance.deleteFavoriteAccount(accountId, personId);
} catch (e) {
    print('Exception when calling FavoritesApi->deleteFavoriteAccount: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | [**String**](.md)|  | 
 **personId** | [**String**](.md)|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deleteTravelFavorite**
> deleteTravelFavorite(accountId, travelId)

Delete favorite travel

Get a travel's favorite

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FavoritesApi();
var accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    api_instance.deleteTravelFavorite(accountId, travelId);
} catch (e) {
    print('Exception when calling FavoritesApi->deleteTravelFavorite: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | [**String**](.md)|  | 
 **travelId** | [**String**](.md)|  | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getFavoriteAccount**
> FavoriteAccount getFavoriteAccount(accountId, personId)

Get account favorite

Get a account's favorite

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FavoritesApi();
var accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var personId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    var result = api_instance.getFavoriteAccount(accountId, personId);
    print(result);
} catch (e) {
    print('Exception when calling FavoritesApi->getFavoriteAccount: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | [**String**](.md)|  | 
 **personId** | [**String**](.md)|  | 

### Return type

[**FavoriteAccount**](FavoriteAccount.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getOwnerFavorites**
> BuiltList<Favorite> getOwnerFavorites(accountId, size, page, sort)

Get owner's favorite

Get owner's account all favorites

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FavoritesApi();
var accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var size = 56; // int | In the backend, this would always be defaulted at 20
var page = 56; // int | In the backend, this would always be defaulted at 0
var sort = []; // BuiltList<String> | Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported.

try { 
    var result = api_instance.getOwnerFavorites(accountId, size, page, sort);
    print(result);
} catch (e) {
    print('Exception when calling FavoritesApi->getOwnerFavorites: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | [**String**](.md)|  | 
 **size** | **int**| In the backend, this would always be defaulted at 20 | [optional] [default to 20]
 **page** | **int**| In the backend, this would always be defaulted at 0 | [optional] 
 **sort** | [**BuiltList<String>**](String.md)| Sorting criteria in the format: property(,asc|desc). Default sort order is ascending. Multiple sort criteria are supported. | [optional] [default to ListBuilder()]

### Return type

[**BuiltList<Favorite>**](Favorite.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getTravelFavorite**
> FavoriteTravel getTravelFavorite(accountId, travelId)

Get travel favorite

Get a travel's favorite

### Example 
```dart
import 'package:nine_travl_api/api.dart';

var api_instance = new FavoritesApi();
var accountId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var travelId = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 

try { 
    var result = api_instance.getTravelFavorite(accountId, travelId);
    print(result);
} catch (e) {
    print('Exception when calling FavoritesApi->getTravelFavorite: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **accountId** | [**String**](.md)|  | 
 **travelId** | [**String**](.md)|  | 

### Return type

[**FavoriteTravel**](FavoriteTravel.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

