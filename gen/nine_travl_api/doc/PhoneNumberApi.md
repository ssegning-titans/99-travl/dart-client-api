# nine_travl_api.api.PhoneNumberApi

## Load the API package
```dart
import 'package:nine_travl_api/api.dart';
```

All URIs are relative to *http://localhost:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getTan**](PhoneNumberApi.md#gettan) | **post** /api/v1/accounts/{id}/phone-number/tan | 
[**validateTan**](PhoneNumberApi.md#validatetan) | **put** /api/v1/accounts/{id}/phone-number/tan | 


# **getTan**
> PhoneTanResponse getTan(id, phoneTanRequest)



### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new PhoneNumberApi();
var id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var phoneTanRequest = new PhoneTanRequest(); // PhoneTanRequest | 

try { 
    var result = api_instance.getTan(id, phoneTanRequest);
    print(result);
} catch (e) {
    print('Exception when calling PhoneNumberApi->getTan: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**String**](.md)|  | 
 **phoneTanRequest** | [**PhoneTanRequest**](PhoneTanRequest.md)|  | 

### Return type

[**PhoneTanResponse**](PhoneTanResponse.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **validateTan**
> PhoneTanStatus validateTan(id, phoneTanValidateRequest)



### Example 
```dart
import 'package:nine_travl_api/api.dart';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';
// TODO Configure OAuth2 access token for authorization: oauth2
//defaultApiClient.getAuthentication<OAuth>('oauth2').accessToken = 'YOUR_ACCESS_TOKEN';

var api_instance = new PhoneNumberApi();
var id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | 
var phoneTanValidateRequest = new PhoneTanValidateRequest(); // PhoneTanValidateRequest | 

try { 
    var result = api_instance.validateTan(id, phoneTanValidateRequest);
    print(result);
} catch (e) {
    print('Exception when calling PhoneNumberApi->validateTan: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**String**](.md)|  | 
 **phoneTanValidateRequest** | [**PhoneTanValidateRequest**](PhoneTanValidateRequest.md)|  | 

### Return type

[**PhoneTanStatus**](PhoneTanStatus.md)

### Authorization

[oauth2](../README.md#oauth2), [oauth2](../README.md#oauth2)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

