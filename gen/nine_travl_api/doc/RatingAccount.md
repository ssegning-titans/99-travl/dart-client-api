# nine_travl_api.model.RatingAccount

## Load the model package
```dart
import 'package:nine_travl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **String** |  | [optional] 
**authorId** | **String** |  | 
**value** | [**RatingValue**](RatingValue.md) |  | 
**createdAt** | **int** |  | [optional] 
**type** | [**RatingType**](RatingType.md) |  | 
**personId** | **String** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


