# nine_travl_api.model.ChatContact

## Load the model package
```dart
import 'package:nine_travl_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**creationDate** | **int** |  | [optional] 
**isAppUser** | **bool** |  | 
**accountId** | **String** |  | [optional] 
**phoneNumber** | **String** |  | 
**description** | **String** |  | [optional] 
**firstName** | **String** |  | 
**lastName** | **String** |  | 
**avatar** | **String** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


