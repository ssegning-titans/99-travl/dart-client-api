# 99Travl Dart Client API

The main project is inside gen. The `ntravl_client_api` is just a wrapper. 

## Important

After codegen, please replace `required BuiltList<` by `required Iterable<` inside all places.

## Links
- https://medium.com/@rtlsilva/generating-dart-rest-api-client-libraries-using-openapi-generator-9b3dc517e68c
